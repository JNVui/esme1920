import 'package:cash_register_app/utils/auth.dart';
import 'package:flutter/material.dart';

Widget buildNameHeader() {
  Future<String> nameFuture = getCurrentDisplayName();
  return FutureBuilder(
    future: nameFuture,
    builder: (BuildContext context, AsyncSnapshot snapshot) {
      switch (snapshot.connectionState) {
        case ConnectionState.none:
          return Text('Press button to start.');
        case ConnectionState.active:
        case ConnectionState.waiting:
          return _buildTex("Hello.");
        case ConnectionState.done:
          if (snapshot.hasError) return Text('Error: ${snapshot.error}');
          String name = snapshot.data;
          return name == null
              ? _buildTex("Hello,")
              : _buildTex("Hello, ${name.split(' ')[0]}.");
      }
    },
  );
}

Widget _buildTex(t) {
  return new Padding(
    padding: const EdgeInsets.only(bottom: 10.0),
    child: new Text(
      t,
      style: new TextStyle(color: Colors.white, fontSize: 30.0),
    ),
  );
}

Widget cartHeader(BuildContext context) {
  return Container(
    child: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
          child: Text(
            'Panier',
            style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(85.0, 12.0, 0.0, 0.0),
          child: Text(
            '.',
            style: TextStyle(
                fontSize: 40.0,
                fontWeight: FontWeight.bold,
                color: Colors.green),
          ),
        )
      ],
    ),
  );
}

Widget cartTotalFooter(double value) {
  return Container(
    child: Stack(
      children: <Widget>[
        Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: EdgeInsets.fromLTRB(20.0, 13.0, 0.0, 0.0),
              child: Text(
                'Total',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              ),
            )),
        Align(
            alignment: Alignment.bottomRight,
            child: Container(
              padding: EdgeInsets.fromLTRB(20.0, 13.0, 10.0, 0.0),
              child: Text(
                value.toStringAsFixed(1) + '€',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              ),
            )),
        Container(
          padding: EdgeInsets.fromLTRB(78.0, 0.0, 0.0, 0.0),
          child: Text(
            '.',
            style: TextStyle(
                fontSize: 40.0,
                fontWeight: FontWeight.bold,
                color: Colors.green),
          ),
        )
      ],
    ),
  );
}
