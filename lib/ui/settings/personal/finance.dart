import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/tax.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/utils/StringsManager.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FinanceSettingsPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const FinanceSettingsPage({Key key, this.observer}) : super(key: key);

  @override
  FinanceSettingsPageState createState() {
    return new FinanceSettingsPageState(this.observer);
  }
}

class FinanceSettingsPageState extends State<FinanceSettingsPage> {
  final FirebaseAnalyticsObserver observer;

  final FocusNode emailFocus = FocusNode();
  final FocusNode closeEmailsFocus = FocusNode();

  TextEditingController emailController = new TextEditingController();
  TextEditingController closeEmailsController = new TextEditingController();

  FinanceSettingsPageState(this.observer);
  int _radioValue1;
  TAX _selectedTax;

  @override
  void dispose() {
    super.dispose();
    emailFocus.dispose();
    closeEmailsFocus.dispose();

    emailController.dispose();
    closeEmailsController.dispose();
  }

  Future<Company> _updateCompany() async {
    return CompanyProvider.updateCompany(Company.updatePartialJson(
        finance: Finance(
      accountantEmail: emailController.text,
      closeEmails: closeEmailsController.text,
      tax: _selectedTax,
    )));
  }

  void _initTextController(Company c) {
    emailController.text = c.contact.email;
    closeEmailsController.text = c.contact.phoneNumber;
  }

  @override
  void initState() {
    super.initState();

    CompanyProvider.fetchCompany().then((Company company) {
      _initTextController(company);
      _selectedTax = company.finance.tax;

      switch (company.finance.tax) {
        case TAX.T20:
          _radioValue1 = 0;
          break;
        case TAX.T10:
          _radioValue1 = 1;
          break;
        case TAX.T85:
          _radioValue1 = 2;
          break;
        case TAX.T5:
          _radioValue1 = 3;
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.transparent,
          title: new Text("Finance"),
        ),
        body: new Container(
            child: Column(children: <Widget>[
          new Expanded(
            child: new ListView(
              children: <Widget>[
                buildText(
                    emailFocus,
                    emailController,
                    TextInputType.emailAddress,
                    FontAwesomeIcons.envelope,
                    StringManager.of(context).email,
                    false,
                    null),
                _buildSeparator(),
                buildText(
                  closeEmailsFocus,
                  closeEmailsController,
                  TextInputType.emailAddress,
                  FontAwesomeIcons.solidEnvelope,
                  StringManager.of(context).email,
                  false,
                  null,
                ),
                _buildSeparator(),
                taxDropDownMenu(),
              ],
            ),
          ),
          buildButton(
              0.0,
              "Modifier",
              () => _updateCompany()
                  .then((Company comp) => Navigator.pop(context)))
        ])));
  }

  Widget _buildSeparator() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: buildSeparator(),
    );
  }

  Widget taxDropDownMenu() {
    return Padding(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                new Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange,
                ),
                new Text("20%"),
              ],
            ),
            Column(
              children: <Widget>[
                new Radio(
                  value: 1,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange,
                ),
                new Text("10%"),
              ],
            ),
            Column(
              children: <Widget>[
                new Radio(
                  value: 2,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange,
                ),
                new Text("8.5%"),
              ],
            ),
            Column(
              children: <Widget>[
                new Radio(
                  value: 3,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange,
                ),
                new Text("5.5%"),
              ],
            )
          ],
        ));
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          _selectedTax = TAX.T20;
          break;
        case 1:
          _selectedTax = TAX.T10;
          break;
        case 2:
          _selectedTax = TAX.T85;
          break;
        case 3:
          _selectedTax = TAX.T5;
          break;
      }
    });
  }
}
