import 'dart:core';

import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/product.dart';
import 'package:firebase_analytics/observer.dart';

enum DiscountType { PERCENTAGE, NUMBER }

class CartModel {
  final FirebaseAnalyticsObserver observer;

  Set<ProductModel> products = new Set<ProductModel>();
  Company company;
  double extraDiscount = 0.0;
  DiscountType discountType = DiscountType.NUMBER;

  CartModel(this.observer);

  int get length {
    return this.products.length;
  }

  double get total {
    switch (discountType) {
      case DiscountType.PERCENTAGE:
        return applyDiscountPercentage(
            this.totalWithoutDiscount, this.extraDiscount);
      case DiscountType.NUMBER:
        return this.totalWithoutDiscount - this.extraDiscount;
      default:
        return totalWithoutDiscount;
    }
  }

  double get taxTotal {
    return this
        .products
        .fold(0, (total, p) => total + p.taxQuantity * p.qtyInCart);
  }

  double get totalWithoutDiscount {
    return this
        .products
        .fold(0, (total, p) => total + p.realPrice * p.qtyInCart);
  }

  double applyDiscountPercentage(double price, double discount) {
    return price * ((100.0 - (discount ?? 0)) / 100);
  }

  bool get isEmpty {
    return this.products.length <= 0;
  }

  bool containsProduct(ProductModel p) {
    return this.products.contains(p);
  }

  bool addProduct(ProductModel p, {number = 1}) {
    _logAddProduct(p, number);
    if (this.containsProduct(p)) {
      getProductById(p.id).qtyInCart += number;
      print(getProductById(p.id).qtyInCart);
      return false;
    } else {
      ProductModel pTmp = ProductModel.fromJson(p.toJson());
      pTmp.qtyInCart += number;
      this.products.add(pTmp);
      return true;
    }
  }

  ProductModel getProductById(String productId) {
    try {
      return this.products.firstWhere((ProductModel p) => p.id == productId);
    } catch (e) {
      return null;
    }
  }

  void removeQty(ProductModel p) {
    if (p.qtyInCart == 1) {
      //Useless
      p.qtyInCart = 0;
      this.products.remove(p);
    } else {
      p.qtyInCart -= 1;
    }
  }

  void makeRefund() {
    this.products.forEach((ProductModel p) => p.salePrice = -p.salePrice);
  }

  void remove(ProductModel p) {
    this.products.remove(p);
  }

  void purge() {
    this.products = new Set<ProductModel>();
  }

  void _logAddProduct(ProductModel p, int number){

    observer.analytics.logAddToCart(
      currency: 'EUR',
      value: p.realPrice,
      itemId: p.id,
      itemName: p.name,
      itemCategory: p.category,
      quantity:number,
      price: p.salePrice,
    );
  }

}
