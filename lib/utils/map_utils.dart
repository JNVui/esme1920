Map<dynamic, dynamic> cleanNull(Map<dynamic, dynamic> map) {
  map.removeWhere((dynamic key, dynamic value) => value == null);
  return map;
}

dynamic dateTimeEncoder(dynamic item) {
  if (item is DateTime) {
    return item.toIso8601String();
  }
  return item;
}
