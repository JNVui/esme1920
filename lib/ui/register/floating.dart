import 'package:cash_register_app/models/product.dart';
import 'package:cash_register_app/models/stock.dart';
import 'package:cash_register_app/ui/register/home.dart';
import 'package:cash_register_app/utils/scan.dart';
import 'package:flutter/material.dart';

class FancyFab extends StatefulWidget {
  Function() onPressed;
  String tooltip;
  IconData icon;
  BuildContext context;

  StockModel stock;
  RegisterScreenState registerScreenState;

  FancyFab({this.context, this.stock, this.registerScreenState});

  @override
  _FancyFabState createState() => _FancyFabState(
      context: this.context,
      registerScreenState: this.registerScreenState,
      stock: this.stock);
}

class _FancyFabState extends State<FancyFab>
    with SingleTickerProviderStateMixin {
  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  BuildContext context;
  RegisterScreenState registerScreenState;
  StockModel stock;

  _FancyFabState({this.context, this.registerScreenState, this.stock});

  @override
  initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500))
          ..addListener(() {
            setState(() {});
          });
    _animateIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _buttonColor = ColorTween(
      begin: Colors.blue,
      end: Colors.red,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));
    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget validate(context) {
    return Container(
      child: FloatingActionButton(
        heroTag: 'validate',
        onPressed: () {
          Scaffold.of(context).openEndDrawer();
        },
        tooltip: 'Validate',
        child: Icon(Icons.shopping_cart),
      ),
    );
  }

  Widget scan() {
    return Container(
      child: FloatingActionButton(
        heroTag: 'scan',
        onPressed: () {
          scanArticle().then((String barcode) {
            _showResultScanDialog(barcode);
          });
        },
        tooltip: 'Scan',
        child: Icon(Icons.photo_camera),
      ),
    );
  }

  Widget enter() {
    return Container(
      child: FloatingActionButton(
        heroTag: 'enter',
        onPressed: () {
          _askCodeDialog();
        },
        tooltip: 'Enter',
        child: Icon(Icons.code),
      ),
    );
  }

  Widget reset() {
    return Container(
      child: FloatingActionButton(
        heroTag: 'close',
        onPressed: () {
          Navigator.of(context).pushNamed("/close");
        },
        tooltip: 'close',
        child: Icon(Icons.restore),
      ),
    );
  }

  Widget toggle() {
    return Container(
      child: FloatingActionButton(
        heroTag: 'toggle',
        backgroundColor: _buttonColor.value,
        onPressed: animate,
        tooltip: 'Toggle',
        child: AnimatedIcon(
          icon: AnimatedIcons.menu_close,
          progress: _animateIcon,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 4.0,
            0.0,
          ),
          child: validate(this.context),
        ),
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 3.0,
            0.0,
          ),
          child: enter(),
        ),
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 2.0,
            0.0,
          ),
          child: scan(),
        ),
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value,
            0.0,
          ),
          child: reset(),
        ),
        toggle(),
      ],
    );
  }

  void _showResultScanDialog(code) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Scan"),
          content: new Text(code),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Ajouter"),
              onPressed: code == null
                  ? null
                  : () {
                      try {
                        ProductModel p = this.stock.getProductById(code);
                        var added = this.registerScreenState.cart.addProduct(p);
                        this.registerScreenState.setState(() {});
                        Navigator.of(context).pop();
                      } catch (e) {}
                    },
            ),
          ],
        );
      },
    );
  }

  void _askCodeDialog() {
    // flutter defined function

    String _error;

    TextEditingController _codeController = new TextEditingController();
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          contentPadding: const EdgeInsets.all(16.0),
          title: new Text("Entrer le code"),
          content: new Row(
            children: <Widget>[
              new Expanded(
                child: new TextField(
                  autofocus: true,
                  controller: _codeController,
                  decoration:
                      new InputDecoration(labelText: 'Code', hintText: ''),
                  cursorColor: Colors.blue,
                ),
              )
            ],
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Ajouter"),
              onPressed: () {
                try {
                  ProductModel p =
                      this.stock.getProductById(_codeController.text);
                  var added = this.registerScreenState.cart.addProduct(p);
                  this.registerScreenState.setState(() {});
                  Navigator.of(context).pop();
                } catch (e) {
                  _error = e.toString();
                }
              },
            ),
          ],
        );
      },
    );
  }
}
