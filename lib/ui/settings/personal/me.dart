import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ressources/seller_api_provider.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/utils/StringsManager.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PersonalContactSettingsPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const PersonalContactSettingsPage({Key key, this.observer}) : super(key: key);

  @override
  PersonalContactSettingsPageState createState() {
    return new PersonalContactSettingsPageState(this.observer);
  }
}

class PersonalContactSettingsPageState
    extends State<PersonalContactSettingsPage> {
  final FirebaseAnalyticsObserver observer;

  final FocusNode emailFocus = FocusNode();
  final FocusNode phoneNumberFocus = FocusNode();
  final FocusNode displayNameFocus = FocusNode();

  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController displayNameController = new TextEditingController();

  PersonalContactSettingsPageState(this.observer);

  @override
  void dispose() {
    super.dispose();
    emailFocus.dispose();
    phoneNumberFocus.dispose();
    displayNameFocus.dispose();

    emailController.dispose();
    phoneController.dispose();
    displayNameController.dispose();
  }

  void _initTextController(User user) {
    emailController.text = user.email;
    phoneController.text = user.phoneNumber;
    displayNameController.text = user.displayName;
  }

  @override
  void initState() {
    super.initState();

    SellerProvider.getMe().then((User user) {
      _initTextController(user);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.transparent,
          title: new Text(StringManager.of(context).contact),
        ),
        body: new Container(
            child: Column(children: <Widget>[
          new Expanded(
            child: new ListView(
              children: <Widget>[
                buildText(
                    displayNameFocus,
                    displayNameController,
                    TextInputType.text,
                    FontAwesomeIcons.user,
                    "Nom",
                    false,
                    null),
                _buildSeparator(),
                buildText(
                    emailFocus,
                    emailController,
                    TextInputType.emailAddress,
                    FontAwesomeIcons.envelope,
                    StringManager.of(context).email,
                    false,
                    null),
                _buildSeparator(),
                buildText(
                  phoneNumberFocus,
                  phoneController,
                  TextInputType.emailAddress,
                  FontAwesomeIcons.solidEnvelope,
                  StringManager.of(context).email,
                  false,
                  null,
                ),
                _buildSeparator()
              ],
            ),
          ),
          buildButton(0.0, "Modifier", () {
            SellerProvider.updateMe({
              "phoneNumber": phoneController.text,
              "email": emailController.text,
              "displayName": displayNameController.text
            }).then((User user) => Navigator.pop(context));
          })
        ])));
  }

  Widget _buildSeparator() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: buildSeparator(),
    );
  }
}
