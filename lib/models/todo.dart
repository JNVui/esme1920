import 'dart:convert';
import 'dart:io';

import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/auth.dart';
import 'package:cash_register_app/utils/map_utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

class Todos {
  Todos({this.todos});

  List<Todo> todos;

  factory Todos.fromJson(List<dynamic> json) {
    return Todos(todos: List.from(json.map((s) => Todo.fromJson(s))));
  }

  Map<String, dynamic> toJson() {
    return {
      'todos': List.from(this.todos.map((Todo todo) => todo.toJson())),
    };
  }

}

class Todo {
  Todo(
      {this.id,
      this.title,
      this.description,
      this.deadline,
      this.isOnline,
      this.tasks}) {
    this.uuid = new Uuid().v1();
  }

  String uuid;
  String id;
  String title;
  String description;
  bool isOnline;
  DateTime deadline;
  List<Task> tasks;

  double percentComplete() {
    if (tasks.isEmpty) {
      return 1.0;
    }
    int completed = 0;
    tasks.forEach((task) {
      if (task.isCompleted()) {
        completed++;
      }
    });
    return completed / this.tasks.length;
  }

  factory Todo.fromJson(Map<String, dynamic> json) {
    return Todo(
        id: json["_id"],
        title: json["title"],
        description: json["description"],
        isOnline: json["isOnline"],
        deadline: json["deadline"],
        tasks:
            List<Task>.from(json["tasks"].map((task) => Task.fromJson(task))));
  }

  Map<String, dynamic> toJson() {
    return {
      'title': this.title,
      'description': this.description,
      'isOnline': this.isOnline,
      'deadline': this.deadline?.toIso8601String(),
      'tasks': List.from(tasks.map((Task task) => task.toJson()))
    };
  }

}

class Task {
  Task({
    this.id,
    this.title,
    this.description,
    this.deadline,
    this.isDone,
  });

  String id;
  String title;
  String description;
  DateTime deadline;
  bool isDone;
  String uuid = new Uuid().v1();

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        isDone: json["isDone"],
        deadline:
            json["deadline"] == null ? null : DateTime.parse(json["deadline"]));
  }

  Map<String, dynamic> toJson() {
    return Map<String, dynamic>.from(cleanNull({
      'title': this.title,
      'description': this.description,
      'isDone': this.isDone,
      'deadline': this.deadline?.toIso8601String(),
    }));
  }

  void setComplete(bool value) {
    isDone = value;
  }

  bool isCompleted() {
    return isDone != null ? isDone : false;
  }
}
