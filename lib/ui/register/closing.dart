import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/ressources/cash_fund_api_provider.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ClosingPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  ClosingPage({this.observer});

  @override
  ClosingPageState createState() {
    return new ClosingPageState(this.observer);
  }
}

class ClosingPageState extends State<ClosingPage> {
  Future<Sales> _futureSales;
  Future<double> _futureCashFund;
  final FirebaseAnalyticsObserver observer;

  ClosingPageState(this.observer);

  @override
  void initState() {
    _futureSales = CompanyProvider.closeSales();
    _futureCashFund = CashFundProvider.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Theme.Colors.loginGradientStart,
                Theme.Colors.loginGradientEnd
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: new AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Text("Fermeture"),
          ),
          body: _build(),
        ));
  }

  Widget _build() {
    return Center(
        child: Container(
            padding: EdgeInsets.only(top: 23.0),
            child: Stack(
                alignment: Alignment.topCenter,
                overflow: Overflow.visible,
                children: <Widget>[
                  Card(
                      elevation: 2.0,
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Container(
                          width: 300.0,
                          height: 450.0,
                          child: _buildBodyFuture())),
                  buildButton(440.0, "Fermer", () {
                    _close();
                  })
                ])));
  }

  Widget _buildBodyFuture() {
    return FutureBuilder(
      future: _futureSales,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Press button to start.');
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            Sales sales = snapshot.data;
            return _buildBody(sales);
        }
      },
    );
  }

  Widget _buildBody(Sales sales) {
    return ListView(
      children: <Widget>[
        ListTile(
          title: Text("Nombre de ventes"),
          trailing: Text(sales.sales.length.toString()),
        ),
        Divider(),
        ListTile(
          title: Text(
            "Total",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          trailing: Text(sales.total.toStringAsFixed(2)),
        ),
        ListTile(
          title: Text("Espèces"),
          trailing: Text(sales.totalCash.toStringAsFixed(2)),
          leading: Icon(FontAwesomeIcons.moneyBillAlt),
        ),
        ListTile(
          title: Text("Carte"),
          trailing: Text(sales.totalCB.toStringAsFixed(2)),
          leading: Icon(FontAwesomeIcons.creditCard),
        ),
        ListTile(
          title: Text("GiftCard"),
          trailing: Text(sales.totalGiftCard.toStringAsFixed(2)),
          leading: Icon(FontAwesomeIcons.gift),
        ),
        ListTile(
          title: Text("Chèque"),
          trailing: Text(sales.totalCheck.toStringAsFixed(2)),
          leading: Icon(FontAwesomeIcons.moneyCheck),
        ),
        Divider(),
        _buildFondDeCaisse()
      ],
    );
  }

  Widget _buildFondDeCaisse() {
    return FutureBuilder(
      future: _futureCashFund,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Press button to start.');
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            double total = snapshot.data;
            return ListTile(
                title: Text("Fond de caisse"),
                trailing: Text(total.toStringAsFixed(2)));
        }
      },
    );
  }

  void _close() async {
    String token = await LoginSharedPreferences.getResisterIsOpen();
    Sales sales = await CompanyProvider.close(token);
    print(sales);
    LoginSharedPreferences.setRegisterIsOpen(null)
        .then((bool sucess) => Navigator.of(context).pop());
  }
}
