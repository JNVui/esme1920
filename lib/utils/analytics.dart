import 'package:firebase_analytics/observer.dart';

void sendCurrentPageToAnalytics(
    FirebaseAnalyticsObserver observer, String routeName) {
  print("[$routeName] Send Analytics event from ");
  observer.analytics.setCurrentScreen(
    screenName: routeName,
  );
}
