import 'dart:async';

import 'package:cash_register_app/models/categories.dart';
import 'package:cash_register_app/ui/categories/edit_or_create_category.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DisplayOrSelectCategoriesPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  final String currentCategoryId;
  final bool isHome;

  DisplayOrSelectCategoriesPage({this.currentCategoryId, this.isHome, this.observer});

  @override
  DisplayOrSelectCategoriesPageState createState() {
    return new DisplayOrSelectCategoriesPageState(this.observer,
        currentCategoryId: this.currentCategoryId, isHome: this.isHome);
  }
}

class DisplayOrSelectCategoriesPageState
    extends State<DisplayOrSelectCategoriesPage> {
  final FirebaseAnalyticsObserver observer;

  Future<Categories> _categoriesFuture;
  final String currentCategoryId;
  bool isHome;

  DisplayOrSelectCategoriesPageState(this.observer, {this.currentCategoryId, this.isHome});

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    observer.analytics.setCurrentScreen(screenName: "/category/display");

    _categoriesFuture = Categories.fetchWithHierarchy();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: buildBody(_categoriesFuture));
  }

  FutureBuilder buildBody(Future<Categories> _categoriesFuture) {
    return FutureBuilder(
      future: _categoriesFuture,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            Categories categories = snapshot.data;
            return Column(
              children: <Widget>[
                _categoriesView(
                    categories.entries, this.currentCategoryId, isHome, observer),
                isHome
                    ? Container()
                    : Card(
                        child: buildButton(
                          0.0,
                          "Créer une catégorie",
                          () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditOrCreateCategory(
                                        isModifying: false, observer: observer,)),
                              ),
                        ),
                        elevation: 10,
                      )
              ],
            );
        }
      },
    );
  }
}

Widget _categoriesView(
    List<Category> categories, String currentCategoryId, bool isHome, observer,
    {Function clickFunction}) {
  if (!isHome) categories.add(Category(name: "Aucune", id: null));
  return Flexible(
      child: Container(
          child: ListView.builder(
    itemBuilder: (BuildContext context, int index) => EntryItem(
        categories[index], context, currentCategoryId, isHome,
        observer: observer,
        clickFunction: clickFunction),
    itemCount: categories.length,
  )));
}

class EntryItem extends StatelessWidget {
  final Category category;
  final String currentCategoryId;
  final BuildContext context;
  final Function clickFunction;
  final bool isHome;
  final FirebaseAnalyticsObserver observer;


  EntryItem(this.category, this.context, this.currentCategoryId, this.isHome,
      {this.clickFunction, this.observer});

  Widget _buildTiles(Category root) {
    if ((root.id == currentCategoryId) & (root.id != null)) return Container();
    if (!root.hasChildren) return _buildLastLeaf(root);
    if (root.children.length == 1 &&
        root.children.values.toList()[0].id == currentCategoryId)
      return _buildLastLeaf(root);
    return _buildExpansionTile(root);
  }

  Widget _buildExpansionTile(Category root) {
    return ExpansionTile(
      key: PageStorageKey<Category>(root),
      title: Text(
        root.displayName,
        style: TextStyle(),
      ),
      children: List<Widget>.from(root.children.values.map(_buildTiles)),
      leading: isHome
          ? IconButton(
              icon: Icon(FontAwesomeIcons.edit),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new EditOrCreateCategory(
                            category: root,
                            isModifying: true,
                            observer: observer,
                          ))))
          : IconButton(
              icon: Icon(FontAwesomeIcons.check),
              onPressed: () => Navigator.of(context).pop(root)),
    );
  }

  Widget _buildLastLeaf(Category root) {
    return ListTile(
      title: Text(root.displayName),
      onTap: isHome
          ? null
          : () => clickFunction == null
              ? Navigator.of(context).pop(root)
              : clickFunction,
      leading: (isHome & (root.id != null))
          ? IconButton(
              icon: Icon(FontAwesomeIcons.edit),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new EditOrCreateCategory(
                            category: root,
                            isModifying: true,
                            observer: observer,
                          ))))
          : null,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color: Colors.white,
          ),
          child: _buildTiles(category),
        ));
  }
}
