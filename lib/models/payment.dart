import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum PaymentTypeValues { creditCard, cash, giftCard, check, sumUp, lydia,  payPal}

class Payment{
  final PaymentTypeValues _paymentType;
  final double _total;

  Payment(this._paymentType, this._total);


  double get total => _total;
  PaymentTypeValues get paymentType => _paymentType;

  factory Payment.fromJson(Map<String, dynamic> json) {
    return Payment(json['type'],json['total']);
  }

  Map<String, dynamic> toJson() {
    return {
      'type': _paymentType.toString(),
      'total': _total,
    };
  }

}

class Payments{
  List<Payment> payments;

  Payments({this.payments}){
    if (this.payments == null){
      this.payments = [];
    }
  }

  double get total{
    return this.payments.fold(0, (tot, Payment s2) => tot + s2.total);
  }

  factory Payments.fromJson(Map<String, dynamic> json) {
    return Payments(payments:List<Payment>.from(json["payments"].map((payment) => Payment.fromJson(payment))));
  }

  Map<String, dynamic> toJson() {
    return {
      'payments': this.payments.map((Payment payment) => payment.toJson()),
    };
  }

  void addPayment(Payment payment){
    this.payments.add(payment);
  }

}


PaymentTypeValues paymentFromString(String paymentString) {
  try {
    PaymentTypeValues payment =
        PaymentTypeValues.values.firstWhere((e) => e.toString() == paymentString);
    return payment;
  } catch (e) {
    return null;
  }
}

Icon getIconFromPayment(PaymentTypeValues p, {color: Colors.white, size:25.0}) {
  switch (p) {
    case PaymentTypeValues.cash:
      return Icon(FontAwesomeIcons.moneyBill, color: color, size: size);

    case PaymentTypeValues.creditCard:
      return Icon(FontAwesomeIcons.creditCard, color: color, size: size);

    case PaymentTypeValues.giftCard:
      return Icon(FontAwesomeIcons.gift, color: color, size: size);

    case PaymentTypeValues.check:
      return Icon(FontAwesomeIcons.moneyCheck, color: color, size: size);

    case PaymentTypeValues.sumUp:
      return Icon(FontAwesomeIcons.solidCreditCard, color: color, size: size);

    case PaymentTypeValues.payPal:
      return Icon(FontAwesomeIcons.paypal, color: color, size: size);

    case PaymentTypeValues.lydia:
      return Icon(FontAwesomeIcons.adn, color: color, size: size);


  }
  return Icon(Icons.not_interested, color: color, size: size);
}

Widget getBulletForHistory(PaymentTypeValues p) {
  Color c = Colors.grey;
  switch (p) {
    case PaymentTypeValues.cash:
      c = Colors.blue;
      break;
    case PaymentTypeValues.creditCard:
      c = Colors.green;
      break;
    case PaymentTypeValues.giftCard:
      c = Colors.orange;
      break;

    case PaymentTypeValues.check:
      c = Colors.red;
      break;
    default:
      break;
  }
  return Material(
      color: c,
      borderRadius: BorderRadius.circular(18.0),
      child: Center(
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0), child: getIconFromPayment(p))));
}
