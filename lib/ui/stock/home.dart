import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/stock/floating.dart';
import 'package:cash_register_app/ui/widget/core_register_stock.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class StockHomeScreen extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const StockHomeScreen({Key key, this.observer}) : super(key: key);

  @override
  StockHomeState createState() => StockHomeState(this.observer);
}

class StockHomeState extends State<StockHomeScreen> {
  Future<Company> companyFuture;
  final FirebaseAnalyticsObserver observer;
  final String routeName = "/stocks";

  StockHomeState(this.observer);

  @override
  void initState() {
    super.initState();
    companyFuture = CompanyProvider.fetchStock();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: companyFuture,
        builder: (BuildContext context, AsyncSnapshot<Company> snapshot) {
          if (snapshot.hasError)
            return Text("Error Occurred ${snapshot.error}");
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              Company company = snapshot.data;
              return _mainBody(
                  company: company,
                  floatingButtonBuilder: Builder(builder: (context) {
                    return FancyFab(observer: this.observer, company: company, stockState: this);
                  }),
                  isRegister: false);
            default:
              return new Container(width: 0.0, height: 0.0);
          }
        });
  }

  Widget _mainBody({Company company, floatingButtonBuilder, bool isRegister}) {
    return new Container(
        decoration: new BoxDecoration(color: Color(0xFFF77B67)),
        child: new Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              title: Text('Stock'),
            ),
            resizeToAvoidBottomPadding: false,
            body: CoreRegisterStock(company, isRegister, observer: this.observer,),
            floatingActionButton: floatingButtonBuilder));
  }
}
