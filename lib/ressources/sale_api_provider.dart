import 'dart:convert';

import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';

class SaleProvider{
  static Future<bool> save(Sale s) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.POST, '/api/v1/me/company/$companyId/sale',
        body: json.encode(s.toJson()));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception(response.body);
    }
  }

  static Future<Sales> fetchAll() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, '/api/v1/me/company/$companyId');

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      return Sales.fromJson(result["content"]["sales"]);
    } else {
      throw Exception('Failed to load post');
    }
  }

  static Future<Sales> fetchBetweenTwoDates(
      DateTime before, DateTime after) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.GET, '/api/v1/company/$companyId/sales', queryParameters: {
      "before": before.toString(),
      "after": after.toString()
    });

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      print("Result $result");
      return Sales.fromJson(List<dynamic>.from(
          result["content"].map((dynamic sale) => sale["sales"])));
    } else {
      throw Exception(response.body);
    }
  }

  static Future<Sales> fetchTodaySales() {
    DateTime today = DateTime.now();
    return fetchBetweenTwoDates(
        DateTime(today.year, today.month, today.day, 23, 59),
        DateTime(today.year, today.month, today.day, 0, 0));
  }
}
