import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ressources/seller_api_provider.dart';
import 'package:cash_register_app/ui/base.dart';
import 'package:cash_register_app/ui/login/bubble_painter.dart';
import 'package:cash_register_app/ui/login/join_or_select_company.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/utils/analytics.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = GoogleSignIn();

class LoginPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  LoginPage({Key key, this.observer}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState(this.observer);
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin, RouteAware {
  final FirebaseAnalyticsObserver observer;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode myFocusNodeEmailLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  final FocusNode myFocusNodePassword = FocusNode();
  final FocusNode myFocusNodeEmail = FocusNode();
  final FocusNode myFocusNodeDisplayName = FocusNode();
  final FocusNode myFocusNodePhone = FocusNode();

  TextEditingController loginEmailController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  bool _obscureTextLogin = true;
  bool _obscureTextSignUp = true;
  bool _obscureTextSignUpConfirm = true;

  TextEditingController signUpEmailController = new TextEditingController();
  TextEditingController signUpPhoneController = new TextEditingController();
  TextEditingController signUpDisplayNameController =
      new TextEditingController();
  TextEditingController signUpPasswordController = new TextEditingController();
  TextEditingController signUpConfirmPasswordController =
      new TextEditingController();

  PageController _pageController;

  Color left = Colors.black;
  Color right = Colors.white;

  _LoginPageState(this.observer);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
        },
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height >= 775.0
                ? MediaQuery.of(context).size.height
                : 775.0,
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: [
                    Theme.Colors.loginGradientStart,
                    Theme.Colors.loginGradientEnd
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 120.0, bottom: 30.0),
                  child: Container(
                    child: Text(
                      "Welcome in. ",
                      style: TextStyle(
                          fontFamily: "WorkSansSemiBold",
                          fontSize: 40.0,
                          color: Colors.white),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 40.0),
                  child: _buildMenuBar(context),
                ),
                Expanded(
                  flex: 2,
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (i) {
                      if (i == 0) {
                        setState(() {
                          right = Colors.white;
                          left = Colors.black;
                        });
                      } else if (i == 1) {
                        setState(() {
                          right = Colors.black;
                          left = Colors.white;
                        });
                      }
                    },
                    children: <Widget>[
                      new ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: _buildSignIn(context),
                      ),
                      new ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: _buildSignUp(context),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    myFocusNodePassword.dispose();
    myFocusNodeEmail.dispose();
    myFocusNodeDisplayName.dispose();
    _pageController?.dispose();
    observer.unsubscribe(this);

    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    // TODO: AutoLogin
    //    LoginSharedPreferences.getToken().then((String token) {
    //      if (token != null) {
    //        final Future<User> user = getMe();
    //        user.then((User user) {
    //          Navigator.push(
    //            context,
    //            MaterialPageRoute(
    //                builder: (context) => BasePage(user, this.observer)),
    //          );
    //        }).catchError((e) {
    //          LoginSharedPreferences.setToken(null);
    //        });
    //      }
    //    });

    _pageController = PageController();
  }

  Widget _buildMenuBar(BuildContext context) {
    return Container(
      width: 300.0,
      height: 50.0,
      decoration: BoxDecoration(
        color: Color(0x552B2B2B),
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: CustomPaint(
        painter: TabIndicationPainter(pageController: _pageController),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignInButtonPress,
                child: Text(
                  "Connecter",
                  style: TextStyle(
                      color: left,
                      fontSize: 16.0,
                      fontFamily: "WorkSansSemiBold"),
                ),
              ),
            ),
            //Container(height: 33.0, width: 1.0, color: Colors.white),
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "Créer",
                  style: TextStyle(
                      color: right,
                      fontSize: 16.0,
                      fontFamily: "WorkSansSemiBold"),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSignIn(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 150.0,
                  child: Column(
                    children: <Widget>[
                      buildText(
                          myFocusNodeEmailLogin,
                          loginEmailController,
                          TextInputType.emailAddress,
                          FontAwesomeIcons.envelope,
                          "Email Address",
                          false,
                          null),
                      buildSeparator(),
                      buildText(
                          myFocusNodePasswordLogin,
                          loginPasswordController,
                          null,
                          FontAwesomeIcons.lock,
                          "Password",
                          _obscureTextLogin,
                          GestureDetector(
                            onTap: _toggleLogin,
                            child: Icon(
                              FontAwesomeIcons.eye,
                              size: 15.0,
                              color: Colors.black,
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              buildButton(140.0, "LOGIN", () {
                _handleLogin();
              })
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: FlatButton(
                onPressed: () {
                  try {
                    if ((loginEmailController.text != null) &
                        (loginEmailController.text != "")) {
                      _auth
                          .sendPasswordResetEmail(
                              email: loginEmailController.text)
                          .then((dynamic s) => showInSnackBar(_scaffoldKey,
                              context, "Un email vient de vous être envoyé"));
                    } else {
                      showInSnackBar(
                          _scaffoldKey, context, "Veuillez renseigner un mail");
                    }
                  } on PlatformException catch (e) {
                    print("ERROR");
                    showInSnackBar(_scaffoldKey, context, e.message.toString());
                  }
                },
                child: Text(
                  "Forgot Password?",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: 16.0,
                      fontFamily: "WorkSansMedium"),
                )),
          ),
          //_buildOr(),
          //_buildSocialSignIn()
        ],
      ),
    );
  }

  Widget _buildSignUp(BuildContext context) {
    double height = MediaQuery.of(context).size.height * 0.50;
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  padding: EdgeInsets.only(bottom: 10.0),
                  width: 300.0,
                  height: height,
                  child: ListView(
                    padding: EdgeInsets.all(0.0),
                    children: <Widget>[
                      Column(children: [
                        buildText(
                            myFocusNodeDisplayName,
                            signUpDisplayNameController,
                            TextInputType.text,
                            FontAwesomeIcons.user,
                            "DisplayName",
                            false,
                            null),
                        buildSeparator()
                      ]),
                      buildSeparator(),
                      buildText(
                          myFocusNodeEmail,
                          signUpEmailController,
                          TextInputType.emailAddress,
                          FontAwesomeIcons.envelope,
                          "Email Address",
                          false,
                          null),
                      buildSeparator(),
                      buildText(
                          myFocusNodePhone,
                          signUpPhoneController,
                          TextInputType.phone,
                          FontAwesomeIcons.phone,
                          "Phone",
                          false,
                          null),
                      buildSeparator(),
                      buildText(
                        myFocusNodePassword,
                        signUpPasswordController,
                        TextInputType.text,
                        FontAwesomeIcons.lock,
                        "Password",
                        _obscureTextSignUp,
                        GestureDetector(
                          onTap: _toggleSignup,
                          child: Icon(
                            FontAwesomeIcons.eye,
                            size: 15.0,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      buildSeparator(),
                      buildText(
                        null,
                        signUpConfirmPasswordController,
                        TextInputType.text,
                        FontAwesomeIcons.lock,
                        "Confirmation",
                        _obscureTextSignUpConfirm,
                        GestureDetector(
                          onTap: _toggleSignupConfirm,
                          child: Icon(
                            FontAwesomeIcons.eye,
                            size: 15.0,
                            color: Colors.black,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              buildButton(height - 10, "SIGN UP", () {
                _registerUser();
                _onSignInButtonPress();
              }),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildOr() {
    return Padding(
        padding: EdgeInsets.only(top: 10.0),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: new LinearGradient(
                  colors: [
                    Colors.white10,
                    Colors.white,
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            width: 100.0,
            height: 1.0,
          ),
          Padding(
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: Text(
              "Or",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontFamily: "WorkSansMedium"),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              gradient: new LinearGradient(
                  colors: [
                    Colors.white,
                    Colors.white10,
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            width: 100.0,
            height: 1.0,
          )
        ]));
  }

  Future<bool> _saveTokens(FirebaseUser fuser) async {
    String token = await fuser.getIdToken();
    return LoginSharedPreferences.setToken(token);
  }

  void _afterAuthUser(FirebaseUser firebaseUser, User user) async {
    observer.analytics.setUserId(firebaseUser.uid);
    _saveTokens(firebaseUser).then((bool success) {
      if (user.companiesId == null || user.companiesId.isEmpty) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => JoinOrCreateCompany(user, this.observer)),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BasePage(user, this.observer)),
        );
      }
    });
  }

  void _handleLogin() async {
    try {
      final FirebaseUser firebaseUser = await _auth.signInWithEmailAndPassword(
        email: loginEmailController.text,
        password: loginPasswordController.text,
      );

      if (firebaseUser == null)
        showInSnackBar(_scaffoldKey, context, "Failed to login");
      else {
        try {

          print("Before auth");
          final User user = await SellerProvider.authUser({
            'email': loginEmailController.text,
            'password': loginPasswordController.text
          });

          print("User $user");

          _afterAuthUser(firebaseUser, user);
        } catch (e) {
          showInSnackBar(_scaffoldKey, context, e.toString());
        }
      }
    } on PlatformException catch (e) {
      showInSnackBar(_scaffoldKey, context, e.message.toString());
    }
  }

  void _registerUser() async {
    try {
      final FirebaseUser user = await _auth.createUserWithEmailAndPassword(
        email: signUpEmailController.text,
        password: signUpPasswordController.text,
      );

      if (user != null) {
        final Future<User> successFuture = SellerProvider.createUser({
          'displayName': signUpDisplayNameController.text,
          'email': signUpEmailController.text,
          'phoneNumber': signUpPhoneController.text,
          'password': signUpPasswordController.text,
          'firebaseId': user.uid,
          'provider': user.providerId
        });

        UserUpdateInfo userInfo = new UserUpdateInfo();
        userInfo.displayName = signUpDisplayNameController.text;
        user.updateProfile(userInfo);
        successFuture.then((success) {
          if (successFuture != null) {
            _onSignUpButtonPress();
          } else {
            showInSnackBar(_scaffoldKey, context, 'Already used');
          }
        });
      } else {
        showInSnackBar(_scaffoldKey, context, "Failed to create");
      }
    } on PlatformException catch (e) {
      showInSnackBar(_scaffoldKey, context, e.message.toString());
    }
  }

  void _onSignInButtonPress() {
    sendCurrentPageToAnalytics(observer, "/login/signIn");
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _onSignUpButtonPress() {
    sendCurrentPageToAnalytics(observer, "/login/login");
    _pageController?.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  void _toggleSignup() {
    setState(() {
      _obscureTextSignUp = !_obscureTextSignUp;
    });
  }

  void _toggleSignupConfirm() {
    setState(() {
      _obscureTextSignUpConfirm = !_obscureTextSignUpConfirm;
    });
  }
}
