import 'dart:io';

import 'package:cash_register_app/utils/auth.dart';
import 'package:http/http.dart' as http;

enum METHOD { GET, POST, PUT, DELETE }
//const apiUrl = "10.0.2.2:6789"; //ANDROID
//const apiUrl = "padang.justech.fr:6789";
//const apiUrl = "192.168.191.255:6789";
const apiUrl = "localhost:6789"; //IOS
// const apiUrl = "192.168.187.130:6789"; //LOCALHOST

Future<http.Response> makeRequest(METHOD method, String path,
    {body, queryParameters, retry = 1, useToken=true}) async {

  var headers = {HttpHeaders.contentTypeHeader: "application/json"};

  if (useToken){
    String token = await getCurrentUserToken();
    headers[HttpHeaders.authorizationHeader] = "Bearer " + token;
  }

  http.Response response;
  switch (method) {
    case METHOD.GET:
      response = await http.get(new Uri.http(apiUrl, path, queryParameters),
          headers: headers);
      break;
    case METHOD.POST:
      response = await http.post(new Uri.http(apiUrl, path),
          body: body, headers: headers);
      break;
    case METHOD.PUT:
      response = await http.put(new Uri.http(apiUrl, path),
          body: body, headers: headers);
      break;
    case METHOD.DELETE:
      response =
          await http.delete(new Uri.http(apiUrl, path), headers: headers);
      break;
    default:
      response = await http.get(new Uri.http(apiUrl, path), headers: headers);
      break;
  }

  if (response.statusCode == 200) {
    return response;
  }
  if (response.statusCode == 401) {
    if (retry > 0) {
      return refreshToken().then((bool success) =>
          makeRequest(method, path, body: body, retry: retry - 1));
    } else
      return response;
  }
  return response;
}
