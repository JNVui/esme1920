import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:flutter/material.dart';

Widget buildText(
    focus, controller, textInputType, icon, hint, obscureText, suffixIcon) {
  return Padding(
    padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 25.0, right: 25.0),
    child: TextField(
      focusNode: focus,
      controller: controller,
      keyboardType: textInputType,
      obscureText: obscureText,
      style: TextStyle(
          fontFamily: "WorkSansSemiBold", fontSize: 16.0, color: Colors.black),
      decoration: InputDecoration(
          border: InputBorder.none,
          labelText: hint,
          icon: Icon(
            icon,
            color: Colors.black,
            size: 22.0,
          ),
          hintText: hint,
          hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 17.0),
          suffixIcon: suffixIcon),
    ),
  );
}

Widget buildRowText(
    focus, controller, textInputType, icon, hint, obscureText, suffixIcon) {
  return Padding(
    padding: EdgeInsets.only(top: 15.0, bottom: 15.0, left: 10.0, right: 10.0),
    child: TextField(
      focusNode: focus,
      controller: controller,
      keyboardType: textInputType,
      obscureText: obscureText,
      style: TextStyle(
          fontFamily: "WorkSansSemiBold", fontSize: 16.0, color: Colors.black),
      decoration: InputDecoration(
          labelText: hint,
          border: InputBorder.none,
          hintText: hint,
          hintStyle: TextStyle(fontFamily: "WorkSansSemiBold", fontSize: 17.0),
          suffixIcon: suffixIcon),
    ),
  );
}

Widget buildSeparator() {
  return Container(
    width: 250.0,
    height: 1.0,
    color: Colors.grey[400],
  );
}

Widget buildButton(marginTop, text, onPressed) {
  return Container(
    margin: EdgeInsets.only(top: marginTop as double),
    decoration: new BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Theme.Colors.loginGradientStart,
          offset: Offset(1.0, 6.0),
          blurRadius: 20.0,
        ),
        BoxShadow(
          color: Theme.Colors.loginGradientEnd,
          offset: Offset(1.0, 6.0),
          blurRadius: 20.0,
        ),
      ],
      gradient: new LinearGradient(
          colors: [
            Theme.Colors.loginGradientEnd,
            Theme.Colors.loginGradientStart
          ],
          begin: const FractionalOffset(0.2, 0.2),
          end: const FractionalOffset(1.0, 1.0),
          stops: [0.0, 1.0],
          tileMode: TileMode.clamp),
    ),
    child: MaterialButton(
        highlightColor: Colors.transparent,
        splashColor: Theme.Colors.loginGradientEnd,
        //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 42.0),
          child: Text(
            text,
            style: TextStyle(
                color: Colors.white,
                fontSize: 25.0,
                fontFamily: "WorkSansBold"),
          ),
        ),
        onPressed: onPressed),
  );
}

void showInSnackBar(
    GlobalKey<ScaffoldState> scaffoldKey, BuildContext context, String value) {
  FocusScope.of(context).requestFocus(new FocusNode());
  scaffoldKey.currentState?.removeCurrentSnackBar();
  scaffoldKey.currentState.showSnackBar(new SnackBar(
    content: new Text(
      value,
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.white, fontSize: 16.0, fontFamily: "WorkSansSemiBold"),
    ),
    backgroundColor: Colors.blue,
    duration: Duration(seconds: 3),
  ));
}
