import 'dart:convert';

import 'package:cash_register_app/models/stats.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/auth.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:charts_flutter/flutter.dart' as charts;


class StatsProvider{

  /// Create a Company
  static Future<List<dynamic>> getCaByProduct() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();
    final response = await makeRequest(
        METHOD.GET, 'api/v1/company/$companyId/stats/ca/products');

    if (response.statusCode == 200) {
      List<dynamic> result = json.decode(response.body)["result"];
      return result;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<List<double>> getCAOverMonth() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, 'api/v1/company/$companyId/stats/month');

    if (response.statusCode == 200) {
      List<dynamic> result = json.decode(response.body)["result"];
      result.forEach((dynamic item) => item["date"] = new DateTime(
          item["_id"]["year"], item["_id"]["month"], item["_id"]["day"]));
      result.sort((a, b) => a["date"].difference(b["date"]).inDays);
      return List<double>.from(result.map((item) => item["total"].toDouble()));
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<List<double>> getCAOverYear() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, 'api/v1/company/$companyId/stats/year');

    if (response.statusCode == 200) {
      List<dynamic> result = json.decode(response.body)["result"];
      result.forEach((dynamic item) => item["date"] = new DateTime(
          item["_id"]["year"], item["_id"]["month"], item["_id"]["day"]));
      result.sort((a, b) => a["date"].difference(b["date"]).inDays);
      return List<double>.from(result.map((item) => item["total"].toDouble()));
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<List<double>> getCAOverWeek() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, 'api/v1/company/$companyId/stats/week');

    if (response.statusCode == 200) {
      List<dynamic> result = json.decode(response.body)["result"];
      result.forEach((dynamic item) => item["date"] = new DateTime(
          item["_id"]["year"], item["_id"]["month"], item["_id"]["day"]));
      result.sort((a, b) => a["date"].difference(b["date"]).inDays);
      return List<double>.from(result.map((item) => item["total"].toDouble()));
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<Map<String, dynamic>> getBasicStatistics() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, 'api/v1/company/$companyId/stats/');

    if (response.statusCode == 200) {
      Map<String, dynamic> data =
      Map<String, dynamic>.from(json.decode(response.body)["content"][0]);
      return data;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<List<charts.Series<CaByProducts, String>>> getSeriesCaProduct() async {
    List<dynamic> results = await getCaByProduct();
    return getSeriesCaProductFromResult(List<CaByProducts>.from(results.map(
            (dynamic item) => CaByProducts(
            item["item"] == null ? null : item["item"]["name"],
            item["totalCA"],
            item["averageQuantity"]))));
  }


}