import 'package:shared_preferences/shared_preferences.dart';

class LoginSharedPreferences {
  static final String _kToken = "token";
  static final String _kId = "id";
  static final String _kCurrentCompanyId = "currentCompanyId";
  static final String _kIsSumUpLogin = "isSumUpLogin";
  static final String _kisPaypal = "paypal";
  static final String _kisLydia = "lydia";
  static final String _kRegisterIsOpen = "registerIdOpen";

  static Future<String> getId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_kId);
  }

  static Future<bool> setId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_kId, value);
  }

  static Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.getString(id + _kToken);
  }

  static Future<bool> setIsPaypalEnable(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.setBool(id + _kisPaypal, value);
  }

  static Future<bool> isPaypalEnable() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    try {
      return prefs.getBool(id + _kisPaypal);
    } catch (e) {
      //default value
      setIsPaypalEnable(false);
      return false;
    }
  }

  static Future<bool> setIsLydiaEnable(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.setBool(id + _kisLydia, value);
  }

  static Future<bool> isLydiaEnable() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    try {
      return prefs.getBool(id + _kisLydia);
    } catch (e) {
      //default value
      setIsLydiaEnable(false);
      return false;
    }
  }

  static Future<bool> setToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.setString(id + _kToken, value);
  }

  static Future<String> getCurrentCompanyId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.getString(id + _kCurrentCompanyId);
  }

  static Future<bool> setCurrentCompanyId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.setString(id + _kCurrentCompanyId, value);
  }

  static Future<bool> getClientId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.setString(id + _kCurrentCompanyId, value);
  }

  static Future<bool> setIsSumUpEnable(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.setBool(id + _kIsSumUpLogin, value);
  }

  static Future<bool> isSumUpEnable() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    try {
      return prefs.getBool(id + _kIsSumUpLogin) ?? false;
    } catch (e) {
      //default value
      setIsSumUpEnable(false);
      return false;
    }
  }

  static String getSumupId() {
    return "IWbGDJUpmdzHLjqyGUMsqQu66tJt";
  }

  static String getSumupSecretId() {
    return "c0b3f54c74fb33fba58a9b9a74f244f5719acbb25dfccd491925fb0c6ea50cc8";
  }

  static Future<bool> setRegisterIsOpen(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    return prefs.setString(id + _kRegisterIsOpen, value);
  }

  static Future<String> getResisterIsOpen() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String id = await getId();

    try {
      return prefs.getString(id + _kRegisterIsOpen);
    } catch (e) {
      return null;
    }
  }

  static Future<bool> clear() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.clear();
  }
}
