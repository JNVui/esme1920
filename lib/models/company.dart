import 'package:cash_register_app/models/categories.dart';
import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/models/stock.dart';
import 'package:cash_register_app/models/tax.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class Company {
  final String id;
  String name;
  String siret;
  String taxNumber;
  List<Sale> sales;
  List<dynamic> items;
  StockModel stock;
  List<dynamic> sellers;
  Categories categories;
  Contact contact;
  String activityType;
  Location location;
  Finance finance;

  Company(
      {this.id,
      this.name,
      this.siret,
      this.taxNumber,
      this.sales,
      this.items,
      this.sellers,
      this.stock,
      this.categories,
      this.contact,
      this.finance,
      this.activityType,
      this.location});

  factory Company.fromJson(Map<String, dynamic> json) {
    print("factory Company.fromJson $json");
    return Company(
        id: json['_id'],
        name: json['name'],
        siret: json['siret'],
        taxNumber: json['taxNumber'],
        location: json["location"] == null
            ? new Location()
            : Location.fromJson(json["location"]),
        contact: json["contact"] == null
            ? new Contact()
            : Contact.fromJson(json["contact"]),
        finance: json["finance"] == null
            ? new Finance()
            : Finance.fromJson(json["finance"]),
        activityType: json['activityType'],
        sales: json['sales'] == null
            ? null
            : List.from(json['sales'].map((sale) => Sale.fromJson(sale))),
        items: json['items'] == null ? null : json['items'],
        stock: json['stockItems'] != null
            ? StockModel.fromJson(json['stockItems'], json["_id"])
            : null,
        sellers: json['items'] == null ? null : json['sellers']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'siret': id ?? siret,
      'taxNumber': taxNumber,
      'sales': sales,
      'items': items,
      'location': location.toJson(),
      'contact': contact.toJson(),
      'finance': finance.toJson(),
    };
  }

  Map<String, dynamic> toCreateJson() {
    return {
      'name': name,
      'siret': id ?? siret,
      'taxNumber': taxNumber,
    };
  }

  static Map<String, dynamic> updatePartialJson({
    id,
    name,
    siret,
    taxNumber,
    Location location,
    Finance finance,
    Contact contact,
    activityType,
  }) {
    Map<String, dynamic> partialMap = new Map();
    if (id != null && (id != '')) partialMap.putIfAbsent('id', () => id);
    if (name != null && (name != ''))
      partialMap.putIfAbsent('name', () => name);
    if (siret != null && (siret != ''))
      partialMap.putIfAbsent('siret', () => siret);
    if (taxNumber != null && (taxNumber != ''))
      partialMap.putIfAbsent('taxNumber', () => taxNumber);

    if (location != null) partialMap.putIfAbsent('location', () => location);
    if (contact != null) partialMap.putIfAbsent('contact', () => contact);

    if (activityType != null && (activityType != ''))
      partialMap.putIfAbsent('activityType', () => activityType);

    return partialMap;
  }
}

class Location {
  String address;
  String zipCode;
  String city;
  String country;
  double latitude;
  double longitude;

  Location({
    this.address,
    this.zipCode,
    this.city,
    this.country,
    this.latitude,
    this.longitude,
  });

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      address: json['address'],
      zipCode: json['zipCode'],
      city: json['city'],
      latitude: json['latitude'],
      longitude: json['longitude'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'address': address,
      'zipCode': zipCode,
      'city': city,
      'latitude': latitude,
      'longitude': longitude,
    };
  }
}

class Contact {
  String email;
  String phoneNumber;
  String website;

  Contact({this.email, this.phoneNumber, this.website});

  factory Contact.fromJson(Map<String, dynamic> json) {
    return Contact(
      email: json['email'],
      phoneNumber: json['phoneNumber'],
      website: json['website'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'phoneNumber': phoneNumber,
      'website': website,
    };
  }
}

class Finance {
  String accountantEmail;
  String closeEmails;
  TAX tax;

  Finance({this.accountantEmail, this.closeEmails, this.tax});

  factory Finance.fromJson(Map<String, dynamic> json) {
    return Finance(
      accountantEmail: json['accountantEmail'],
      closeEmails: json['closeEmails'],
      tax: taxFromDouble(json['tax']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'accountantEmail': accountantEmail,
      'closeEmails': closeEmails,
      'tax': taxToDouble(tax),
    };
  }
}
