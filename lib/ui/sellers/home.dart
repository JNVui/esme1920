import 'package:cash_register_app/models/customer.dart';
import 'package:cash_register_app/ressources/customer_api_provider.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class SellersPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const SellersPage({Key key, this.observer}) : super(key: key);

  @override
  SellersPageState createState() {
    return new SellersPageState(this.observer);
  }
}

class SellersPageState extends State<SellersPage> {
  Future<List<Customer>> _customersFuture;
  final FirebaseAnalyticsObserver observer;

  SellersPageState(this.observer);

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    _customersFuture = CustomerProvider.fetchAll();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Customers"),
        ),
        body: buildBody(_customersFuture));
  }

  FutureBuilder buildBody(Future<List<Customer>> _customerFuture) {
    return FutureBuilder(
      future: _customerFuture,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Press button to start.');
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            List<Customer> customers = snapshot.data;
            return ListView(
              children: customers.map((Customer c) => _buildTile(c)).toList(),
            );
        }
      },
    );
  }

  Widget _buildTile(Customer customer) {
    return ListTile(
      leading: customer.email != null ? Icon(Icons.email) : Icon(Icons.phone),
      title: Text(customer.email ?? customer.phone),
      trailing: Text(customer.sales.length.toString()),
    );
  }
}
