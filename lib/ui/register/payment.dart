import 'package:cash_register_app/models/customer.dart';
import 'package:cash_register_app/models/payment.dart';
import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/ressources/sale_api_provider.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sum_up_plugin/sum_up_plugin.dart';

class PaymentScreen extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  final Sale sale;
  final Customer customer;

  PaymentScreen(this.observer, this.sale, this.customer);

  @override
  PaymentScreenState createState() {
    return new PaymentScreenState(this.observer, this.sale, this.customer);
  }
}

class PaymentScreenState extends State<PaymentScreen> {
  final FirebaseAnalyticsObserver observer;
  final Sale sale;
  final Customer customer;
  bool useSumUp;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  FocusNode _toPayFocus = new FocusNode();
  TextEditingController _toPayController = new TextEditingController();

  PaymentScreenState(this.observer, this.sale, this.customer);

  @override
  void initState() {
    observer.analytics.setCurrentScreen(screenName: "/payment");

    super.initState();
    LoginSharedPreferences.isSumUpEnable().then((value) {
      useSumUp = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Checkout"),
        ),
        body: Column(children: <Widget>[
          _buildBody(),
          Divider(),
          _buildPayments(),
          _buildPaiementTypes()
        ]));
  }

  Widget _buildPayments() {
    return Expanded(
        child: ListView.builder(
      itemBuilder: (BuildContext context, int index) =>
          _buildPaymentWidget(this.sale.payments.payments[index]),
      itemCount: this.sale.payments.payments.length,
    ));
  }

  Widget _buildPaymentWidget(Payment p) {
    return ListTile(
      leading: getIconFromPayment(p.paymentType, color: Colors.black),
      title: Text(p.paymentType.toString()),
      trailing: Text(p.total.toStringAsFixed(2)),
    );
  }

  Widget _buildBody() {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Total"),
          trailing: Text(sale.totalPrice.toStringAsFixed(2)),
        ),
        ListTile(
          title: Text("Reste à payer"),
          trailing: Text(sale.restToPay.toStringAsFixed(2)),
        ),
        buildSeparator(),
        buildText(
            _toPayFocus,
            _toPayController,
            TextInputType.number,
            FontAwesomeIcons.idCard,
            "Total",
            false,
            IconButton(
              icon: Icon(FontAwesomeIcons.check),
              onPressed: () => this.setState(() =>
                  _toPayController.text = this.sale.restToPay.toStringAsFixed(2)),
            ))
      ],
    );
  }

  Widget _buildPaiementTypes() {
    return Container(
        height: 50,
        color: Colors.grey,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: List<Widget>.from(PaymentTypeValues.values
              .map((PaymentTypeValues pv) => _buildPaymentCard(pv))),
        ));
  }

  Widget _buildPaymentCard(PaymentTypeValues paymentType) {
    return IconButton(
      tooltip: paymentType.toString().split(".")[1],
      icon: getIconFromPayment(paymentType, color: Colors.black, size: 40.0),
      onPressed: () =>
          _handlePayment(paymentType, double.tryParse(_toPayController.text)),
    );
  }

  void _logSale(Sale sale) {
    observer.analytics.logEcommercePurchase(
      currency: 'EUR',
      value: sale.totalPrice,
      transactionId: sale.id,
      tax: sale.tax,
      shipping: 5.67,
    );
  }

  void _handlePayment(PaymentTypeValues paymentType, double toPay) {
    if(toPay>double.parse(sale.restToPay.toStringAsFixed(2))){
      showInSnackBar(_scaffoldKey, context, "Impossible de payer plus que l'on doit");
      return;
    }
    switch (paymentType) {
      case PaymentTypeValues.sumUp:
        SumUpPlugin.prepareTransaction(toPay.toString())
            .then((dynamic success) {
          if (success != null) {
            return completePayment(sale);
          }
        }).catchError(
                (e) => showInSnackBar(_scaffoldKey, context, e.toString()));
        break;
      default:
        this.sale.payments.addPayment(new Payment(paymentType, toPay));
    }
    this.setState((){});
  }

  void completePayment(sale) {
    SaleProvider.save(sale).then((bool success) {
      if (success) {
        Navigator.pop(context, true);
        _logSale(sale);
      } else {
        showInSnackBar(
            _scaffoldKey, context, "Impossible de sauver cette vente");
      }
    });
  }
}
