import 'dart:convert';

import 'package:cash_register_app/models/stock.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';


class StockProvider{
  static Future<StockModel> fetch() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, '/api/v1/me/company/$companyId/stock');
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      StockModel company = StockModel.fromJson(
          List<Map<String, dynamic>>.from(result['content']["stockItems"]),
          companyId);
      return company;
    } else {
      throw Exception(response.body);
    }
  }
}