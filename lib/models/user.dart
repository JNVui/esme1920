List<String> colors = ["#558B6E", "#88A09E", "#704C5E", "#B88C9E", "#F1C8DB"];

class User {
  final String id;
  final String displayName;
  final String password;
  final String phoneNumber;
  final String email;
  final String token;
  List<dynamic> companies;
  List<String> companiesId;
  String currentCompanyId;

  User(
      {this.id,
      this.displayName,
      this.password,
      this.phoneNumber,
      this.email,
      this.token,
      this.companiesId,
      this.companies}) {
    if (this.companies != null) {
      for (var index = 0; index < this.companies.length; index++) {
        this.companies[index]["index"] = this.companies.length - index;
      }
      this.companies = List.from(this.companies.reversed);
    }
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['_id'],
      displayName: json['displayName'],
      password: json['password'],
      phoneNumber: json['phoneNumber'],
      email: json['email'],
      token: json['token'],
      companies: json['companiesDetails'],
      companiesId:
          List<String>.from(json['companies'].map((dynamic s) => s.toString())),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'displayName': displayName,
      'password': password,
      'phoneNumber': phoneNumber,
      'email': email,
      'token': token,
    };
  }
}
