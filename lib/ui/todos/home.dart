import 'dart:ui';

import 'package:cash_register_app/models/todo.dart';
import 'package:cash_register_app/ressources/todo_api_provider.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/todos/details.dart';
import 'package:cash_register_app/ui/widget/headers.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'custom_icons.dart';

class TodoScreen extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  TodoScreen({Key key, this.observer}) : super(key: key);
  @override
  _TodoScreenState createState() => new _TodoScreenState(this.observer);
}

class _TodoScreenState extends State<TodoScreen> with TickerProviderStateMixin {
  final FirebaseAnalyticsObserver observer;

  ScrollController scrollController;
  Color backgroundColor;
  int currentPage = 0;
  Color constBackColor;
  Future<Todos> _todosFuture;

  int todoNumber;

  Todos _todos;

  _TodoScreenState(this.observer);

  @override
  void initState() {
    backgroundColor = Color(0xFF5A89E6);
    scrollController = new ScrollController();
    _todosFuture = TodoProvider.fetch();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _ratioW = _width / 375.0;

    final double _height = MediaQuery.of(context).size.height;
    final double _ratioH = _height / 812.0;

    return new Container(
      decoration: new BoxDecoration(
        gradient: new LinearGradient(
            colors: [
              Theme.Colors.loginGradientStart,
              Theme.Colors.loginGradientEnd
            ],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: new Scaffold(
          backgroundColor: Colors.transparent,
          appBar: new AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            title: new Text("TODO"),
            actions: <Widget>[
              new IconButton(
                icon: new Icon(
                  CustomIcons.search,
                  size: 26.0,
                ),
                onPressed: () {},
              )
            ],
          ),
          body: new Container(
            child: new Stack(
              children: <Widget>[
                new Container(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        padding: const EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 50.0, right: 60.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.only(bottom: 25.0),
                              child: new Container(
                                decoration: new BoxDecoration(
                                  boxShadow: [
                                    new BoxShadow(
                                        color: Colors.black38,
                                        offset: new Offset(5.0, 5.0),
                                        blurRadius: 15.0)
                                  ],
                                  shape: BoxShape.circle,
                                ),
                                child: new CircleAvatar(
                                  backgroundColor: Colors.grey,
                                ),
                              ),
                            ),
                            buildNameHeader(),
                            new Text(
                              "This is a daily quote.",
                              style: new TextStyle(color: Colors.white70),
                            ),
                            new Text(
                              todoNumber != null
                                  ? "Vous avez $todoNumber choses à faire."
                                  : "Commencez par créer votre premier TODO",
                              style: new TextStyle(color: Colors.white70),
                            ),
                          ],
                        ),
                      ),
                      _buildTodoBody(_width)
                    ],
                  ),
                ),
                _buildActionButton(),
              ],
            ),
          )),
    );
  }

  Widget _buildTodoBody(width) {
    return FutureBuilder(
      future: _todosFuture,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Press button to start.');
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            Todos todos = snapshot.data;
            _todos = todos;
            todoNumber = todos.todos.length;
            return _buildTodoList(todos, width);
        }
      },
    );
  }

  Widget _buildActionButton() {
    return new Padding(
      padding: const EdgeInsets.only(right: 15.0, bottom: 15.0),
      child: new Align(
        alignment: Alignment.bottomRight,
        child: new FloatingActionButton(
          onPressed: () {
            Todo todo = new Todo(
                title: "Nouveau TODO",
                tasks: [new Task(title: "Nouvelle tache")]);
            TodoProvider.create(todo).then((Todo t) => this.setState(
                () => _todos != null ? _todos.todos.insert(0, t) : null));
          },
          tooltip: 'Increment',
          child: new Icon(Icons.add),
        ),
      ),
    );
  }

  Widget _buildTodoList(Todos todos, width) {
    return new Container(
      height: 350.0,
      width: width,
      child: new ListView.builder(
        itemBuilder: (context, index) {
          Todo todo = todos.todos[index];

          return _buildTodoCard(todos, todo);
        },
        padding: const EdgeInsets.only(left: 40.0, right: 40.0),
        scrollDirection: Axis.horizontal,
        controller: scrollController,
        itemExtent: width - 80,
        itemCount: todos.todos.length,
      ),
    );
  }

  Widget _buildTodoCard(Todos todos, Todo todo) {
    EdgeInsets padding =
        const EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0, bottom: 30.0);

    double percentComplete = todo.percentComplete();

    return new Padding(
        padding: padding,
        child: new InkWell(
          onTap: () {
            Navigator.of(context).push(new PageRouteBuilder(
                pageBuilder: (BuildContext context, Animation<double> animation,
                        Animation<double> secondaryAnimation) =>
                    new DetailPage(todoObject: todo, isEditMode: false),
                transitionsBuilder: (
                  BuildContext context,
                  Animation<double> animation,
                  Animation<double> secondaryAnimation,
                  Widget child,
                ) {
                  return new SlideTransition(
                    position: new Tween<Offset>(
                      begin: const Offset(0.0, 1.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(0.0, 1.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
                },
                transitionDuration: const Duration(milliseconds: 1000)));
          },
          child: new Container(
              decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.circular(10.0),
                  boxShadow: [
                    new BoxShadow(
                        color: Colors.black.withAlpha(70),
                        offset: const Offset(3.0, 10.0),
                        blurRadius: 15.0)
                  ]),
              height: 250.0,
              child: new Stack(
                children: <Widget>[
                  new Hero(
                    tag: todo.uuid + "_background",
                    child: new Container(
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Expanded(
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Hero(
                                tag: todo.uuid + "_icon",
                                child: new Container(
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: new Border.all(
                                        color: Colors.grey.withAlpha(70),
                                        style: BorderStyle.solid,
                                        width: 1.0),
                                  ),
                                  child: new Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: new Icon(Icons.toc,
                                        color: backgroundColor),
                                  ),
                                ),
                              ),
                              new Expanded(
                                child: new Container(
                                    alignment: Alignment.topRight,
                                    child: new Hero(
                                      tag: todo.uuid + "_more_vert",
                                      child: new Material(
                                        color: Colors.transparent,
                                        type: MaterialType.transparency,
                                        child: new IconButton(
                                          icon: new Icon(
                                            Icons.archive,
                                            color: Colors.grey,
                                          ),
                                          onPressed: () {
                                            todo.isOnline = false;
                                            todos.todos.remove(todo);
                                            TodoProvider.update(todo).then((Todo todo) =>
                                                this.setState(() {}));
                                          },
                                        ),
                                      ),
                                    )),
                              )
                            ],
                          ),
                        ),
                        new Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: new Align(
                                alignment: Alignment.bottomLeft,
                                child: new Hero(
                                  tag: todo.uuid + "_number_of_tasks",
                                  child: new Material(
                                      color: Colors.transparent,
                                      child: new Text(
                                        todo.tasks.length.toString() + " Tasks",
                                        style: new TextStyle(),
                                      )),
                                ))),
                        new Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: new Align(
                              alignment: Alignment.bottomLeft,
                              child: new Hero(
                                tag: todo.uuid + "_title",
                                child: new Material(
                                  color: Colors.transparent,
                                  child: new Text(
                                    todo.title,
                                    style: new TextStyle(fontSize: 30.0),
                                  ),
                                ),
                              )),
                        ),
                        new Align(
                            alignment: Alignment.bottomLeft,
                            child: new Hero(
                                tag: todo.uuid + "_progress_bar",
                                child: new Material(
                                  color: Colors.transparent,
                                  child: new Row(
                                    children: <Widget>[
                                      new Expanded(
                                        child: new LinearProgressIndicator(
                                          value: percentComplete,
                                          backgroundColor:
                                              Colors.grey.withAlpha(50),
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  backgroundColor),
                                        ),
                                      ),
                                      new Padding(
                                        padding:
                                            const EdgeInsets.only(left: 5.0),
                                        child: new Text((percentComplete * 100)
                                                .round()
                                                .toString() +
                                            "%"),
                                      )
                                    ],
                                  ),
                                )))
                      ],
                    ),
                  ),
                ],
              )),
        ));
  }
}

class CustomScrollPhysics extends ScrollPhysics {
  final Todos todos;
  double numOfItems;

  CustomScrollPhysics(this.todos, {ScrollPhysics parent})
      : super(parent: parent) {
    numOfItems = todos.todos.length.toDouble() - 1;
  }

  @override
  CustomScrollPhysics applyTo(ScrollPhysics ancestor) {
    return new CustomScrollPhysics(todos, parent: buildParent(ancestor));
  }

  double _getPage(ScrollPosition position) {
    return position.pixels / (position.maxScrollExtent / numOfItems);
    // return position.pixels / position.viewportDimension;
  }

  double _getPixels(ScrollPosition position, double page) {
    // return page * position.viewportDimension;
    return page * (position.maxScrollExtent / numOfItems);
  }

  double _getTargetPixels(
      ScrollPosition position, Tolerance tolerance, double velocity) {
    double page = _getPage(position);
    if (velocity < -tolerance.velocity)
      page -= 0.5;
    else if (velocity > tolerance.velocity) page += 0.5;
    return _getPixels(position, page.roundToDouble());
  }

  @override
  Simulation createBallisticSimulation(
      ScrollMetrics position, double velocity) {
    if ((velocity <= 0.0 && position.pixels <= position.minScrollExtent) ||
        (velocity >= 0.0 && position.pixels >= position.maxScrollExtent))
      return super.createBallisticSimulation(position, velocity);
    final Tolerance tolerance = this.tolerance;
    final double target = _getTargetPixels(position, tolerance, velocity);
    if (target != position.pixels)
      return new ScrollSpringSimulation(
          spring, position.pixels, target, velocity,
          tolerance: tolerance);
    return null;
  }
}
