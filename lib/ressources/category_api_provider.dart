
import 'dart:convert';

import 'package:cash_register_app/models/categories.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';


class CategoryProvider{

  static Future<Category> create(Category category) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.POST, '/api/v1/company/$companyId/category',
        body: json.encode(category.getCleanJson()));

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      return Category.simpleFromJson(result["content"]);
    } else {
      throw Exception(response.body);
    }
  }

  static Future<Category> update(Category category) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.PUT, '/api/v1/company/$companyId/category/${category.id}',
        body: json.encode(category.getCleanJson()));

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      return Category.simpleFromJson(result["content"]);
    } else {
      throw Exception(response.body);
    }
  }
}