import 'dart:convert';

import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/auth.dart';
import 'package:cash_register_app/utils/map_utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';


class SellerProvider{

  static Future<User> createUser(data) async {
    final response = await makeRequest(METHOD.POST, 'api/v1/seller',
        body: json.encode(cleanNull(data)), useToken: false);

    if (response.statusCode == 200) {
      return User.fromJson(json.decode(response.body)["content"]);
    } else if (response.statusCode == 409) {
      return null;
    } else {
      throw Exception(response.body);
    }
  }

  static Future<User> authUser(data) async {

    final response = await makeRequest(METHOD.POST, '/api/v1/seller/auth',
        body: json.encode(cleanNull(data)), useToken: false);
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      result['content']['token'] = result['token'];
      User user = User.fromJson(result['content']);
      LoginSharedPreferences.setId(user.id);
      return user;
    } else {
      throw Exception(response.body);
    }
  }

  static Future<User> getMe() async {
    final response = await makeRequest(METHOD.GET, '/api/v1/me');
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      result['content']['token'] = result['token'];
      return User.fromJson(result['content']);
    } else {
      throw Exception(response.body);
    }
  }

  static Future<User> updateMe(data) async {
    final response =
    await makeRequest(METHOD.PUT, '/api/v1/me', body: json.encode(data));
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      result['content']['token'] = result['token'];
      return User.fromJson(result['content']);
    } else {
      throw Exception(response.body);
    }
  }

  static Future<Company> getMeCurrentCompany(User user) async {
    String companyId =
        await LoginSharedPreferences.getCurrentCompanyId() ?? user.companies[0];
    final response =
    await makeRequest(METHOD.GET, '/api/v1/me/company/$companyId');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      Map<String, dynamic> result = json.decode(response.body);
      Company company = Company.fromJson(result['content']);
      return company;
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body);
    }
  }

  static Future<String> joinCompany(String siret, String token) async {
    final response = await makeRequest(
        METHOD.POST, '/api/v1/me/company/$siret/join',
        body: json.encode({"token": token}));

    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception(response.body);
    }
  }

  static Future<String> quitCompany(String companyId) async {
    String _companyId = await LoginSharedPreferences.getCurrentCompanyId();

    if (_companyId == companyId) {
      LoginSharedPreferences.setCurrentCompanyId(null);
    }
    final response =
    await makeRequest(METHOD.DELETE, '/api/v1/me/company/$companyId');
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception(response.body);
    }
  }

}