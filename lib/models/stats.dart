import 'package:charts_flutter/flutter.dart' as charts;
class CaByProducts {
  final String productId;
  final int ca;
  final int id;

  CaByProducts(this.productId, this.ca, this.id);
}

List<charts.Series<CaByProducts, String>> getSeriesCaProductFromResult(
    List<dynamic> results) {
  if (results.isEmpty) {
    return null;
  }
  return [
    new charts.Series<CaByProducts, String>(
      id: 'Sales',
      domainFn: (CaByProducts stat, _) => stat.productId,
      measureFn: (CaByProducts stat, _) => stat.ca,
      data: results,
      // Set a label accessor to control the text of the arc label.
      labelAccessorFn: (CaByProducts row, _) => '${row.productId}',
    )
  ];
}

