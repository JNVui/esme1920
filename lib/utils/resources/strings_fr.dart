class StringsFR {
  static var map = {
    'activity_type': 'Type d\'activité',
    'active_paypal': 'active paypal',
    'active_sumup': 'active sumup',
    'address': 'addresse',
    'cashfund': 'Fond de caisse',
    'city': 'city',
    'email': 'adresse email',
    'fax_number': 'numéro de fax',
    'personal': 'personnelle',
    'phone_number': 'numéro de téléphone',
    'rc_number': 'numero RC',
    'sumup': 'SumUp',
    'shop_settings': 'Boutique',
    'shop_settings_subtitle': 'Configuration de la Boutique',
    'store_name': 'nom de la boutique',
    'title': 'Welcome',
    'tva_number': 'numero TVA',
    'website': 'site web',
    'zipcode': 'zipcode',
    'location': 'Localisation',
    'locationSettingsSubtitle':
        'Informations sur la localisation de la boutique',
    'contact': 'Contact',
    'contactSettingsSubtitle': 'Information de contact',
  };
}
