import 'package:cash_register_app/models/cart.dart';
import 'package:cash_register_app/models/product.dart';
import 'package:cash_register_app/ui/register/checkout.dart';
import 'package:cash_register_app/ui/widget/headers.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class Cart extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  final CartModel cart;

  Cart({this.cart, this.observer});

  @override
  CartState createState() {
    return new CartState(this.observer, cart: this.cart);
  }
}

class CartState extends State<Cart> {
  final CartModel cart;
  final FirebaseAnalyticsObserver observer;

  CartState(this.observer, {this.cart});

  @override
  Widget build(BuildContext context) {
    return new Drawer(
        child: Column(children: [
      cartHeader(context),
      Divider(height: 10.0),
      buildCart(),
      _passToPaymen(),
      Divider(
        height: 10,
      ),
      cartTotalFooter(this.cart.total)
    ]));
  }

  Widget _passToPaymen(){
    return FlatButton(
      child: Text("Passer au Paiement"),
      onPressed: (){
        Navigator.push(
            context,
            MaterialPageRoute(
            builder: (context) => CheckoutScreen(
            this.observer,
            cartState: this,
            parentContext: context,
            cart: this.cart)));
      },
    );
  }

  void removeQty(ProductModel p) {
    this.cart.removeQty(p);
    this.setState(() {});
  }

  void addQty(ProductModel p) {
    this.cart.addProduct(p);
    this.setState(() {});
  }

  void removeProduct(ProductModel p) {
    this.cart.remove(p);
    this.setState(() {});
  }

  Widget buildCart() {
    return Expanded(
        child: ListView(
            children: this.cart.products.map<Widget>((product) {
      return Dismissible(
          key: Key(product.id),
          onDismissed: (DismissDirection direction) => removeProduct(product),
          background: Container(color: Colors.green),
          child: ListTile(
            title: Text(product.name),
            subtitle: Text(product.getCalculationString),
            trailing: new GestureDetector(
              child: Icon(Icons.remove),
              onTap: () => removeQty(product),
            ),
            leading: new GestureDetector(
              child: Icon(Icons.add),
              onTap: () => addQty(product),
            ),
            onLongPress: () => removeProduct(product),
          ));
    }).toList()));
  }

  Widget returnEmptyCart() {
    return new Material(
        borderRadius: new BorderRadius.circular(8.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Icon(Icons.remove_shopping_cart,
                  color: Colors.black26, size: 96.0),
              new Padding(padding: new EdgeInsets.only(bottom: 48.0)),
              new Text('Your cart is empty!',
                  style: new TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 28.0)),
              new Padding(padding: new EdgeInsets.only(bottom: 8.0)),
              new Container(
                margin: new EdgeInsets.symmetric(horizontal: 64.0),
                child: new Text(
                    'Looks like you haven\'t added any plants to your cart yet.',
                    textAlign: TextAlign.center,
                    style: new TextStyle(fontSize: 20.0)),
              ),
              new Padding(padding: new EdgeInsets.only(bottom: 96.0)),
            ],
          ),
        ));
  }
}
