import 'package:cash_register_app/models/product.dart';
import 'package:collection/collection.dart';

class StockModel {
  final Set<ProductModel> products;
  String companyId;

  StockModel({this.products});

  factory StockModel.fromJson(List<dynamic> json, String companyId) {
    print("StockModel.fromJson $json");
    Set<ProductModel> products = Set<ProductModel>.from(
        json.map(((item) => ProductModel.fromCompanyJson(item, companyId))));
    return StockModel(products: products);
  }
  int get length {
    return this.products.length;
  }

  double get total {
    return this
        .products
        .fold(0, (total, p) => total + p.realPrice * p.qtyInCart);
  }

  List<ProductModel> get productList {
    return List.from(this.products);
  }

  Map<String, List<ProductModel>> get groupedProduct {
    return groupBy(this.products, (ProductModel p) => p.category);
  }

  List<String> get categories {
    return this.groupedProduct.keys.toList();
  }

  bool contains(ProductModel p) {
    return this.products.contains(p);
  }

  bool add(ProductModel p) {
    if (this.contains(p)) {
      return false;
    } else {
      this.products.add(p);
      return true;
    }
  }

  ProductModel getProductById(String productId) {
    try {
      return this.products.firstWhere((ProductModel p) =>
          ((p.id == productId) | (p.correspondToQrCode(productId))));
    } catch (e) {
      return null;
    }
  }

  List<ProductModel> getProductByCategory(String category) {
    return groupedProduct[category];
  }

  void remove(ProductModel p) {
    this.products.remove(p);
  }
}
