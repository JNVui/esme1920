import 'package:cash_register_app/models/cart.dart';
import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/ressources/cash_fund_api_provider.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/register/cart.dart';
import 'package:cash_register_app/ui/register/floating.dart';
import 'package:cash_register_app/ui/widget/core_register_stock.dart';
import 'package:cash_register_app/ui/widget/dialog.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class RegisterScreen extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const RegisterScreen({Key key, this.observer}) : super(key: key);

  @override
  RegisterScreenState createState() {
    return new RegisterScreenState(this.observer);
  }
}

class RegisterScreenState extends State<RegisterScreen> {
  final FirebaseAnalyticsObserver observer;

  Future<Company> companyFuture;
  CartModel cart;

  RegisterScreenState(this.observer);

  @override
  void initState() {
    cart = new CartModel(observer);
    super.initState();
    this.fetchData();
    checkIfRegisterIsOpen();
    observer.analytics.setCurrentScreen(screenName: "/register");
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: companyFuture,
        builder: (BuildContext context, AsyncSnapshot<Company> snapshot) {
          if (snapshot.hasError)
            return Text("Error Occurred ${snapshot.error}");
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              Company company = snapshot.data;
              return _mainBody(
                  company: company,
                  endDrawer: Cart(cart: cart, observer:this.observer),
                  floatingButtonBuilder: Builder(builder: (context) {
                    return FancyFab(
                      registerScreenState: this,
                      context: context,
                      stock: company.stock,
                    );
                  }),
                  isRegister: true);
            default:
              return new Container(width: 0.0, height: 0.0);
          }
        });
  }

  Widget _mainBody(
      {Company company, endDrawer, floatingButtonBuilder, bool isRegister}) {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomPadding: false,
      endDrawer: endDrawer,
      body: CoreRegisterStock(company, isRegister, cart: cart, observer: this.observer,),
      floatingActionButton: floatingButtonBuilder,
    );
  }

  void checkIfRegisterIsOpen() {
    LoginSharedPreferences.getResisterIsOpen().then((String isOpen) =>
        (isOpen == null)
            ? showAskDialog(
                    context, "Ouvrir la caisse", "Fond de caisse", "Ouvrir",
                    askValue: true)
                .then((value) {
                if (value != null) {
                  CashFundProvider.update(double.parse(value)).then((bool success) {
                    if (success) {
                      LoginSharedPreferences.setRegisterIsOpen(new Uuid().v1());
                    }
                  });
                }
              })
            : null);
  }

  void fetchData() {
    this.companyFuture = CompanyProvider.fetchStock();
  }
}
