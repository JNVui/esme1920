import 'dart:convert';
import 'dart:io';

import 'package:cash_register_app/models/bill.dart';
import 'package:cash_register_app/ui/billsManager/create_bill.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(new MaterialApp(
      home: new BillsManager(),
    ));

class BillsManager extends StatefulWidget {
  @override
  _BillsManagerState createState() => new _BillsManagerState();
}

class _BillsManagerState extends State<BillsManager> {
  Widget appBarTitle = new Text(
    "Search Example",
    style: new TextStyle(color: Colors.white),
  );
  Icon icon = new Icon(
    Icons.search,
    color: Colors.white,
  );
  final globalKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _controller = new TextEditingController();
  List<BillModel> _list;
  bool _isSearching;
  String _searchText = "";
  List<BillModel> searchresult = new List();

  _BillsManagerState() {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _isSearching = false;
    _list = _getList();
    _getBills();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: globalKey,
        appBar: buildAppBar(context),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _createBill();
          },
          child: Icon(Icons.add),
        ),
        body: searchresult.length != 0 || _controller.text.isNotEmpty
            ? GridView.count(
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this would produce 2 rows.
                crossAxisCount: 2,
                // Generate 100 Widgets that display their index in the List
                children: _generateListWidget(searchresult),
              )
            : GridView.count(
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this would produce 2 rows.
                crossAxisCount: 2,
                // Generate 100 Widgets that display their index in the List
                children: _generateListWidget(_list),
              ));
  }

  Widget buildAppBar(BuildContext context) {
    return new AppBar(centerTitle: true, title: appBarTitle, actions: <Widget>[
      new IconButton(
        icon: icon,
        onPressed: () {
          setState(() {
            if (this.icon.icon == Icons.search) {
              this.icon = new Icon(
                Icons.close,
                color: Colors.white,
              );
              this.appBarTitle = new TextField(
                controller: _controller,
                style: new TextStyle(
                  color: Colors.white,
                ),
                decoration: new InputDecoration(
                    prefixIcon: new Icon(Icons.search, color: Colors.white),
                    hintText: "Search...",
                    hintStyle: new TextStyle(color: Colors.white)),
                onChanged: searchOperation,
              );
              _handleSearchStart();
            } else {
              _handleSearchEnd();
            }
          });
        },
      ),
    ]);
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.icon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Search Sample",
        style: new TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _controller.clear();
    });
  }

  void searchOperation(String searchText) {
    searchresult.clear();
    if (_isSearching != null) {
      for (int i = 0; i < _list.length; i++) {
        String data = _list[i].tag;
        if (data.toLowerCase().contains(searchText.toLowerCase())) {
          searchresult.add(_list[i]);
        }
      }
    }
  }

  List<Widget> _generateListWidget(List<BillModel> inputList) {
    List<Widget> list = new List<Widget>();

    Set<BillModel> set = Set.from(inputList);
    set.forEach((bill) => {
          list.add(
            GestureDetector(
              onTap: () {
                _editBill(bill);
              },
              child: Card(
                child: Column(
                  children: <Widget>[
                    Text(
                      bill.id,
                      style: Theme.of(context).textTheme.headline,
                    ),
                    Text(
                      bill.seller,
                      style: Theme.of(context).textTheme.headline,
                    ),
                    Text(
                      bill.buyer,
                      style: Theme.of(context).textTheme.headline,
                    ),
                    Text(
                      bill.price.toString(),
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ],
                ),
              ),
            ),
          )
        });

    return list;
  }

  List<BillModel> _getList() {
    List<BillModel> list = new List<BillModel>();
    return list;
  }

  _createBill() async {
    Navigator.pushNamed(context, "/billsManager/create_bill").then((it) async {
      BillModel bill = it as BillModel;
      Directory dir = await getApplicationDocumentsDirectory();
      File file = new File("${dir.path}/bills/${bill.id}");
      await file.create(recursive: true);
      file.writeAsStringSync(json.encode(bill.toJson()));
      setState(() {
        _getBills();
      });
    });
  }

  _editBill(BillModel billModel) async {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CreateBillPage(billModel: billModel)))
        .then((it) async {
      BillModel bill = it as BillModel;
      Directory dir = await getApplicationDocumentsDirectory();
      File file = new File("${dir.path}/bills/${bill.id}");
      await file.create(recursive: true);
      file.writeAsStringSync(json.encode(bill.toJson()));
      setState(() {
        _getBills();
      });
    });
  }

  void _getBills() async {
    Directory dir = await getApplicationDocumentsDirectory();
    var systemTempDir = Directory("${dir.path}/bills/");
    _list
        .clear(); //remove all elements before refreshing list find better solution
    systemTempDir
        .list(recursive: true, followLinks: false)
        .listen((FileSystemEntity entity) {
      File file = new File(entity.path);
      file.readAsString().then((it) {
        var bill = BillModel.fromJson(json.decode(it));
        setState(() {
          _list.add(bill);
        });
      });
    });
  }
}
