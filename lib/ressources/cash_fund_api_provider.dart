import 'dart:convert';

import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';

class CashFundProvider {
  static Future<double> fetch() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
        await makeRequest(METHOD.GET, '/api/v1/company/$companyId/cashFunds');

    if (response.statusCode == 200) {
      return json.decode(response.body)["content"]["total"].toDouble();
    } else {
      throw Exception(response.body);
    }
  }

  static Future<bool> update(double total) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.POST, '/api/v1/company/$companyId/cashFunds',
        body: json.encode({"total": total}));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception(response.body);
    }
  }
}
