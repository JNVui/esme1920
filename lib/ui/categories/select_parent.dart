import 'package:cash_register_app/ui/categories/display_categories.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class SelectCategoriesPage extends StatefulWidget {
  final String currentCategoryId;
  final bool isHome;
  final FirebaseAnalyticsObserver observer;


  SelectCategoriesPage({this.currentCategoryId, this.isHome, this.observer});

  @override
  SelectCategoriesPageState createState() {
    return new SelectCategoriesPageState(this.observer,
        currentCategoryId: this.currentCategoryId, isHome: this.isHome);
  }
}

class SelectCategoriesPageState extends State<SelectCategoriesPage> {
  final String currentCategoryId;
  bool isHome;

  final FirebaseAnalyticsObserver observer;

  SelectCategoriesPageState(this.observer, {this.currentCategoryId, this.isHome});

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    observer.analytics.setCurrentScreen(screenName: "/category/select");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Theme.Colors.loginGradientStart,
                Theme.Colors.loginGradientEnd
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              title: Text("Catégories"),
            ),
            body: DisplayOrSelectCategoriesPage(
              observer: observer,
              isHome: false,
              currentCategoryId: this.currentCategoryId,
            )));
  }
}
