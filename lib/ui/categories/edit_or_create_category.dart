import 'package:cash_register_app/models/categories.dart';
import 'package:cash_register_app/ressources/category_api_provider.dart';
import 'package:cash_register_app/ui/categories/select_parent.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EditOrCreateCategory extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  final Category category;
  final bool isModifying;

  const EditOrCreateCategory({Key key, this.category, this.isModifying, this.observer})
      : super(key: key);

  @override
  _EditOrCreateCategoryState createState() =>
      _EditOrCreateCategoryState(this.category, this.isModifying, this.observer);
}

class _EditOrCreateCategoryState extends State<EditOrCreateCategory> {
  final FirebaseAnalyticsObserver observer;

  Category category;
  final bool isModifying;
  Category parentEntry;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _EditOrCreateCategoryState(this.category, this.isModifying, this.observer);

  final FocusNode nameFocus = FocusNode();

  TextEditingController nameController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    observer.analytics.setCurrentScreen(screenName: "/category/${isModifying?'edit':'create'}");

    nameController = new TextEditingController(
        text: category == null ? null : category.displayName);

    if (this.category == null) {
      this.category = new Category();
    }
  }

  @override
  void dispose() {
    nameController.dispose();
    nameFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Theme.Colors.loginGradientStart,
                Theme.Colors.loginGradientEnd
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: new Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: Text("Modifier ${category.name}"),
              backgroundColor: Colors.transparent,
              elevation: 0.0,
            ),
            backgroundColor: Colors.transparent,
            body: Padding(
                padding: EdgeInsets.only(top: 150.0),
                child: Center(child: _buildJoin(context)))));
  }

  Widget _buildJoin(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 150.0,
                  child: Column(
                    children: <Widget>[
                      buildText(nameFocus, nameController, TextInputType.text,
                          FontAwesomeIcons.idCard, "Nom", false, null),
                      _buildParentSelectionOrDisplay(),
                    ],
                  ),
                ),
              ),
              buildButton(140.0, isModifying ? "Modifier" : "Ajouter", () {
                this.category.name = nameController.text;
                this.category.slug = nameController.text.toLowerCase();
                Future<Category> _future = this.isModifying
                    ? CategoryProvider.update(this.category)
                    : CategoryProvider.create(this.category);
                _future.then((Category p) {
                  if (p != null)
                    Navigator.of(context).pop(true);
                  else
                    print("p not found");
                }).catchError((error) => print(error));
              })
            ],
          )
        ],
      ),
    );
  }

  Widget _buildParentSelectionOrDisplay() {
    return this.category.parentNode == null
        ? ListTile(
            title: Text("Catégorie parente"),
            leading: Icon(FontAwesomeIcons.clipboardList),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SelectCategoriesPage(
                          currentCategoryId: this.category.id,
                          isHome: false,
                        )),
              ).then((dynamic category) {
                parentEntry = category;
                this.category.parentNode = parentEntry;
              });
            },
          )
        : ListTile(
            title: Text(this.category.parentName),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SelectCategoriesPage(
                          currentCategoryId: this.category.id,
                          isHome: false,
                        )),
              ).then((dynamic category) {
                parentEntry = category;
                this.category.parentNode = parentEntry;
              });
            },
            trailing: Icon(Icons.mode_edit),
            leading: Text("Parent :"),
          );
  }
}
