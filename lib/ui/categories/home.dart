import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/categories/display_categories.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class CategoriesPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const CategoriesPage({Key key, this.observer}) : super(key: key);

  @override
  CategoriesPageState createState() {
    return new CategoriesPageState(this.observer);
  }
}

class CategoriesPageState extends State<CategoriesPage> {
  Future<Company> companyFuture;
  final FirebaseAnalyticsObserver observer;

  CategoriesPageState(this.observer);

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    companyFuture = CompanyProvider.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Theme.Colors.loginGradientStart,
                Theme.Colors.loginGradientEnd
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              title: Text("Catégories"),
            ),
            body: DisplayOrSelectCategoriesPage(isHome: true, observer: observer,)));
  }
}
