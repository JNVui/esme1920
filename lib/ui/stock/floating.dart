import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/product.dart';
import 'package:cash_register_app/ressources/product_api_provider.dart';
import 'package:cash_register_app/ui/stock/home.dart';
import 'package:cash_register_app/ui/stock/product_edit.dart';
import 'package:cash_register_app/utils/scan.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class FancyFab extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  final Function() onPressed;
  final String tooltip;
  final IconData icon;
  final Company company;
  final StockHomeState stockState;

  FancyFab(
      {this.stockState, this.onPressed, this.tooltip, this.icon, this.company, this.observer});

  @override
  _FancyFabState createState() =>
      _FancyFabState(this.observer, stockState: this.stockState, company: this.company);
}

class _FancyFabState extends State<FancyFab>
    with SingleTickerProviderStateMixin {
  final FirebaseAnalyticsObserver observer;

  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;
  final StockHomeState stockState;

  final Company company;

  _FancyFabState(this.observer, {this.stockState, this.company});

  @override
  initState() {

    observer.analytics.setCurrentScreen(screenName: "/stock/floating");

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500))
          ..addListener(() {
            setState(() {});
          });
    _animateIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _buttonColor = ColorTween(
      begin: Colors.blue,
      end: Colors.red,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));
    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget add() {
    return Container(
      child: FloatingActionButton(
        heroTag: 'add',
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CreateModifyItemScreen(
                  observer: this.observer,
                      company: this.company,
                      isModifying: false,
                    )),
          );
        },
        tooltip: 'Add',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget scan() {
    return Container(
      child: FloatingActionButton(
        heroTag: 'scan',
        onPressed: () {
          scanArticle()
              .then((String code) => _showResultScanDialog(code))
              .catchError((e) => print("Error $e"));
        },
        tooltip: 'Scan',
        child: Icon(Icons.photo_camera),
      ),
    );
  }

  Widget toggle() {
    return Container(
      child: FloatingActionButton(
        heroTag: 'toggle',
        backgroundColor: _buttonColor.value,
        onPressed: animate,
        tooltip: 'Toggle',
        child: AnimatedIcon(
          icon: AnimatedIcons.menu_close,
          progress: _animateIcon,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 2.0,
            0.0,
          ),
          child: add(),
        ),
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value,
            0.0,
          ),
          child: scan(),
        ),
        toggle(),
      ],
    );
  }

  void _showResultScanDialog(code) {
    // flutter defined function
    ProductProvider.fetchFromId(ProductModel.getCodeFromUrl(code))
        .then((ProductModel p) => ProductProvider.askForUpdate(p,context))
        .catchError((e) => print(e));
  }
}
