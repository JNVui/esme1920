import 'package:cash_register_app/models/todo.dart';
import 'package:cash_register_app/ressources/todo_api_provider.dart';
import 'package:cash_register_app/ui/todos/custom_list_tile.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class DetailPage extends StatefulWidget {
  final bool isEditMode;

  DetailPage({@required this.todoObject, Key key, this.isEditMode})
      : super(key: key);

  final Todo todoObject;

  @override
  _DetailPageState createState() => new _DetailPageState(this.isEditMode);
}

class _DetailPageState extends State<DetailPage> with TickerProviderStateMixin {
  double percentComplete;
  AnimationController animationBar;
  double barPercent = 0.0;
  Tween<double> animT;
  AnimationController scaleAnimation;
  Color backgroundColor = Color(0xFF5A89E6);
  bool _isEditMode;

  _DetailPageState(this._isEditMode);

  @override
  void initState() {
    scaleAnimation = new AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 1000),
        lowerBound: 0.0,
        upperBound: 1.0);

    percentComplete = widget.todoObject.percentComplete();
    barPercent = percentComplete;
    animationBar = new AnimationController(
        vsync: this, duration: const Duration(milliseconds: 100))
      ..addListener(() {
        setState(() {
          barPercent = animT.lerp(animationBar.value);
        });
      });
    ;
    animT = new Tween<double>(begin: percentComplete, end: percentComplete);
    scaleAnimation.forward();
    super.initState();
  }

  void updateBarPercent() async {
    double newPercentComplete = widget.todoObject.percentComplete();
    if (animationBar.status == AnimationStatus.forward ||
        animationBar.status == AnimationStatus.completed) {
      animT.begin = newPercentComplete;
      await animationBar.reverse();
    } else if (animationBar.status == AnimationStatus.reverse ||
        animationBar.status == AnimationStatus.dismissed) {
      animT.end = newPercentComplete;
      await animationBar.forward();
    } else {
      print("wtf");
    }
    percentComplete = newPercentComplete;
  }

  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new Hero(
          tag: widget.todoObject.uuid + "_background",
          child: new Container(
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.circular(0.0),
            ),
          ),
        ),
        new Scaffold(
          backgroundColor: Colors.transparent,
          appBar: new AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            leading: new IconButton(
              icon: new Icon(
                Icons.arrow_back,
                color: Colors.grey,
              ),
              onPressed: () {
                TodoProvider.update(widget.todoObject);
                Navigator.of(context).pop();
              },
            ),
            actions: <Widget>[
              new Hero(
                tag: widget.todoObject.uuid + "_more_vert",
                child: new Material(
                  color: Colors.transparent,
                  type: MaterialType.transparency,
                  child: new IconButton(
                    icon: new Icon(
                        _isEditMode
                            ? FontAwesomeIcons.save
                            : FontAwesomeIcons.edit,
                        color: Colors.grey),
                    onPressed: () {
                      this.setState(() {
                        if (_isEditMode) {
                          TodoProvider.update(widget.todoObject);
                        }
                        _isEditMode = !_isEditMode;
                      });
                    },
                  ),
                ),
              )
            ],
          ),
          floatingActionButton: _isEditMode
              ? FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () => this.setState(() => widget.todoObject.tasks
                      .add(new Task(id: null, title: "Nouvelle tâche"))),
                )
              : null,
          body: new Padding(
            padding: const EdgeInsets.only(left: 40.0, right: 40.0, top: 35.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                _buildIcon(),
                _buildNumberOfTasks(),
                _buildTitle(),
                _buildProgressBar(),
                _buildTasks()
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildIcon() {
    return new Padding(
      padding: const EdgeInsets.only(bottom: 30.0),
      child: new Align(
        alignment: Alignment.bottomLeft,
        child: new Hero(
          tag: widget.todoObject.uuid + "_icon",
          child: new Container(
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              border: new Border.all(
                  color: Colors.grey.withAlpha(70),
                  style: BorderStyle.solid,
                  width: 1.0),
            ),
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Icon(Icons.toc, color: backgroundColor),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildNumberOfTasks() {
    return new Padding(
        padding: const EdgeInsets.only(bottom: 12.0),
        child: new Align(
            alignment: Alignment.bottomLeft,
            child: new Hero(
              tag: widget.todoObject.uuid + "_number_of_tasks",
              child: new Material(
                  color: Colors.transparent,
                  child: new Text(
                    widget.todoObject.tasks.length.toString() + " Tasks",
                    style: new TextStyle(),
                  )),
            )));
  }

  Widget _buildTitle() {
    return new Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: new Align(
          alignment: Alignment.bottomLeft,
          child: new Hero(
            tag: widget.todoObject.uuid + "_title",
            child: new Material(
              color: Colors.transparent,
              child: new Text(
                widget.todoObject.title,
                style: new TextStyle(fontSize: 30.0),
              ),
            ),
          )),
    );
  }

  Widget _buildProgressBar() {
    return new Padding(
      padding: const EdgeInsets.only(bottom: 30.0),
      child: new Align(
          alignment: Alignment.bottomLeft,
          child: new Hero(
              tag: widget.todoObject.uuid + "_progress_bar",
              child: new Material(
                color: Colors.transparent,
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: new LinearProgressIndicator(
                        value: barPercent,
                        backgroundColor: Colors.grey.withAlpha(50),
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(backgroundColor),
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(left: 5.0),
                      child:
                          new Text((barPercent * 100).round().toString() + "%"),
                    )
                  ],
                ),
              ))),
    );
  }

  Widget _buildTasks() {
    Map<DateTime, List<Task>> groupedTask =
        groupBy(widget.todoObject.tasks, (Task task) => task.deadline);

    print("grouped");
    print(groupedTask);
    return new Expanded(
        child: new ScaleTransition(
      scale: scaleAnimation,
      child: new ListView.builder(
        padding: const EdgeInsets.all(0.0),
        itemBuilder: (BuildContext context, int index) {
          DateTime currentDate =
              groupedTask.keys.toList()[index] ?? new DateTime.now();
          DateTime _now = new DateTime.now();
          DateTime today = new DateTime(_now.year, _now.month, _now.day);
          String dateString;
          if (currentDate.isBefore(today)) {
            dateString = "Previous - " + new DateFormat.E().format(currentDate);
          } else if (currentDate.isAtSameMomentAs(today)) {
            dateString = "Today";
          } else if (currentDate
              .isAtSameMomentAs(today.add(const Duration(days: 1)))) {
            dateString = "Tomorrow";
          } else {
            dateString = new DateFormat.E("fr_FR").format(currentDate);
          }
          List<Widget> tasks = [new Text(dateString)];
          groupedTask.values.toList()[index].forEach((task) {
            tasks.add(_buildTile(task, _isEditMode, index));
          });
          return new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: tasks,
          );
        },
        itemCount: groupedTask.values.length,
      ),
    ));
  }

  Widget _buildTile(Task task, bool isEditMode, int index) {
    TextEditingController _editController =
        new TextEditingController(text: task.title);
    _editController.addListener(() => task.title = _editController.text);
    if (isEditMode)
      return ListTile(
        title: new TextField(controller: _editController),
        trailing: IconButton(
            icon: Icon(Icons.calendar_today),
            onPressed: () async {
              final DateTime picked = await showDatePicker(
                  context: context,
                  initialDate: task.deadline ?? new DateTime.now(),
                  firstDate: DateTime(2019, 2),
                  lastDate: DateTime(2101));
              this.setState(() => task.deadline = picked);
            }),
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => this.setState(() => widget.todoObject.tasks
                .removeWhere((Task t) => t.uuid == task.uuid))),
      );
    else
      return new CustomCheckboxListTile(
        activeColor: backgroundColor,
        value: task.isCompleted(),
        onChanged: (value) {
          setState(() {
            task.setComplete(value);
            updateBarPercent();
          });
        },
        title: new Text(task.title),
        secondary: new Icon(Icons.alarm),
      );
  }
}
