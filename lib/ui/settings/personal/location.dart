import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/utils/StringsManager.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LocationSettingsPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const LocationSettingsPage({Key key, this.observer}) : super(key: key);

  @override
  LocationSettingsPageState createState() {
    return new LocationSettingsPageState(this.observer);
  }
}

class LocationSettingsPageState extends State<LocationSettingsPage> {
  final FirebaseAnalyticsObserver observer;

  final FocusNode addressFocus = FocusNode();
  final FocusNode zipCodeFocus = FocusNode();
  final FocusNode cityFocus = FocusNode();
  final FocusNode countryFocus = FocusNode();

  TextEditingController addressController = new TextEditingController();
  TextEditingController zipCodeController = new TextEditingController();
  TextEditingController cityController = new TextEditingController();
  TextEditingController countryController = new TextEditingController();

  LocationSettingsPageState(this.observer);

  @override
  void dispose() {
    super.dispose();
    addressController.dispose();
    zipCodeController.dispose();
    cityController.dispose();
    countryController.dispose();

    addressFocus.dispose();
    zipCodeFocus.dispose();
    countryFocus.dispose();
    cityFocus.dispose();
  }

  Future<Company> _updateCompany() async {
    return CompanyProvider.updateCompany(Company.updatePartialJson(
        location: Location(
            address: addressController.text,
            city: cityController.text,
            zipCode: zipCodeController.text,
            country: countryController.text)));
  }

  void _initTextController(Company c) {
    addressController.text = c.location.address;
    zipCodeController.text = c.location.zipCode;
    cityController.text = c.location.city;
    countryController.text = c.location.country;
  }

  @override
  void initState() {
    super.initState();

    CompanyProvider.fetchCompany().then((company) {
      _initTextController(company);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.transparent,
          title: new Text(StringManager.of(context).location),
        ),
        body: new Container(
            child: Column(children: <Widget>[
          new Expanded(
            child: new ListView(
              children: <Widget>[
                buildText(
                    addressFocus,
                    addressController,
                    TextInputType.text,
                    FontAwesomeIcons.addressBook,
                    StringManager.of(context).address,
                    false,
                    null),
                _buildSeparator(),
                buildText(
                  zipCodeFocus,
                  zipCodeController,
                  TextInputType.text,
                  FontAwesomeIcons.code,
                  StringManager.of(context).zipCode,
                  false,
                  null,
                ),
                _buildSeparator(),
                buildText(
                    cityFocus,
                    cityController,
                    TextInputType.text,
                    FontAwesomeIcons.city,
                    StringManager.of(context).city,
                    false,
                    null),
                _buildSeparator(),
                buildText(countryFocus, countryController, TextInputType.text,
                    FontAwesomeIcons.globeEurope, "Country", false, null),
                _buildSeparator(),
              ],
            ),
          ),
          buildButton(
              0.0,
              "Modifier",
              () => _updateCompany()
                  .then((Company comp) => Navigator.pop(context)))
        ])));
  }

  Widget _buildSeparator() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: buildSeparator(),
    );
  }
}
