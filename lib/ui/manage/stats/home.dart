import 'package:cash_register_app/models/stats.dart';
import 'package:cash_register_app/ressources/stats_api_provider.dart';
import 'package:cash_register_app/ui/manage/charts/donut_autolabel.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StatsPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const StatsPage(this.observer);

  @override
  _StatsPageState createState() => _StatsPageState(this.observer);
}

class _StatsPageState extends State<StatsPage> {
  final FirebaseAnalyticsObserver observer;

  Future<List<charts.Series<CaByProducts, String>>> __futureCaByProducts;
  Future<List<double>> _futureCa;
  Future<Map<String, dynamic>> _basicStatsFuture;

  static List<Function> _chartsCaFutures = [
    StatsProvider.getCAOverWeek,
    StatsProvider.getCAOverMonth,
    StatsProvider.getCAOverYear
  ];

  static List<String> chartDropdownItems = [
    'Last 7 days',
    'Last month',
    'Last year'
  ];
  String actualDropdown = chartDropdownItems[0];
  int actualChart = 0;

  _StatsPageState(this.observer);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    observer.analytics.setCurrentScreen(screenName: "/manage/stats");

    _basicStatsFuture = StatsProvider.getBasicStatistics();
    __futureCaByProducts = StatsProvider.getSeriesCaProduct();
    _futureCa = StatsProvider.getCAOverWeek();
  }

  @override
  Widget build(BuildContext context) {
    return _build();
  }

  Widget _build() {
    return FutureBuilder(
      future: _basicStatsFuture,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            Map<String, dynamic> data = snapshot.data;
            return buildBody(data);
        }
      },
    );
  }

  Widget buildBody(Map<String, dynamic> data) {
    print(data);
    print(data["totalCA"]);
    return StaggeredGridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 12.0,
      mainAxisSpacing: 12.0,
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      children: <Widget>[
        buildSalesWidget(data["totalSales"] as int),
        buildMeanCartWidget(
            data["totalCA"].toDouble(), data["totalSales"] as int),
        buildCategoryWidget(data["totalCategories"] as int),
        buildCAEvolution(),
        buildStockItemsWidget(data["totalProducts"] as int),
        buildPaymentGraph(),
        buildCategoryWidget(data["totalCategories"] as int),
      ],
      staggeredTiles: [
        StaggeredTile.extent(2, 110.0),
        StaggeredTile.extent(1, 180.0),
        StaggeredTile.extent(1, 180.0),
        StaggeredTile.extent(2, 220.0),
        StaggeredTile.extent(2, 110.0),
        StaggeredTile.extent(1, 200.0),
        StaggeredTile.extent(1, 200.0),
      ],
    );
  }

  Widget buildSalesWidget(int saleNumber) {
    return _buildTile(
      Padding(
        padding: const EdgeInsets.all(24.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Nombre de ventes',
                      style: TextStyle(color: Colors.blueAccent)),
                  Text(saleNumber.toString(),
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 34.0))
                ],
              ),
              Material(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(24.0),
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(FontAwesomeIcons.poll,
                        color: Colors.white, size: 30.0),
                  )))
            ]),
      ),
    );
  }

  Widget buildMeanCartWidget(double total, int saleNumber) {
    return _buildTile(
      Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Material(
                  color: Colors.teal,
                  shape: CircleBorder(),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(Icons.add_shopping_cart,
                        color: Colors.white, size: 30.0),
                  )),
              Padding(padding: EdgeInsets.only(bottom: 16.0)),
              Text(
                  saleNumber > 0
                      ? (total ~/ saleNumber).toString() + " €"
                      : "0",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 24.0)),
              Text('Panier moyen', style: TextStyle(color: Colors.black45)),
            ]),
      ),
    );
  }

  Widget buildCategoryWidget(int categoryNumber) {
    return _buildTile(
      Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Material(
                  color: Colors.amber,
                  shape: CircleBorder(),
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Icon(Icons.notifications,
                        color: Colors.white, size: 30.0),
                  )),
              Padding(padding: EdgeInsets.only(bottom: 16.0)),
              Text(categoryNumber.toString(),
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 24.0)),
              Text('Categories', style: TextStyle(color: Colors.black45)),
            ]),
      ),
    );
  }

  Widget _buildCaEvolutionBody(List<double> data) {
    return _buildTile(
      Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Chiffre d'affaire",
                          style: TextStyle(color: Colors.green)),
                      Text(
                          data
                                  .fold(0, (total, other) => total + other)
                                  .toInt()
                                  .toString() +
                              " €",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 34.0)),
                    ],
                  ),
                  DropdownButton(
                      isDense: true,
                      value: actualDropdown,
                      onChanged: (String value) => setState(() {
                            actualDropdown = value;
                            actualChart = chartDropdownItems.indexOf(value); //
                            _futureCa = _chartsCaFutures[chartDropdownItems
                                .indexOf(value)](); // Refresh the chart
                          }),
                      items: chartDropdownItems.map((String title) {
                        return DropdownMenuItem(
                          value: title,
                          child: Text(title,
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14.0)),
                        );
                      }).toList())
                ],
              ),
              Padding(padding: EdgeInsets.only(bottom: 4.0)),
              data.length > 0
                  ? Sparkline(
                      data: data,
                      lineWidth: 5.0,
                      lineColor: Colors.greenAccent,
                    )
                  : Center(child: Text("Pas de données"))
            ],
          )),
    );
  }

  Widget buildCAEvolution() {
    return FutureBuilder(
      future: _futureCa,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Press button to start.');
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            List<double> data = snapshot.data;
            return _buildCaEvolutionBody(data);
        }
      },
    );
  }

  Widget buildStockItemsWidget(int productNumber) {
    return _buildTile(
      Padding(
        padding: const EdgeInsets.all(24.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Produits en stock',
                      style: TextStyle(color: Colors.redAccent)),
                  Text(productNumber.toString(),
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 34.0))
                ],
              ),
              Material(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(24.0),
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Icon(Icons.store, color: Colors.white, size: 30.0),
                  )))
            ]),
      ),
    );
  }

  Widget buildPaymentGraph() {
    return _buildTile(FutureBuilder(
        future: __futureCaByProducts,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('Press button to start.');
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              if (snapshot.hasError) return Text('Error: ${snapshot.error}');
              List<charts.Series<dynamic, dynamic>> series = snapshot.data;
              if (series == null) {
                return Center(child: Text("Pas de données"));
              }
              return Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: new SizedBox(
                      height: 500.0, child: DonutAutoLabelChart(series)));
          }
        }));
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
        elevation: 14.0,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x802196F3),
        child: InkWell(
            // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }
}
