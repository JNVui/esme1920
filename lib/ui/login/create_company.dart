import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ressources/seller_api_provider.dart';
import 'package:cash_register_app/ui/base.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CreateCompanyPage extends StatefulWidget {
  final bool isTunnel;
  final FirebaseAnalyticsObserver observer;

  const CreateCompanyPage({Key key, this.isTunnel, this.observer})
      : super(key: key);

  @override
  _CreateCompanyPageState createState() =>
      _CreateCompanyPageState(this.isTunnel, this.observer);
}

class _CreateCompanyPageState extends State<CreateCompanyPage> {
  final bool isTunnel;
  final FirebaseAnalyticsObserver observer;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode myFocusName = FocusNode();
  final FocusNode myFocusSiret = FocusNode();
  final FocusNode myFocusAddress = FocusNode();
  final FocusNode myFocusPhoneNumber = FocusNode();

  TextEditingController siretController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();

  _CreateCompanyPageState(this.isTunnel, this.observer);

  @override
  void initState() {
    observer.analytics.setCurrentScreen(screenName: "/company/create");
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    siretController.dispose();
    nameController.dispose();
    addressController.dispose();
    phoneController.dispose();

    myFocusName.dispose();
    myFocusSiret.dispose();
    myFocusAddress.dispose();
    myFocusPhoneNumber.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isTunnel
        ? new Scaffold(
            key: _scaffoldKey,
            body: Center(child: _buildCreate(context)),
            backgroundColor: Colors.transparent)
        : Container(
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: [
                    Theme.Colors.loginGradientStart,
                    Theme.Colors.loginGradientEnd
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: new Scaffold(
                key: _scaffoldKey,
                appBar: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                ),
                backgroundColor: Colors.transparent,
                body: Padding(
                    padding: EdgeInsets.only(top: 150.0),
                    child: Center(child: _buildCreate(context)))));
  }

  Widget _buildCreate(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 300.0,
                  child: ListView(
                    children: <Widget>[
                      buildText(
                          myFocusSiret,
                          siretController,
                          TextInputType.text,
                          FontAwesomeIcons.idCard,
                          "Siret",
                          false,
                          null),
                      buildSeparator(),
                      buildText(myFocusName, nameController, TextInputType.text,
                          FontAwesomeIcons.signature, "Nom", false, null),
                      buildSeparator(),
                      buildText(
                          myFocusAddress,
                          addressController,
                          null,
                          FontAwesomeIcons.mapMarkedAlt,
                          "Adresse",
                          false,
                          null),
                      buildSeparator(),
                      buildText(myFocusPhoneNumber, phoneController, null,
                          FontAwesomeIcons.phone, "Téléphone", false, null)
                    ],
                  ),
                ),
              ),
              buildButton(290.0, "Créer", () {
                print("Create clicked");
                Company company = new Company(
                    id: siretController.text,
                    name: nameController.text,
                    location: new Location(address: addressController.text),
                    contact: new Contact(phoneNumber: phoneController.text));
                CompanyProvider
                    .create(company)
                    .then((Company c) => isTunnel
                        ? SellerProvider.getMe().then((User u) => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      BasePage(u, this.observer)),
                            ))
                        : Navigator.pop(context, true))
                    .catchError((e) =>
                        showInSnackBar(_scaffoldKey, context, e.toString()));
              })
            ],
          )
        ],
      ),
    );
  }
}
