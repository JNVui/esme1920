import 'package:cash_register_app/models/bill.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CreateBillPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;
  final BillModel billModel;

  const CreateBillPage({Key key, this.observer, this.billModel})
      : super(key: key);

  @override
  CreateBillPageState createState() =>
      new CreateBillPageState(this.observer, billModel: billModel);
}

class CreateBillPageState extends State<CreateBillPage> {
  final FirebaseAnalyticsObserver observer;

  final FocusNode idFocus = FocusNode();
  final FocusNode sellerNumberFocus = FocusNode();
  final FocusNode buyerFocus = FocusNode();
  final FocusNode priceFocus = FocusNode();

  TextEditingController idController = new TextEditingController();
  TextEditingController sellerController = new TextEditingController();
  TextEditingController buyerController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();

  List<Widget> _listFile = new List<Widget>();
  var test = true;

  CreateBillPageState(this.observer, {BillModel billModel}) {
    _initTextController(billModel);
  }

  @override
  void dispose() {
    super.dispose();
    idFocus.dispose();
    sellerNumberFocus.dispose();
    buyerFocus.dispose();
    priceFocus.dispose();

    idController.dispose();
    sellerController.dispose();
    buyerController.dispose();
    priceController.dispose();
  }

  void _updateCompany() {
    var bill = new BillModel(
        id: idController.text,
        seller: sellerController.text,
        buyer: buyerController.text,
        price: double.tryParse(priceController.text));

    Navigator.pop(context, bill);
  }

  void _initTextController(BillModel bill) {
    idController.text = bill?.id;
    sellerController.text = bill?.seller;
    buyerController.text = bill?.buyer;
    priceController.text = bill?.price?.toString();
  }

  @override
  void initState() {
    super.initState();

    _initializeLizeList();
  }

  void _initializeLizeList() {
    _listFile.add(_addButton());
  }

  Widget _addButton() {
    return SizedBox(
      width: 100.0,
      height: 100.0,
      child: Card(
        child: IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            setState(() {
              _listFile.add(_addButton());
            });
          },
        ),
      ),
    );
  }

  Widget _addFile() {
    return SizedBox(
      width: 100.0,
      height: 100.0,
      child: Card(
        child: IconButton(
          icon: Icon(Icons.insert_drive_file),
          onPressed: () {
            setState(() {
              print("hello");
            });
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: new Text("Add Bill"),
        ),
        body: ListView(children: <Widget>[
          buildText(idFocus, idController, TextInputType.text,
              FontAwesomeIcons.envelope, "", false, null),
          _buildSeparator(),
          buildText(
            sellerNumberFocus,
            sellerController,
            TextInputType.text,
            FontAwesomeIcons.envelope,
            "",
            false,
            null,
          ),
          _buildSeparator(),
          buildText(buyerFocus, buyerController, TextInputType.text,
              FontAwesomeIcons.code, "", false, null),
          _buildSeparator(),
          buildText(priceFocus, priceController, TextInputType.text,
              FontAwesomeIcons.code, "", false, null),
          _buildSeparator(),
          SizedBox(
            height: 100,
            child: !test
                ? Icon(Icons.add)
                : ListView.builder(
                    itemCount: _listFile.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return _listFile[index];
                    },
                    scrollDirection: Axis.horizontal,
                  ),
          ),
          buildButton(0.0, "Ajouter", () => _updateCompany())
        ]));
  }

  Widget _buildSeparator() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: buildSeparator(),
    );
  }
}
