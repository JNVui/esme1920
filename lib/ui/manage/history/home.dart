import 'package:cash_register_app/models/payment.dart';
import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/ressources/sale_api_provider.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HistoryPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const HistoryPage({Key key, this.observer}) : super(key: key);

  @override
  HistoryPageState createState() => HistoryPageState(this.observer);
}

class HistoryPageState extends State<HistoryPage> {

  final FirebaseAnalyticsObserver observer;

  Future<Sales> _companySales;
  int _selectedGraphIndex = 0;
  static List<String> chartDropdownItems = [
    'Last 7 days',
    'Last month',
    'Last year'
  ];

  String actualDropdown = chartDropdownItems[0];
  int actualChart = 0;

  HistoryPageState(this.observer);

  @override
  void initState() {
    // TODO: implement initState
    observer.analytics.setCurrentScreen(screenName: "/manage/history");

    super.initState();
    DateTime now = DateTime.now();
    _companySales = SaleProvider.fetchBetweenTwoDates(
        now, DateTime(now.year, now.month, now.day - 1));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _companySales,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Press button to start.');
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            Sales sales = snapshot.data;
            return _build(context, sales);
        }
      },
    );
  }

  Widget _build(BuildContext context, Sales sales) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            buildCAEvolution(sales),
            Expanded(
              child: ListView.builder(
                  padding: new EdgeInsets.all(10.0),
                  itemCount: sales.sales.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildTile(
                        buildHistoryWidget(sales.sales[index], index));
                  }),
            ),
          ],
        ),
      ),
    );
  }

  charts.Color getColor(bool isHighlighted) {
    Color color = isHighlighted ? Colors.black : Colors.blue;

    return new charts.Color(
        r: color.red, g: color.green, b: color.blue, a: color.alpha);
  }

  Widget buildGraph(Sales sales) {
    if (sales.sales.isEmpty) return Container();
    var series = [
      new charts.Series(
        id: 'Sales',
        domainFn: (Sale sale, _) => sale.createdAt.toString(),
        measureFn: (Sale sale, _) => sale.totalPrice,
        colorFn: (Sale sale, int index) =>
            getColor(_selectedGraphIndex == index),
        data: sales.sales,
      ),
    ];

    var chart = new charts.BarChart(
      series,
      animate: true,
      domainAxis: new charts.OrdinalAxisSpec(
          // Make sure that we draw the domain axis line.
          showAxisLine: true,
          // But don't draw anything else.
          renderSpec: new charts.NoneRenderSpec()),
    );

    var chartWidget = new Padding(
      padding: new EdgeInsets.all(10.0),
      child: new SizedBox(
        height: 100.0,
        child: chart,
      ),
    );
    return chartWidget;
  }

  Widget buildCAEvolution(Sales sales) {
    return Container(
        margin: const EdgeInsets.all(10.0),
        child: _buildTile(
          Container(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Chiffre d'affaire",
                              style: TextStyle(color: Colors.green)),
                          Text(sales.total.toInt().toString() + " €",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 34.0)),
                        ],
                      ),
                      IconButton(
                        icon: Icon(FontAwesomeIcons.calendarAlt),
                        onPressed: () async {
                          DateRagePicker.showDatePicker(
                                  context: context,
                                  initialFirstDate: new DateTime.now(),
                                  initialLastDate: (new DateTime.now())
                                      .add(new Duration(days: 7)),
                                  firstDate: new DateTime(2018),
                                  lastDate: new DateTime(2040))
                              .then((List<DateTime> picked) {
                            if (picked != null && picked.length == 2) {
                              _companySales = SaleProvider.fetchBetweenTwoDates(
                                  picked[1], picked[0]);
                              this.setState(() {});
                            }
                          });
                        },
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 4.0)),
                  buildGraph(sales)
                ],
              )),
        ));
  }

  Widget buildHistoryWidget(Sale sale, int index) {
    return _buildTile(
        Container(
          padding: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 24.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Total', style: TextStyle(color: Colors.black54)),
                    Text(sale.totalPrice.toString(),
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                            fontSize: 34.0))
                  ],
                ),
                getBulletForHistory(sale.paymentType)
              ]),
        ),
        index: index);
  }

  Widget _buildTile(Widget child, {Function() onTap, int index}) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
        child: Material(
            elevation: 14.0,
            borderRadius: BorderRadius.circular(12.0),
            shadowColor: Color(0x802196F3),
            child: InkWell(
                // Do onTap() if it isn't null, otherwise do print()
                onTap: () => this.setState(() {
                      _selectedGraphIndex = index;
                    }),
                child: child)));
  }
}
