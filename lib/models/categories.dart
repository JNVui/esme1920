import 'dart:convert';

import 'package:cash_register_app/models/product.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';

class Category {
  final String id;
  String name;
  String slug;
  String parent;
  Category parentNode;
  final int depth;
  bool selected = false;

  Map<String, Category> children;
  List<ProductModel> products;

  Category(
      {this.id,
      this.name,
      this.slug,
      this.parent,
      this.depth,
      this.children,
      this.products,
      this.parentNode});

  get parentId {
    return parent != null ? parent : null;
  }

  bool get hasChildren {
    if (this.children == null) return false;
    if (this.children.isEmpty) return false;
    if (this.children.values.isEmpty)
      return false;
    else
      return true;
  }

  get parentName {
    return parentNode != null ? parentNode.name : null;
  }

  get displayName {
    return "    " * (depth ?? 0) + this.name;
  }

  factory Category.fromJson(Map<String, dynamic> json, {Category parentNode}) {
    Category category = Category(
        id: json["data"]["_id"],
        name: json["data"]["name"],
        slug: json["data"]["slug"],
        parent: json["data"]["parent"],
        depth: json["depth"]);
    category.children = Map<String, Category>.from(json["children"].map(
        (id, cat) => MapEntry<String, Category>(
            id, Category.fromJson(cat, parentNode: category))));
    category.parentNode = parentNode;
    return category;
  }

  factory Category.simpleFromJson(Map<String, dynamic> json) {
    return Category(
        id: json["_id"],
        name: json["name"],
        slug: json["slug"],
        parent: json["parent"],
        depth: null);
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': this.id,
      'name': this.name,
      'slug': this.slug,
      'parent': this.parentNode != null ? this.parentNode.id : null,
      'depth': this.depth,
    };
  }

  Map<String, dynamic> getCleanJson() {
    Map<String, dynamic> data = this.toJson();
    data.removeWhere(
        (String key, value) => ((value == null) & (key != "parent")));
    data.removeWhere((String key, value) => ["_id", "depth"].contains(key));
    return data;
  }
}

class Categories {
  final Map<String, Category> hierarchy;
  final List<Category> categories;
  Map<String, Category> mappedCategories;

  Categories(this.hierarchy, this.categories) {
    mappedCategories = new Map<String, Category>.fromIterable(this.categories,
        key: (category) => category.id, value: (category) => category);
  }

  factory Categories.fromJson(Map<String, dynamic> json) {
    return Categories(
        Map<String, Category>.from(json["content"]
            .map((id, category) => MapEntry(id, Category.fromJson(category)))),
        List<Category>.from(json["categories"]
            .map((category) => Category.simpleFromJson(category))));
  }

  static Future<Categories> fetchWithHierarchy() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.GET, '/api/v1/company/$companyId/categories',
        queryParameters: {"hierarchy": "true"});

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);

      Categories categories = Categories.fromJson(result);
      return categories;
    } else {
      throw Exception(response.body);
    }
  }

  get entries {
    return List<Category>.from(this.hierarchy.values);
  }

  String getNameFromId(String categoryId) {
    return mappedCategories[categoryId].name;
  }

  Map<String, dynamic> toJson() {
    return {
      'categories':
          this.categories.map((Category cat) => cat.toJson()).toList(),
    };
  }
}
