enum TAX { T20, T10, T85, T5 }

TAX taxFromDouble(double tax) {
  if (tax == 20.0) return TAX.T20;
  if (tax == 10.0) return TAX.T10;
  if (tax == 8.5) return TAX.T85;
  if (tax == 5.5) return TAX.T5;
  return null;
}

double taxToDouble(TAX tax) {
  switch (tax) {
    case TAX.T20:
      return 20.0;
    case TAX.T10:
      return 10.0;
    case TAX.T85:
      return 8.5;
    case TAX.T5:
      return 5.5;
  }
}
