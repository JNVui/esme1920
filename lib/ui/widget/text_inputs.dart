import 'package:flutter/material.dart';

Widget textFieldFormWithController(
    {name,
    controller,
    obscureText = false,
    keyboardType = TextInputType.text,
    textInputAction = TextInputAction.next}) {
  return TextFormField(
    decoration: InputDecoration(
        labelText: name,
        labelStyle: TextStyle(
          fontFamily: 'Montserrat',
          fontWeight: FontWeight.bold,
          color: Colors.grey,
        ),
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.green))),
    keyboardType: keyboardType,
    obscureText: obscureText,
    onSaved: (String value) {},
    controller: controller,
    textInputAction: TextInputAction.next,
  );
}
