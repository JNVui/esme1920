import 'package:cash_register_app/utils/resources/strings_fr.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class StringManager {
  StringManager(this.locale);

  final Locale locale;

  static StringManager of(BuildContext context) {
    return Localizations.of<StringManager>(context, StringManager);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    "en": StringsFR.map,
    "fr": StringsFR.map,
  };

  String getTextFromName(String name) {
    return _localizedValues[locale.languageCode][name];
  }

  String get sumup {
    return _localizedValues[locale.languageCode]['sumup'];
  }

  String get shopSettings {
    return _localizedValues[locale.languageCode]['shop_settings'];
  }

  String get shopSettingsSubtitle {
    return _localizedValues[locale.languageCode]['shop_settings_subtitle'];
  }

  String get activePaypal {
    return _localizedValues[locale.languageCode]['active_paypal'];
  }

  String get activeSumUp {
    return _localizedValues[locale.languageCode]['active_sumup'];
  }

  String get cashfund {
    return _localizedValues[locale.languageCode]['cashfund'];
  }

  String get personal {
    return _localizedValues[locale.languageCode]['personal'];
  }

  String get name {
    return _localizedValues[locale.languageCode]['name'];
  }

  String get storeName {
    return _localizedValues[locale.languageCode]['store_name'];
  }

  String get email {
    return _localizedValues[locale.languageCode]['email'];
  }

  String get rcNumber {
    return _localizedValues[locale.languageCode]['rc_number'];
  }

  String get tvaNumber {
    return _localizedValues[locale.languageCode]['tva_number'];
  }

  String get address {
    return _localizedValues[locale.languageCode]['address'];
  }

  String get zipCode {
    return _localizedValues[locale.languageCode]['zipcode'];
  }

  String get phoneNumber {
    return _localizedValues[locale.languageCode]['phone_number'];
  }

  String get faxNumber {
    return _localizedValues[locale.languageCode]['fax_number'];
  }

  String get city {
    return _localizedValues[locale.languageCode]['city'];
  }

  String get title {
    return _localizedValues[locale.languageCode]['title'];
  }

  String get webSite {
    return _localizedValues[locale.languageCode]['web_site'];
  }

  String get activityType {
    return _localizedValues[locale.languageCode]['activity_type'];
  }

  String get location {
    return _localizedValues[locale.languageCode]['location'];
  }

  String get locationSettingsSubtitle {
    return _localizedValues[locale.languageCode]['locationSettingsSubtitle'];
  }

  String get contact {
    return _localizedValues[locale.languageCode]['contact'];
  }

  String get contactSettingsSubtitle {
    return _localizedValues[locale.languageCode]['contactSettingsSubtitle'];
  }
}

class StringManagerDelegate extends LocalizationsDelegate<StringManager> {
  const StringManagerDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'fr'].contains(locale.languageCode);

  @override
  Future<StringManager> load(Locale locale) {
    return SynchronousFuture<StringManager>(StringManager(locale));
  }

  @override
  bool shouldReload(StringManagerDelegate old) => false;
}
