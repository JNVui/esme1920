import 'dart:convert';

import 'package:cash_register_app/models/customer.dart';
import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/ressources/utils.dart';


class CustomerProvider{
  static Future<Customer> fetchOrCreate(Customer customer) async {
    final response = await makeRequest(METHOD.POST, '/api/v1/customer',
        body: json.encode(customer.toAuthJson()));

    if (response.statusCode == 200) {
      print(response.body);
      Map<String, dynamic> result = json.decode(response.body);
      Customer customer = Customer.fromJson(result['content']);
      return customer;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<Customer> addSale(Customer customer, Sale sale) async {
    final response = await makeRequest(
        METHOD.POST, '/api/v1/customer/${customer.id}/sale',
        body: json.encode(sale.toJson()));
    if (response.statusCode == 200) {
      print(response.body);
      Map<String, dynamic> result = json.decode(response.body);
      Customer customer = Customer.fromJson(result['content']);
      return customer;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<List<Customer>> fetchAll() async {
    final response = await makeRequest(METHOD.GET, '/api/v1/customer');
    if (response.statusCode == 200) {
      List<Customer> customers = List<Customer>.from(json
          .decode(response.body)["content"]
          .map((customer) => Customer.fromJson(customer)));
      return customers;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }
}