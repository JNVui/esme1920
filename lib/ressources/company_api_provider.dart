import 'dart:convert';

import 'package:cash_register_app/models/categories.dart';
import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';


const String COMPANY_ME_ROUTE = '/api/v1/me/company';
const String COMPANY_ROUTE = '/api/v1/company';
const String CREATE_COMPANY_ROUTE = COMPANY_ME_ROUTE + '/create';


class CompanyProvider{
  static Future<Company> create(Company company) async {
    final response =
    await makeRequest(METHOD.POST,  '/api/v1/me/company/create', body: json.encode(company.toCreateJson()));

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Company company = Company.fromJson(result['content']);
      return company;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  static Future<Company> fetch() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, '/api/v1/me/company/$companyId');

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Company company = Company.fromJson(result['content']);
      company.categories = await Categories.fetchWithHierarchy();
      return company;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  static Future<Company> fetchStock() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();
    final response =
    await makeRequest(METHOD.GET, '/api/v1/company/$companyId/stock');

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Company company = Company.fromJson(result['content']);
      company.categories = await Categories.fetchWithHierarchy();
      return company;
    } else {
      // If that call was not successful, throw an error.
      throw Exception(response.body);
    }
  }

  /// Create a Company
  static Future<Company> createCompany(Company company) async {
    final response = await makeRequest(METHOD.POST, CREATE_COMPANY_ROUTE,
        body: json.encode(company.toCreateJson()));
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Company company = Company.fromJson(result['content']);
      return company;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  ///Update Company
  static Future<Company> updateCompany(Map<String, dynamic> partialJson) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(METHOD.PUT, '$COMPANY_ROUTE/$companyId',
        body: json.encode(partialJson));

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Company company = Company.fromJson(result['content']);
      return company;
    } else {
      throw Exception(json.decode(response.body)["message"]);
    }
  }

  /// Get Company
  static Future<Company> fetchCompany() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response =
    await makeRequest(METHOD.GET, '$COMPANY_ROUTE/$companyId/infos');

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Company company = Company.fromJson(result['content']);
      company.categories = await Categories.fetchWithHierarchy();
      return company;
    } else {
// If that call was not successful, throw an error.
      print(response.body);
      throw Exception(response.body);
    }
  }

  static Future<Sales> closeSales({String token}) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    if (token == null) token = await LoginSharedPreferences.getResisterIsOpen();

    final response = await makeRequest(
        METHOD.GET, '$COMPANY_ROUTE/$companyId/sales/close',
        queryParameters: {"token": token});

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Sales sales = Sales.fromJson(
          List<dynamic>.from(result['content'].map((json) => json["sales"])));
      return sales;
    } else {
      throw Exception(response.body);
    }
  }

  static Future<Sales> close(String token) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.GET, '$COMPANY_ROUTE/$companyId/close',
        queryParameters: {"token": token});

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      Sales sales = Sales.fromJson(
          List<dynamic>.from(result['content'].map((json) => json["sales"])));
      return sales;
    } else {
      throw Exception(response.body);
    }
  }
}