import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Future showAskDialog(context, name, hintText, successText,
    {askValue = true}) async {
  return showDialog(
      context: context,
      builder: (BuildContext context) =>
          askAlertDialog(context, name, hintText, successText, askValue));
}

Widget askAlertDialog(context, name, hintText, successText, askValue) {
  FocusNode myFocus = new FocusNode();
  TextEditingController myController = new TextEditingController();

  return new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.only(top: 10.0),
      content: Container(
        width: 300.0,
        height: 215.0,
        color: Colors.transparent,
        padding: EdgeInsets.only(top: 23.0),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.topCenter,
              overflow: Overflow.visible,
              children: <Widget>[
                Card(
                  elevation: 2.0,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Container(
                    width: 300.0,
                    height: 150.0,
                    child: Column(
                      children: <Widget>[
                        _buildTitle(name),
                        buildText(myFocus, myController, TextInputType.number,
                            FontAwesomeIcons.idCard, hintText, false, null),
                        buildSeparator()
                      ],
                    ),
                  ),
                ),
                buildButton(140.0, successText,
                    () => Navigator.pop(context, myController.text))
              ],
            )
          ],
        ),
      ));
}

Widget _buildTitle(name) {
  return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Text(name,
          style: TextStyle(
              fontFamily: "WorkSansSemiBold",
              fontSize: 30.0,
              color: Colors.black)));
}
