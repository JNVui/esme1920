class BillModel {
  BillModel({this.id, this.seller, this.buyer, this.price, this.tag}) {
    tag = id + " " + seller + " " + buyer + " " + price.toString();
  }

  String id;
  String seller;
  String buyer;
  String tag;
  double price;

  factory BillModel.fromJson(Map<String, dynamic> json) {
    return BillModel(
      id: json['_id'] ?? json['id'],
      seller: json['seller'],
      buyer: json['buyer'],
      price: json['price'],
      tag: json['tag'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'seller': seller,
      'buyer': buyer,
      'price': price,
      'tag': tag,
    };
  }
}
