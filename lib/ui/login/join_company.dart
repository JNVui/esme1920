import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ressources/seller_api_provider.dart';
import 'package:cash_register_app/ui/base.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class JoinCompanyPage extends StatefulWidget {
  final bool isTunnel;
  final FirebaseAnalyticsObserver observer;

  const JoinCompanyPage({Key key, this.isTunnel, this.observer})
      : super(key: key);

  @override
  _JoinCompanyPageState createState() =>
      _JoinCompanyPageState(this.isTunnel, this.observer);
}

class _JoinCompanyPageState extends State<JoinCompanyPage> {
  final bool isTunnel;
  final FirebaseAnalyticsObserver observer;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode myFocusSiret = FocusNode();
  final FocusNode myFocusToken = FocusNode();

  TextEditingController siretController = new TextEditingController();
  TextEditingController tokenController = new TextEditingController();

  _JoinCompanyPageState(this.isTunnel, this.observer);

  @override
  void initState() {
    observer.analytics.setCurrentScreen(screenName: "/company/join");

    super.initState();
  }

  @override
  void dispose() {
    myFocusSiret.dispose();
    myFocusToken.dispose();
    siretController.dispose();
    tokenController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isTunnel
        ? new Scaffold(
            key: _scaffoldKey,
            body: Center(child: _buildJoin(context)),
            backgroundColor: Colors.transparent)
        : Container(
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: [
                    Theme.Colors.loginGradientStart,
                    Theme.Colors.loginGradientEnd
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: new Scaffold(
                key: _scaffoldKey,
                appBar: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                ),
                backgroundColor: Colors.transparent,
                body: Padding(
                    padding: EdgeInsets.only(top: 150.0),
                    child: Center(child: _buildJoin(context)))));
  }

  Widget _buildJoin(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 150.0,
                  child: Column(
                    children: <Widget>[
                      buildText(
                          myFocusSiret,
                          siretController,
                          TextInputType.text,
                          FontAwesomeIcons.idCard,
                          "Siret",
                          false,
                          null),
                      buildSeparator(),
                      buildText(
                          myFocusToken,
                          tokenController,
                          TextInputType.text,
                          FontAwesomeIcons.code,
                          "Token",
                          false,
                          null),
                    ],
                  ),
                ),
              ),
              buildButton(140.0, "Rejoindre", () {
                SellerProvider.joinCompany(siretController.text, tokenController.text)
                    .then((String s) => isTunnel
                        ? SellerProvider.getMe().then((User u) => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      BasePage(u, this.observer)),
                            ))
                        : Navigator.pop(context, true))
                    .catchError((e) =>
                        showInSnackBar(_scaffoldKey, context, e.toString()));
              })
            ],
          )
        ],
      ),
    );
  }
}
