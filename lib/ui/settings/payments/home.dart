import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_settings/flutter_cupertino_settings.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sum_up_plugin/sum_up_plugin.dart';

class PaymentSettingsPage extends StatefulWidget {
  final Future<Company> companyFuture;
  final FirebaseAnalyticsObserver observer;

  const PaymentSettingsPage({this.companyFuture, this.observer});

  @override
  PaymentSettingsPageState createState() {
    return new PaymentSettingsPageState(this.companyFuture, this.observer);
  }
}

class PaymentSettingsPageState extends State<PaymentSettingsPage> {
  Future<Company> companyFuture;
  final FirebaseAnalyticsObserver observer;

  final FocusNode payPalFocus = FocusNode();
  final FocusNode lydiaFocus = FocusNode();
  TextEditingController payPalController = new TextEditingController();
  TextEditingController lydiaController = new TextEditingController();

  PaymentSettingsPageState(this.companyFuture, this.observer);

  bool isSumUpEnable = false;
  bool isPayPalEnable = false;
  bool isLydiaEnable = false;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    LoginSharedPreferences.isSumUpEnable().then((value) {
      setState(() {
        if (value != null) isSumUpEnable = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: Text("Paiement"),
        ),
        body: new CupertinoSettings(
            items: <Widget>[]
              ..addAll(_buildSumUpSettings())
              ..add(new CSHeader("Paypal"))
              ..addAll(_buildPayPalSettings())
              ..add(new CSHeader("Lydia"))
              ..addAll(_buildLydiaSettings())));
  }

  List<Widget> _buildSumUpSettings() {
    var sumUpPluggin = SumUpPlugin(
        clientId: LoginSharedPreferences.getSumupId(),
        clientSecret: LoginSharedPreferences.getSumupSecretId());

    return <Widget>[
      new CSControl(
        'Active SumUp',
        new CupertinoSwitch(
          value: isSumUpEnable,
          onChanged: (value) {
            LoginSharedPreferences.setIsSumUpEnable(value);
            setState(() {
              isSumUpEnable = value;
            });
          },
        ),
        style: CSWidgetStyle(
            icon:
                const Icon(FontAwesomeIcons.creditCard, color: Colors.black54)),
      ),
      isSumUpEnable
          ? new CSButton(CSButtonType.DEFAULT, "Login", () {
              sumUpPluggin
                  .login(context)
                  .then((String s) => print("Hello" + s))
                  .catchError((e) => print(e));

              SumUpPlugin.isSumUpTokenValid()
                  .then((bool s) => print("Hello = " + s.toString()))
                  .catchError((e) => print("GoodBye" + e));
            })
          : Container(),
      isSumUpEnable
          ? new CSButton(CSButtonType.DEFAULT, "Configure", () {
              SumUpPlugin.paymentPreferences();
            })
          : Container()
    ];
  }

  List<Widget> _buildPayPalSettings() {
    return <Widget>[
      CSControl(
        'Active PayPal',
        new CupertinoSwitch(
          value: isPayPalEnable,
          onChanged: (value) {
            LoginSharedPreferences.setIsPaypalEnable(value);
            setState(() {
              isPayPalEnable = value;
            });
          },
        ),
        style: CSWidgetStyle(
            icon: const Icon(FontAwesomeIcons.paypal, color: Colors.black54)),
      ),
      isPayPalEnable
          ? CSWidget(TextField(
              focusNode: payPalFocus,
              controller: payPalController,
            ))
          : Container()
    ];
  }

  List<Widget> _buildLydiaSettings() {
    return <Widget>[
      CSControl(
        'Active Lydia',
        new CupertinoSwitch(
          value: isLydiaEnable,
          onChanged: (value) {
            LoginSharedPreferences.setIsPaypalEnable(value);
            setState(() {
              isLydiaEnable = value;
            });
          },
        ),
        style: CSWidgetStyle(
            icon: const Icon(FontAwesomeIcons.mobile, color: Colors.black54)),
      ),
      isLydiaEnable
          ? CSWidget(TextField(
              focusNode: lydiaFocus,
              controller: lydiaController,
            ))
          : Container()
    ];
  }
}
