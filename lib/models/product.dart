import 'package:cash_register_app/models/tax.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:quiver/core.dart';

String baseObjectRedirectUrl = apiUrl + "/api/v1/company/item/";

class ProductModel {
  ProductModel(
      {this.id,
      this.name,
      this.aisle,
      this.salePrice,
      this.costPrice,
      this.subtitle,
      this.photoUrl,
      this.url,
      this.code,
      this.brands,
      this.category,
      this.externalId,
      this.discount,
      this.tax,
      this.quantity,
      this.companyId});

  String id;
  String name;
  String aisle;
  double salePrice;
  double discount;
  TAX tax;
  double costPrice;
  String subtitle;
  String photoUrl;
  String url;
  String code;
  String brands;
  String category;
  String externalId;
  String companyId;
  int qtyInCart = 0;
  int quantity;

  double get realPrice {
    return this.salePrice * ((100.0 - (this.discount ?? 0)) / 100);
  }

  double get taxQuantity {
    return this.realPrice * ((this.taxValue ?? 0) / 100);
  }

  double get discountValue {
    return this.salePrice - this.realPrice;
  }

  bool get hasDiscount {
    return this.discount != null && this.discount != 0;
  }

  double get taxValue {
    return taxToDouble(this.tax);
  }

  String get getCalculationString {
    if (this.hasDiscount) {
      return '${this.qtyInCart.toString()} x (${this.salePrice.toString()} - ${this.discountValue.toStringAsFixed(1)}) = ${(this.qtyInCart * this.realPrice).toStringAsFixed(1)} €';
    } else {
      return '${this.qtyInCart.toString()} x ${this.salePrice.toStringAsFixed(1)}'
          ' = ${(this.qtyInCart * this.salePrice).toStringAsFixed(1)} €';
    }
  }

  String get qrCodeValue {
    return baseObjectRedirectUrl + "${this.id}/redirect";
  }

  bool correspondToQrCode(String codeUrl) {
    return codeUrl == this.qrCodeValue;
  }

  static String getCodeFromUrl(String url) {
    List<String> splittedString = url.split("/");
    return splittedString[splittedString.length - 2];
  }

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
        id: json['_id'] ?? json['id'],
        name: json['name'],
        aisle: json['aisle'],
        subtitle: json['subtitle'],
        photoUrl: json['photoUrl'],
        url: json['url'],
        code: json['code'],
        brands: json['brands'],
        category: json['category'],
        externalId: json['externalId'],
        tax: json['tax'] == null ? null : taxFromDouble(json['tax'].toDouble()),
        salePrice:
            json['salePrice'] == null ? null : json['salePrice'].toDouble(),
        costPrice:
            json['costPrice'] == null ? null : json['costPrice'].toDouble(),
        discount: json['discount'] == null ? null : json['discount'].toDouble(),
        quantity: json['quantity'] == null ? null : json['quantity']);
  }

  factory ProductModel.fromCompanyJson(
      Map<String, dynamic> json, String companyId) {
    ProductModel p = ProductModel(
        id: json['_id'] ?? json['id'],
        companyId: companyId,
        name: json['name'],
        aisle: json['aisle'],
        subtitle: json['subtitle'],
        photoUrl: json['photoUrl'],
        url: json['url'],
        code: json['code'],
        brands: json['brands'],
        category: json['category'],
        externalId: json['externalId'],
        tax: json['tax'] == null ? null : taxFromDouble(json['tax'].toDouble()),
        salePrice: json['salePrice'] == null
            ? null
            : json['salePrice']["last"].toDouble(),
        costPrice: json['costPrice'] == null
            ? null
            : json['costPrice']["last"].toDouble(),
        discount: json['discount'] == null
            ? null
            : json['discount']["last"].toDouble(),
        quantity: json['quantity'] == null ? null : json['quantity']["last"]);
    return p;
  }

  Map<String, dynamic> setterFunctions() {
    return {
      'id': this.id,
      'name': this.name,
      'aisle': this.aisle,
      'subtitle': this.subtitle,
      'photoUrl': this.photoUrl,
      'url': this.url,
      'code': this.code,
      'brands': this.brands,
      'category': this.category,
      'externalId': this.externalId,
      'salePrice': this.salePrice,
      'discount': this.discount,
      'tax': this.tax,
      'quantity': this.quantity,
    };
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'aisle': aisle,
      'subtitle': subtitle,
      'photoUrl': photoUrl,
      'url': this.url,
      'code': code,
      'brands': brands,
      'category': category,
      'externalId': externalId,
      'salePrice': salePrice,
      'discount': discount,
      'quantity': quantity,
      'tax': this.taxValue,
      'qtyInCart': qtyInCart,
    };
  }

  Map<String, dynamic> toSaveJson() {
    return {
      'id': id,
      'name': name,
      'aisle': aisle,
      'subtitle': subtitle,
      'photoUrl': photoUrl,
      'url': this.url,
      'code': code,
      'brands': brands,
      'category': category,
      'externalId': externalId,
      'salePrice': salePrice,
      'discount': discount,
      'tax': this.taxValue,
      'quantity': quantity,
    };
  }

  Map<String, dynamic> toLightJson() {
    return {
      'id': id,
      'name': this.name,
      'salePrice': this.salePrice,
      'discount': this.discount,
      'tax': this.taxValue,
      'qty': this.qtyInCart,
    };
  }

  bool operator ==(o) => o is ProductModel && o.name == name;

  int get hashCode => hash2(name.hashCode, id.hashCode);



}
