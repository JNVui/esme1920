import 'dart:convert';

import 'package:cash_register_app/models/todo.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/utils/map_utils.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';



class TodoProvider{

  static Future<Todo> create(Todo todo) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.PUT, '/api/v1/company/$companyId/todo/',
        body: json.encode(cleanNull(todo.toJson())));

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      return Todo.fromJson(result["content"]);
    } else {
      throw Exception(response.body);
    }
  }

  static Future<Todo> update(Todo todo) async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();

    final response = await makeRequest(
        METHOD.PUT, '/api/v1/company/$companyId/todo/${todo.id}',
        body: json.encode(cleanNull(todo.toJson())));

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      return Todo.fromJson(result["content"]);
    } else {
      throw Exception(response.body);
    }
  }

  static Future<Todos> fetch() async {
    String companyId = await LoginSharedPreferences.getCurrentCompanyId();
    final response = await makeRequest(
        METHOD.GET, '/api/v1/company/$companyId/todos', queryParameters: {"selection": "online"});

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body);
      try {
        Todos ts = Todos.fromJson(result["content"]["todos"]);
        return ts;
      } catch (e) {
        Todos ts = Todos.fromJson(result["content"][0]["todos"]);
        return ts;
      }
    } else {
      throw Exception(response.body);
    }
  }



}