
import 'dart:convert';

import 'package:cash_register_app/models/product.dart';
import 'package:cash_register_app/ressources/utils.dart';
import 'package:cash_register_app/ui/widget/dialog.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:flutter/material.dart';

 class ProductProvider{
   static Future<ProductModel> create(ProductModel product) async {
     String companyId = await LoginSharedPreferences.getCurrentCompanyId();

     Map<String, dynamic> data = product.toJson();
     data.removeWhere((String key, value) => [null, ""].contains(value));
     data.removeWhere((String key, value) => ["id", "qtyInCart"].contains(key));

     final response = await makeRequest(
         METHOD.POST, '/api/v1/company/$companyId/item',
         body: json.encode(data));

     if (response.statusCode == 200) {
       Map<String, dynamic> result = json.decode(response.body);
       ProductModel p = ProductModel.fromJson(result["content"]);
       return p;
     } else {
       throw Exception('Failed to create item ${response.body}');
     }
   }
   static Future<ProductModel> update(ProductModel product) async {
     String companyId = await LoginSharedPreferences.getCurrentCompanyId();

     Map<String, dynamic> data = product.toJson();
     data.removeWhere((String key, value) => [null, ""].contains(value));
     data.removeWhere((String key, value) => ["id", "qtyInCart"].contains(key));

     final response = await makeRequest(
         METHOD.PUT, '/api/v1/company/$companyId/item/${product.id}',
         body: json.encode(data));

     if (response.statusCode == 200) {
       Map<String, dynamic> result = json.decode(response.body);
       ProductModel p =
       ProductModel.fromCompanyJson(result["item"], companyId);

       return p;
     } else {
       throw Exception('Failed to update item ${response.body}');
     }
   }
   static Future<ProductModel> fetchFromId(String objectId) async {
     String companyId = await LoginSharedPreferences.getCurrentCompanyId();

     final response = await makeRequest(
         METHOD.GET, '/api/v1/company/$companyId/item/$objectId');
     if (response.statusCode == 200) {
       Map<String, dynamic> result = json.decode(response.body);
       ProductModel p = ProductModel.fromCompanyJson(result["item"], companyId);

       return p;
     } else {
       throw Exception('Failed to update item ${response.body}');
     }
   }

   static Future<ProductModel> askForUpdate(ProductModel product, BuildContext context) {
     showAskDialog(context, product.name, "Quantité", "Ajouter")
         .then((dynamic value) {
       product.quantity = product.quantity == null
           ? int.parse(value)
           : product.quantity + int.parse(value);
       ProductProvider.update(product).then((ProductModel p) => p).catchError((e) => print(e));
     }).catchError((e) => print(e));
     return null;
   }

 }


