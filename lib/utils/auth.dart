import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

Future<String> _getCurrentUserToken() async {
  FirebaseUser fUser = await _auth.currentUser();
  return fUser.getIdToken();
}

Future<String> getCurrentUserToken() async {
  return LoginSharedPreferences.getToken();
}

Future<bool> refreshToken() async {
  FirebaseUser fUser = await _auth.currentUser();
  String token = await fUser.getIdToken();
  return LoginSharedPreferences.setToken(token);
}

Future<String> getCurrentDisplayName() async {
  FirebaseUser fUser = await _auth.currentUser();
  return fUser.displayName;
}
