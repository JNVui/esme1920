import 'package:cash_register_app/utils/StringsManager.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class HomeParametersPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const HomeParametersPage({Key key, this.observer}) : super(key: key);

  @override
  HomeParametersPageState createState() {
    return new HomeParametersPageState(this.observer);
  }
}

class HomeParametersPageState extends State<HomeParametersPage> {
  final FirebaseAnalyticsObserver observer;
  final String routeName = "/settings";

  HomeParametersPageState(this.observer);

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
        backgroundColor: Colors.transparent,
      ),
      body: ListView(children: [
        settingsCard(
          "Vendeur",
          "Paramètres du vendeur",
          Icon(Icons.settings),
          [
            settingsItem(
                "Informations",
                StringManager.of(context).locationSettingsSubtitle,
                Icon(FontAwesomeIcons.personBooth),
                onTap: () => Navigator.pushNamed(context, "/settings/me")),
          ],
        ),
        //Personal
        settingsCard(
          "Gestion",
          "Paramètres de la boutique",
          Icon(Icons.settings),
          [
            settingsItem(
                StringManager.of(context).location,
                StringManager.of(context).locationSettingsSubtitle,
                Icon(FontAwesomeIcons.locationArrow),
                onTap: () =>
                    Navigator.pushNamed(context, "/settings/location")),
            settingsItem(
                StringManager.of(context).contact,
                StringManager.of(context).contactSettingsSubtitle,
                Icon(FontAwesomeIcons.idCard),
                onTap: () => Navigator.pushNamed(context, "/settings/contact")),
            settingsItem(
                "Finance",
                StringManager.of(context).contactSettingsSubtitle,
                Icon(FontAwesomeIcons.moneyCheck),
                onTap: () => Navigator.pushNamed(context, "/settings/finance"))
          ],
        ),
        //Payment tools
        settingsCard(
          "Matériel",
          "Tools for payment",
          Icon(Icons.settings),
          [
            settingsItem("Paiement", "Gérer vos moyens de paiements",
                Icon(FontAwesomeIcons.moneyBillAlt),
                onTap: () => Navigator.pushNamed(context, "/settings/payment")),
          ],
        ),
        RaisedButton(
            child: Text("Se déconnecter"),
            onPressed: () {
              _auth.signOut();
              LoginSharedPreferences.setToken(null);
              Navigator.pushNamed(context, "/login");
            }),
      ]),
    );
  }

  //GroupCard
  Widget settingsCard(
      String title, String explanation, Icon icon, List<Widget> settings) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: Text(title),
              subtitle: Text(explanation),
            ),
          ]..addAll(settings),
        ),
      ),
    );
  }

  Widget settingsItem(String title, String subtitle, Icon icon,
      {Function onTap}) {
    return ListTile(
      leading: icon,
      title: Text(title),
      subtitle: Text(subtitle),
      onTap: onTap,
    );
  }
}
