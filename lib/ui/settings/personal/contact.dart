import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/utils/StringsManager.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ContactSettingsPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  const ContactSettingsPage({Key key, this.observer}) : super(key: key);

  @override
  ContactSettingsPageState createState() {
    return new ContactSettingsPageState(this.observer);
  }
}

class ContactSettingsPageState extends State<ContactSettingsPage> {
  final FirebaseAnalyticsObserver observer;

  final FocusNode emailFocus = FocusNode();
  final FocusNode phoneNumberFocus = FocusNode();
  final FocusNode websiteFocus = FocusNode();

  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController websiteController = new TextEditingController();

  ContactSettingsPageState(this.observer);

  @override
  void dispose() {
    super.dispose();
    emailFocus.dispose();
    phoneNumberFocus.dispose();
    websiteFocus.dispose();

    emailController.dispose();
    phoneController.dispose();
    websiteController.dispose();
  }

  Future<Company> _updateCompany() async {
    return CompanyProvider.updateCompany(Company.updatePartialJson(
        contact: Contact(
      email: emailController.text,
      phoneNumber: phoneController.text,
      website: websiteController.text,
    )));
  }

  void _initTextController(Company c) {
    emailController.text = c.contact.email;
    phoneController.text = c.contact.phoneNumber;
    websiteController.text = c.contact.website;
  }

  @override
  void initState() {
    super.initState();

    CompanyProvider.fetchCompany().then((company) {
      _initTextController(company);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.transparent,
          title: new Text(StringManager.of(context).contact),
        ),
        body: new Container(
            child: Column(children: <Widget>[
          new Expanded(
            child: new ListView(
              children: <Widget>[
                buildText(
                    emailFocus,
                    emailController,
                    TextInputType.emailAddress,
                    FontAwesomeIcons.envelope,
                    StringManager.of(context).email,
                    false,
                    null),
                _buildSeparator(),
                buildText(
                  phoneNumberFocus,
                  phoneController,
                  TextInputType.phone,
                  FontAwesomeIcons.phone,
                  StringManager.of(context).phoneNumber,
                  false,
                  null,
                ),
                _buildSeparator(),
                buildText(websiteFocus, websiteController, TextInputType.text,
                    FontAwesomeIcons.code, 'Site web', false, null),
                _buildSeparator(),
              ],
            ),
          ),
          buildButton(
              0.0,
              "Modifier",
              () => _updateCompany()
                  .then((Company comp) => Navigator.pop(context)))
        ])));
  }

  Widget _buildSeparator() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: buildSeparator(),
    );
  }
}
