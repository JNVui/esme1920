class Customer {
  String id;
  String fistName;
  String lastName;
  String email;
  String phone;
  List<dynamic> sales;

  Customer(
      {this.id,
      this.fistName,
      this.lastName,
      this.email,
      this.phone,
      this.sales});

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
        id: json['_id'],
        fistName: json['fistName'],
        lastName: json['lastName'],
        email: json['email'],
        phone: json['phone'],
        sales: json['sales']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'fistName': fistName,
      'lastName': lastName,
      'email': email,
      'phone': phone,
      'sales': sales,
    };
  }

  Map<String, dynamic> toAuthJson() {
    Map<String, dynamic> data = {};
    if (this.email != null) data["email"] = this.email;
    if (this.phone != null) data["phone"] = this.phone;
    return data;
  }
}
