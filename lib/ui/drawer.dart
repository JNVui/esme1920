import 'dart:math';

import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ressources/seller_api_provider.dart';
import 'package:cash_register_app/ui/base.dart';
import 'package:cash_register_app/ui/login/create_company.dart';
import 'package:cash_register_app/ui/login/join_company.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/utils/analytics.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

List<Color> colors = [
  Theme.Colors.loginGradientEnd,
  Theme.Colors.loginGradientStart,
  Colors.yellow,
  Colors.purple,
  Colors.grey
];

class MyDrawer extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;
  final BasePageState baseWidget;

  MyDrawer(this.baseWidget, this.observer);
  @override
  _MyDrawerState createState() =>
      new _MyDrawerState(this.baseWidget, this.observer);
}

class _MyDrawerState extends State<MyDrawer> {
  final FirebaseAnalyticsObserver observer;
  final String routeName = "/drawer";

  Future<User> _user;
  BasePageState baseWidget;
  Future<String> _currentCompanyIdFuture;
  bool _isExpanded = false;

  _MyDrawerState(this.baseWidget, this.observer);

  @override
  void initState() {
    sendCurrentPageToAnalytics(this.observer, routeName);
    super.initState();
    this.fetchUser();
    _currentCompanyIdFuture = LoginSharedPreferences.getCurrentCompanyId();
  }

  void fetchUser() {
    _user = SellerProvider.getMe();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
        child: new FutureBuilder(
      future: _user,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Container(child: Center(child: CircularProgressIndicator()));
          default:
            if (snapshot.hasError)
              return new Center(child: Text('Error: ${snapshot.error}'));
            else
              return createDrawer(context, snapshot);
        }
      },
    ));
  }

  Widget createDrawer(context, snapshot) {
    User user = snapshot.data;
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        userAccountProfileBuilder(user),
        Expanded(
            child: Container(
                child: ListView(
                    padding: EdgeInsets.all(0),
                    children: _isExpanded
                        ? createCompaniesList(context, user)
                        : createInfoList(context, user))))
      ],
    );
  }

  List<Widget> createCompaniesList(BuildContext context, User user) {
    return List.from(user.companies
        .map((company) => new ListTile(
              title: Text(company["name"]),
              subtitle: Text(company["_id"]),
              trailing: createCircleAvatarFromCompany(company),
              leading: IconButton(
                  icon: Icon(FontAwesomeIcons.trashAlt),
                  onPressed: () {
                    SellerProvider.quitCompany(company["_id"])
                        .then(
                            (String s) => this.setState(() => this.fetchUser()))
                        .catchError((e) => print(e));
                  }),
              onTap: () => switchAccounts(company["_id"]),
            ))
        .toList())
      ..add(Divider())
      ..add(ListTile(
        leading: Icon(FontAwesomeIcons.plusSquare),
        title: Text("Créer une boutique"),
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CreateCompanyPage(
                      isTunnel: false, observer: this.observer)),
            ),
      ))
      ..add(ListTile(
        leading: Icon(FontAwesomeIcons.signInAlt),
        title: Text("Rejoindre une boutique"),
        onTap: () {
          final Future<bool> result = Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => JoinCompanyPage(
                      isTunnel: false,
                    )),
          );
          result.then((bool res) {
            if ((res != null) & res) {
              this.setState(() => this.fetchUser());
            } else {
              print("Cancel");
            }
          });
        },
      ));
  }

  List<Widget> createInfoList(context, user) {
    return [
      new Divider(),
      new Container(
        child: Text("   Compte"),
        color: Colors.transparent,
      ),
      new ListTile(
          title: new Text(user.id),
          leading: new Icon(FontAwesomeIcons.userAlt),
          onTap: () {}),
      new ListTile(
          title: new Text(user.phoneNumber),
          leading: new Icon(FontAwesomeIcons.phone),
          onTap: () {}),
      new Divider(),
      new Container(
        child: Text("   Gestion"),
        color: Colors.transparent,
      ),
      new ListTile(
        title: new Text("Stocks"),
        leading: new Icon(FontAwesomeIcons.archive),
        onTap: () => Navigator.pushNamed(context, "/stocks"),
      ),
      new ListTile(
        title: new Text("Todos"),
        leading: new Icon(FontAwesomeIcons.stickyNote),
        onTap: () => Navigator.pushNamed(context, "/todos"),
      ),
      new ListTile(
        title: new Text("Clients"),
        leading: new Icon(FontAwesomeIcons.users),
        onTap: () => Navigator.pushNamed(context, "/customers"),
      ),
      new ListTile(
        title: new Text("Bills Manager"),
        leading: new Icon(FontAwesomeIcons.moneyBill),
        onTap: () => Navigator.pushNamed(context, "/billsManager"),
      ),
      new ListTile(
        title: new Text("Catégories"),
        leading: new Icon(FontAwesomeIcons.clipboardList),
        onTap: () => Navigator.pushNamed(context, "/categories"),
      ),
      new ListTile(
        title: new Text("Réseaux sociaux"),
        leading: new Icon(FontAwesomeIcons.wifi),
        onTap: () {},
      ),
      new Divider(),
      new Container(
        child: Text("   Paramètres"),
        color: Colors.transparent,
      ),
      new ListTile(
        title: new Text("Settings"),
        leading: new Icon(FontAwesomeIcons.cogs),
        onTap: () => Navigator.pushNamed(context, "/settings"),
      ),
      new ListTile(
        title: new Text("Cancel"),
        leading: new Icon(FontAwesomeIcons.ban),
        onTap: () => Navigator.pop(context),
      )
    ];
  }

  Widget userAccountProfileBuilder(User user) {
    return FutureBuilder(
      future: _currentCompanyIdFuture,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Text('loading...');
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return userAccountProfile(user, snapshot.data);
        }
      },
    );
  }

  Widget userAccountProfile(User user, String companyId) {
    String tCompanyId = companyId ??
        (user.companies.length > 0 ? user.companies[0]["_id"] : null);
    dynamic currentCompany = user.companies
        .firstWhere((company) => company["_id"] == tCompanyId, orElse: () {});

    return UserAccountsDrawerHeader(
      accountEmail: Container(
          padding: EdgeInsets.all(0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Text(user.email),
              ),
              Container(
                margin: new EdgeInsets.only(right: 20),
                child: GestureDetector(
                    child: Icon(
                      _isExpanded ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                      size: 20.0,
                      color: Colors.white,
                    ),
                    onTap: () => this.setState(
                          () => _isExpanded = !_isExpanded,
                        )),
              ),
            ],
          )),
      accountName: Text(user.displayName ?? ""),
      currentAccountPicture: new GestureDetector(
        child: (user.companies.length == 0) | (currentCompany == null)
            ? null
            : createCircleAvatarFromCompany(currentCompany),
        onTap: () => print("This is your current account."),
      ),
      otherAccountsPictures: user.companies.length > 1
          ? user.companies
              .where((company) => company["_id"] != tCompanyId)
              .map((company) {
              return new GestureDetector(
                child: createCircleAvatarFromCompany(company),
                onTap: () => switchAccounts(company["_id"]),
              );
            }).toList()
          : null,
      decoration: new BoxDecoration(
        gradient: new LinearGradient(
            colors: [
              Theme.Colors.loginGradientStart,
              Theme.Colors.loginGradientEnd
            ],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
    );
  }

  void switchAccounts(String companyId) {
    LoginSharedPreferences.setCurrentCompanyId(companyId);
    this.setState(() {
      this._currentCompanyIdFuture =
          LoginSharedPreferences.getCurrentCompanyId();
    });
  }

  Widget createCircleAvatarFromCompany(company) {
    return CircleAvatar(
      child: Text(
        company["name"][0].toUpperCase(),
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: colors[min(company["index"] - 1, colors.length - 1)],
    );
  }
}
