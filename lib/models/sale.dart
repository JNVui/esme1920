import 'dart:convert';

import 'package:cash_register_app/models/payment.dart';
import 'package:cash_register_app/models/product.dart';
import 'package:uuid/uuid.dart';

class Sale {
  final Set<ProductModel> products;
  String id = Uuid().v1();
  final String sellerId;
  final String clientId;
  final String token;
  final double totalPrice;
  final double extraDiscount;
  final double tax;
  final PaymentTypeValues paymentType;
  final DateTime createdAt;
  Payments payments;

  Sale(
      {this.id,
      this.products,
      this.sellerId,
      this.clientId,
      this.totalPrice,
        this.payments,
      this.extraDiscount,
      this.tax,
      this.token,
      this.paymentType,
      this.createdAt}){
    if(this.payments == null){
      this.payments = new Payments();
    }
  }

  double get totalPayed => this.payments.total;
  double get restToPay => this.totalPrice - (this.payments?.total ?? 0);
  bool get isFullyPayed =>this.payments.total == this.totalPrice;

  Map<String, dynamic> toJson() {
    return {
      '_id': this.id,
      'items': List.from(
          products.map((ProductModel p) => json.encode(p.toLightJson()))),
      'totalPrice': this.totalPrice,
      'extraDiscount': this.extraDiscount,
      'tax': this.tax,
      'token': this.token,
      'payments': this.payments.toJson(),
      'clientId': this.clientId,
      'sellerId': this.sellerId,
      'type': paymentType.toString(),
    };
  }

  factory Sale.fromJson(Map<String, dynamic> json) {
    print(json);
    Sale sale = Sale(
      id: json["_id"],
      products: json["items"] != null
          ? Set.from(json["items"].map((p) => ProductModel.fromJson(p)))
          : null,
      sellerId: json["sellerId"],
      clientId: json["clientId"],
      token: json["token"],
      totalPrice:
          json["totalPrice"] != null ? json["totalPrice"].toDouble() : null,
        payments: json["payments"] !=null ? Payments.fromJson(json["payments"]):null,
      extraDiscount: json["extraDiscount"] != null
          ? json["extraDiscount"].toDouble()
          : null,
      tax: json["tax"] != null ? json["tax"].toDouble() : null,
      paymentType: paymentFromString(json["type"]),
      createdAt: DateTime.parse(json["createdAt"]),
    );
    return sale;
  }
}

class Sales {
  List<Sale> sales;

  Sales(this.sales);

  double get total {
    return this.sales.fold(0, (tot, Sale s2) => tot + s2.totalPrice);
  }

  double get totalCB {
    return this
        .sales
        .where((Sale s) => s.paymentType == PaymentTypeValues.creditCard)
        .fold(0, (tot, Sale s2) => tot + s2.totalPrice);
  }

  double get totalGiftCard {
    return this
        .sales
        .where((Sale s) => s.paymentType == PaymentTypeValues.giftCard)
        .fold(0, (tot, Sale s2) => tot + s2.totalPrice);
  }

  double get totalCash {
    return this
        .sales
        .where((Sale s) => s.paymentType == PaymentTypeValues.cash)
        .fold(0, (tot, Sale s2) => tot + s2.totalPrice);
  }

  double get totalCheck {
    return this
        .sales
        .where((Sale s) => s.paymentType == PaymentTypeValues.check)
        .fold(0, (tot, Sale s2) => tot + s2.totalPrice);
  }

  factory Sales.fromJson(List<dynamic> json) {
    return Sales(List.from(json.map((s) => Sale.fromJson(s))));
  }
}
