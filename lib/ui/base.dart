import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ui/drawer.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/manage/home.dart';
import 'package:cash_register_app/ui/register/home.dart';
import 'package:cash_register_app/utils/analytics.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sum_up_plugin/sum_up_plugin.dart';

class BasePage extends StatefulWidget {
  final User user;
  final FirebaseAnalyticsObserver observer;

  BasePage(this.user, this.observer);

  @override
  BasePageState createState() => new BasePageState(this.user, this.observer);
}

class BasePageState extends State<BasePage> {
  int _currentIndex = 1;
  final User user;
  Color backgroundColor = Color(0xFFF77B67);
  final FirebaseAnalyticsObserver observer;

  final String routeName = "/base";

  GlobalKey bottomNavigationKey = GlobalKey();

  List<Widget> _children;

  BasePageState(this.user, this.observer);

  @override
  void initState() {
    sendCurrentPageToAnalytics(this.observer, routeName);

    _children = [
      RegisterScreen(observer:this.observer),
      HomeShopScreen(observer:this.observer),
    ];

    _baseCheckAtInit();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Theme.Colors.loginGradientStart,
                Theme.Colors.loginGradientEnd
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.settings),
                  onPressed: () {
                    Navigator.pushNamed(context, "/settings");
                  },
                )
              ],
            ),
            body: _children[_currentIndex], // new
            drawer: MyDrawer(this, this.observer),
            bottomNavigationBar: Container(
                child: FancyBottomNavigation(
              circleColor: backgroundColor,
              inactiveIconColor: backgroundColor,
              onTabChangedListener: onTabTapped,
              key: bottomNavigationKey,

              // new
              initialSelection: 1,
              // this will be set when a new tab is tapped
              tabs: [
                TabData(
                    iconData: FontAwesomeIcons.cashRegister, title: "Caisse"),
                //TabData(iconData: Icons.archive, title: "Stock"),
                TabData(
                    iconData: FontAwesomeIcons.tachometerAlt, title: "Boutique")
              ],
            ))));
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void _baseCheckAtInit() {
    if (this.user.companiesId != null && this.user.companiesId.isEmpty) {
      //TODO : run tunnel
    } else {
      LoginSharedPreferences.getCurrentCompanyId().then((String cId) {
        if (cId == null) {
          LoginSharedPreferences.setCurrentCompanyId(user.companiesId[0]);
        }
      });
    }
    _sumUpInit();
  }

  void _sumUpInit() {
    LoginSharedPreferences.isSumUpEnable().then((bool isTrue) =>
        ((isTrue != null) & (isTrue))
            ? SumUpPlugin(
                    clientId: LoginSharedPreferences.getSumupId(),
                    clientSecret: LoginSharedPreferences.getSumupSecretId())
                .login(context)
            : () {});
  }

  List get children {
    return _children;
  }
}
