import 'package:cash_register_app/models/cart.dart';
import 'package:cash_register_app/models/customer.dart';
import 'package:cash_register_app/models/sale.dart';
import 'package:cash_register_app/ressources/customer_api_provider.dart';
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:cash_register_app/ui/register/cart.dart';
import 'package:cash_register_app/ui/register/payment.dart';
import 'package:cash_register_app/ui/widget/buttons.dart';
import 'package:cash_register_app/ui/widget/headers.dart';
import 'package:cash_register_app/utils/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum CustomerType { PHONE, EMAIL }

var useSumUp = false;

class CheckoutScreen extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  final CartModel cart;
  final CartState cartState;
  final BuildContext parentContext;

  CheckoutScreen(this.observer,
      {this.cart, this.cartState, this.parentContext});

  @override
  CheckoutScreenState createState() {
    return new CheckoutScreenState(this.observer,
        cart: this.cart,
        parentContext: this.parentContext,
        cartState: this.cartState);
  }
}

class CheckoutScreenState extends State<CheckoutScreen> {
  final FirebaseAnalyticsObserver observer;

  final CartModel cart;
  final CartState cartState;
  final BuildContext parentContext;
  Customer customer;

  CustomerType _customerType = CustomerType.EMAIL;
  DiscountType _discountType = DiscountType.PERCENTAGE;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  double _refund;
  TextEditingController _payedController = new TextEditingController();
  TextEditingController _extraDiscountController = new TextEditingController();
  TextEditingController _customerInfoController = new TextEditingController();
  FocusNode _customerInfoFocus = new FocusNode();
  FocusNode _extraDiscountFocus = new FocusNode();

  CheckoutScreenState(
    this.observer, {
    this.cartState,
    this.cart,
    this.parentContext,
  });

  @override
  void initState() {
    observer.analytics.setCurrentScreen(screenName: "/checkout");

    super.initState();
    _refund = -this.cart.total;
    _payedController.text = "0.0";
    _extraDiscountController.text = this.cart.extraDiscount.toStringAsFixed(1);
    _payedController.addListener(() {
      this.setState(() =>
          _refund = double.tryParse(_payedController.text) - this.cart.total);
    });
    _extraDiscountController.addListener(() {
      this.setState(() => this.cart.extraDiscount =
          double.tryParse(_extraDiscountController.text) ?? 0);
    });

    LoginSharedPreferences.isSumUpEnable().then((value) {
      useSumUp = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Checkout"),
        ),
        body: Column(children: [
          cartTotalFooter(this.cart.total),
          ListTile(
            title: Text("TVA : "),
            trailing: Text(this.cart.taxTotal.toStringAsFixed(2) + " €"),
          ),
          Divider(),
          Expanded(
              child: ListView(
                  children: this.cart.products.map<Widget>((product) {
            return new ListTile(
              title: Text(
                product.name,
                style: TextStyle(fontSize: 15.0),
              ),
              trailing: Text(
                product.salePrice.toStringAsFixed(1),
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
              leading: Text(
                product.qtyInCart.toString(),
                style: TextStyle(fontSize: 15.0),
              ),
            );
          }).toList())),
          Divider(),
          buildText(
              _customerInfoFocus,
              _customerInfoController,
              TextInputType.text,
              FontAwesomeIcons.idCard,
              "Client",
              false,
              DropdownButton<CustomerType>(
                items: <CustomerType>[CustomerType.EMAIL, CustomerType.PHONE]
                    .map((CustomerType value) {
                  return new DropdownMenuItem<CustomerType>(
                    value: value,
                    child: Icon(value == CustomerType.EMAIL
                        ? FontAwesomeIcons.envelope
                        : FontAwesomeIcons.phone),
                  );
                }).toList(),
                onChanged: (CustomerType newValue) =>
                    setState(() => _customerType = newValue),
                value: _customerType,
              )),
          buildSeparator(),
          buildText(
              _extraDiscountFocus,
              _extraDiscountController,
              TextInputType.number,
              FontAwesomeIcons.idCard,
              "Réduction",
              false,
              DropdownButton<DiscountType>(
                items: <DiscountType>[
                  DiscountType.PERCENTAGE,
                  DiscountType.NUMBER
                ].map((DiscountType value) {
                  return new DropdownMenuItem<DiscountType>(
                    value: value,
                    child: Icon(value == DiscountType.PERCENTAGE
                        ? FontAwesomeIcons.percent
                        : FontAwesomeIcons.euroSign),
                  );
                }).toList(),
                onChanged: (DiscountType newValue) =>
                    setState((){
                      _discountType = newValue;
                      this.cart.discountType = _discountType;
                    }),
                value: _discountType,
              )),
          buildSeparator(),
          Container(
              padding: EdgeInsets.only(
                  top: 10.0, left: 10.0, right: 10.0, bottom: 30.0),
              child: baseButton(context, () async {
                print("Clicked");
                String token = await LoginSharedPreferences.getResisterIsOpen();
                Sale sale = createSale(token);

                Customer customer =
                    await CustomerProvider.fetchOrCreate(new Customer(
                  email: _customerType == CustomerType.EMAIL
                      ? _customerInfoController.text
                      : null,
                  phone: _customerType == CustomerType.PHONE
                      ? _customerInfoController.text
                      : null,
                ));
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new PaymentScreen(observer, sale, customer)));
              }, 'Finaliser'))
        ]));
  }

  Sale createSale(String token) {
    this.cart.extraDiscount = double.tryParse(_extraDiscountController.text);
    return new Sale(
        products: this.cart.products,
        extraDiscount: this.cart.extraDiscount,
        totalPrice: this.cart.total,
        tax: this.cart.taxTotal,
        token: token);
  }

  void _logSale(Sale sale) {
    observer.analytics.logEcommercePurchase(
      currency: 'EUR',
      value: sale.totalPrice,
      transactionId: sale.id,
      tax: sale.tax,
      shipping: 5.67,
    );
  }
}
