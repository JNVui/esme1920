import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';

Future<String> scanArticle() async {
  try {
    String barcode = await BarcodeScanner.scan();
    return barcode;
  } on PlatformException catch (e) {
    if (e.code == BarcodeScanner.CameraAccessDenied) {
      return 'The user did not grant the camera permission!';
    } else {
      return 'Unknown error: $e';
    }
  } on FormatException {
    return 'Vous avez quitté le scanner';
  } catch (e) {
    return 'Unknown error: $e';
  }
}
