import 'package:cash_register_app/models/user.dart';
import 'package:cash_register_app/ui/login/create_company.dart';
import 'package:cash_register_app/ui/login/join_company.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/manage/painter.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class JoinOrCreateCompany extends StatefulWidget {
  final User user;
  final FirebaseAnalyticsObserver observer;

  JoinOrCreateCompany(this.user, this.observer);

  @override
  JoinOrCreateCompanyState createState() {
    return new JoinOrCreateCompanyState(this.user, this.observer);
  }
}

class JoinOrCreateCompanyState extends State<JoinOrCreateCompany>
    with SingleTickerProviderStateMixin {
  final User user;
  final FirebaseAnalyticsObserver observer;

  PageController _pageController;
  Color left = Colors.black;
  Color right = Colors.white;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  JoinOrCreateCompanyState(this.user, this.observer);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    // Dispose of the Tab Controller
    _pageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Theme.Colors.loginGradientStart,
                Theme.Colors.loginGradientEnd
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: new Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          key: _scaffoldKey,
          body: NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
            },
            child: SingleChildScrollView(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height >= 775.0
                    ? MediaQuery.of(context).size.height
                    : 775.0,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 120.0, bottom: 30.0),
                      child: Container(
                        child: Text(
                          "Welcome in. ",
                          style: TextStyle(
                              fontFamily: "WorkSansSemiBold",
                              fontSize: 40.0,
                              color: Colors.white),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: _buildMenuBar(context),
                    ),
                    Expanded(
                      flex: 2,
                      child: PageView(
                        controller: _pageController,
                        onPageChanged: (i) {
                          if (i == 0) {
                            setState(() {
                              right = Colors.white;
                              left = Colors.black;
                            });
                          } else if (i == 1) {
                            setState(() {
                              right = Colors.black;
                              left = Colors.white;
                            });
                          }
                        },
                        children: <Widget>[
                          new ConstrainedBox(
                            constraints: const BoxConstraints.expand(),
                            child: JoinCompanyPage(
                              isTunnel: true,
                            ),
                          ),
                          new ConstrainedBox(
                            constraints: const BoxConstraints.expand(),
                            child: CreateCompanyPage(
                              isTunnel: true,
                              observer: this.observer,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Widget getBody() {
    return Column(
      children: <Widget>[_buildMenuBar(context), _buildPages()],
    );
  }

  Widget _buildPages() {
    return Expanded(
        flex: 2,
        child: PageView(
          controller: _pageController,
          onPageChanged: (i) {
            if (i == 0) {
              setState(() {
                right = Colors.white;
                left = Colors.black;
              });
            } else if (i == 1) {
              setState(() {
                right = Colors.black;
                left = Colors.white;
              });
            }
          },
          children: <Widget>[
            new ConstrainedBox(
              constraints: const BoxConstraints.expand(),
              child: CreateCompanyPage(isTunnel: true, observer: observer,),
            ),
            new ConstrainedBox(
              constraints: const BoxConstraints.expand(),
              child: JoinCompanyPage(isTunnel: true),
            ),
          ],
        ));
  }

  Widget _buildMenuBar(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      width: 300.0,
      height: 50.0,
      decoration: BoxDecoration(
        color: Color(0x552B2B2B),
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: CustomPaint(
        painter: TabIndicationPainter(pageController: _pageController),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignInButtonPress,
                child: Text(
                  "Rejoindre",
                  style: TextStyle(color: left, fontSize: 16.0),
                ),
              ),
            ),
            //Container(height: 33.0, width: 1.0, color: Colors.white),
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "Créer",
                  style: TextStyle(color: right, fontSize: 16.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onSignInButtonPress() {
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _onSignUpButtonPress() {
    _pageController?.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }
}
