import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:cash_register_app/models/cart.dart';
import 'package:cash_register_app/models/categories.dart';
import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/product.dart';
import 'package:cash_register_app/ressources/company_api_provider.dart';
import 'package:cash_register_app/ui/stock/product_edit.dart';
import 'package:cash_register_app/ui/widget/dialog.dart';
import 'package:cash_register_app/ui/widget/headers.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:share_extend/share_extend.dart';

class CoreRegisterStock extends StatefulWidget {
  final Company company;
  final bool isRegister;
  final CartModel cart;
  final FirebaseAnalyticsObserver observer;

  CoreRegisterStock(this.company, this.isRegister, {this.cart, this.observer});

  @override
  CoreRegisterStockState createState() {
    return new CoreRegisterStockState(this.company, this.isRegister, this.cart, this.observer);
  }
}

class CoreRegisterStockState extends State<CoreRegisterStock> {
  final FirebaseAnalyticsObserver observer;

  Company company;
  final bool isRegister;
  final CartModel cart;

  List<dynamic> whatToDisplay;
  Category selectedCategory;
  bool displayCategories = true;

  GlobalKey qrImageKey = new GlobalKey();
  GlobalKey codeImageKey = new GlobalKey();

  CoreRegisterStockState(this.company, this.isRegister, this.cart, this.observer);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _createBody(company: company, isRegister: isRegister);
  }

  Widget _createBody({BuildContext context, Company company, bool isRegister}) {
    return new Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      new Container(
          child: Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 40.0, 0.0, 20.0),
              child: buildNameHeader())),
      Container(
          color: Colors.transparent,
          child: ListTile(
              title: Text(
                this.displayCategories
                    ? (this.selectedCategory == null
                        ? "Catégories"
                        : this.selectedCategory.name)
                    : "Tous",
                style: new TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontFamily: 'Montserrat'),
              ),
              leading: this.displayCategories & (this.selectedCategory != null)
                  ? IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                      ),
                      onPressed: () => this.setState(() {
                            this.selectedCategory =
                                this.selectedCategory.parentNode;
                          }))
                  : null,
              trailing: CupertinoSwitch(
                  value: !this.displayCategories,
                  activeColor: Colors.green,
                  onChanged: (bool value) => this.setState(() {
                        this.displayCategories = !value;
                        if (!this.displayCategories) {
                          this.selectedCategory = null;
                        }
                      })))),
      createBodyGrid(company: company, isRegister: isRegister),
      Divider(),
    ]);
  }

  Widget createBodyGrid({Company company, bool isRegister}) {
    List<dynamic> whatToDisplay;
    if (!this.displayCategories) {
      selectedCategory = null;
      whatToDisplay = company.stock.productList;
    } else {
      whatToDisplay = List.from(selectedCategory == null
          ? company.categories.hierarchy.values
          : selectedCategory.children.values);
      if (selectedCategory != null) {
        whatToDisplay
          ..addAll(List.from(
              company.stock.getProductByCategory(selectedCategory.id) ?? []));
      } else {
        whatToDisplay
          ..addAll(List.from(company.stock.getProductByCategory(null) ?? []));
      }
    }
    return createGridView(whatToDisplay, isRegister);
  }

  Widget createGridView(List<dynamic> objects, bool isRegister) {
    return Expanded(
        child: Container(
            margin: EdgeInsets.all(10.0),
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              color: Color(0x552B2B2B),
            ),
            child: Container(
                padding: EdgeInsets.all(10),
                child: GridView.builder(
                    itemCount: objects.length,
                    padding: EdgeInsets.fromLTRB(6.0, 0.0, 6.0, 0.0),
                    gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 0.9,
                        crossAxisSpacing: 12.0,
                        mainAxisSpacing: 12.0,
                        crossAxisCount: 3),
                    itemBuilder: (BuildContext context, int index) => buildCard(
                        context: context,
                        object: objects[index],
                        isRegister: isRegister)))));
  }

  Widget buildCard({BuildContext context, dynamic object, bool isRegister}) {
    Widget topLeftWidget = Container();
    Widget topRightWidget = Container();
    Widget bottomLeftWidget = Container();
    Widget bottomRightWidget = Container();
    String nameCenter;
    Function onTap;
    Function onLongPress;

    if (object is Category) {
      nameCenter = object.name;
      topLeftWidget = Icon(
        FontAwesomeIcons.clipboardList,
        size: 20,
      );
      onTap = () {
        this.setState(() {
          selectedCategory = object;
        });
      };
    } else if (object is ProductModel) {
      //Mode Caisse
      if (isRegister) {
        if (this.cart.containsProduct(object)) {
          object = this.cart.getProductById(object.id);
        }

        nameCenter = object.name;
        bottomRightWidget = priceWidget(object);
        topLeftWidget = object.qtyInCart > 0
            ? Text(object.qtyInCart.toString())
            : Container();
        onTap = () {
          this.setState(() {
            this.cart.addProduct(object);
          });
        };
        onLongPress = () => showAskDialog(
                context, object.name, "Quantité", "Ajouter")
            .then((dynamic value) => this.setState(
                () => this.cart.addProduct(object, number: int.parse(value))));
      }
      // Mode Stock
      else {
        nameCenter = object.name;
        bottomRightWidget = Text(object.salePrice.toString() + "€",
            style: new TextStyle(
                fontSize: 15.0, color: Colors.black, fontFamily: 'Montserrat'));
        topLeftWidget = ((object.quantity ?? 0) > 0)
            ? Text(object.quantity.toString(),
                style: new TextStyle(
                    fontSize: 15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'))
            : Container();

        topRightWidget = IconButton(
          icon: Icon(Icons.edit),
          iconSize: 20,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CreateModifyItemScreen(
                    observer:this.observer,
                        company: company,
                        product: object,
                        isModifying: true,
                      )),
            ).then((dynamic result) =>
                CompanyProvider.fetch().then((Company comp) => this.company = comp));
          },
        );
        onTap = () => object.askForUpdate(context);
        onLongPress = () => settingModalBottomSheet(context, object);
      }
    }

    return _buildCard(
        nameCenter: nameCenter,
        bottomLeftWidget: bottomLeftWidget,
        bottomRightWidget: bottomRightWidget,
        topLeftWidget: topLeftWidget,
        topRightWidget: topRightWidget,
        onTap: onTap,
        onLongPress: onLongPress);
  }

  Widget _buildCard(
      {nameCenter,
      bottomRightWidget,
      topRightWidget,
      bottomLeftWidget,
      topLeftWidget,
      onTap,
      onLongPress}) {
    return Material(
        elevation: 8,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x802196F3),
        child: new InkWell(
            child: new Container(
                alignment: Alignment.center,
                child: Stack(
                  children: <Widget>[
                    Align(
                        alignment: Alignment.center,
                        child: Text(nameCenter,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                                fontSize: 20.0,
                                color: Colors.black,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold))),
                    Positioned(
                        bottom: 10.0, right: 10.0, child: bottomRightWidget),
                    Positioned(top: -5.0, right: -5.0, child: topRightWidget),
                    Positioned(
                        bottom: 10.0, left: 10.0, child: bottomLeftWidget),
                    Positioned(top: 10.0, left: 10.0, child: topLeftWidget)
                  ],
                )),
            onLongPress: onLongPress,
            onTap: onTap));
  }

  void settingModalBottomSheet(BuildContext context, ProductModel product) {
    final bodyHeight = MediaQuery.of(context).size.height -
        MediaQuery.of(context).viewInsets.bottom;

    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return ListView(children: <Widget>[
            Container(
                child: new Wrap(
              children: <Widget>[
                Text(product.name,
                    style: new TextStyle(
                        fontSize: 15.0,
                        color: Colors.black,
                        fontFamily: 'Montserrat')),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                      "Tout comme Dragon Ball, Dragon Ball Z se déroule dans des univers multiples où la guerre et l’affrontement entre le bien et le mal sont permanents. L'une des particularités de ces mondes est caractérisée par la présence de boules de cristal. Si sept boules de cristal sont réunies, n’importe quel vœu peut être exaucé en invoquant le dragon qui sort des boules (Shenron). Ces dernières sont la cause des plus grands conflits de la saga, car elles peuvent apporter l’immortalité. Presque tous les plus grands ennemis de Dragon Ball Z les recherchent (Vegeta, Freezer, Garlic Junior,Picolo)."),
                ),
                _stickersQR(qrImageKey, product, 0.10 * bodyHeight),
                ListTile(
                  leading: Icon(FontAwesomeIcons.share),
                  title: Text("Partager Fiche Produit"),
                  onTap: () => _captureAndSharePng(qrImageKey),
                ),
                ListTile(
                  leading: Icon(FontAwesomeIcons.print),
                  title: Text("Partager Etiquette QR"),
                  onTap: () => _captureAndSharePng(qrImageKey),
                ),
              ],
            )),
          ]);
        });
  }

  Widget _stickersQR(GlobalKey globalKey, ProductModel product, double size) {
    return Card(
      child: RepaintBoundary(
        key: globalKey,
        child: SizedBox(
          height: 100,
          width: 300,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(product.name,
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          fontFamily: 'Montserrat')),
                ),
              ),
              Align(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(product.salePrice.toString() + "€",
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          fontFamily: 'Montserrat')),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.network(
                    "https://www.altermundi.com/themes/altermundi/img/logo_1.png",
                    height: 50,
                    width: 50,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                      "Faire bouillir trente minute"), //TODO short description
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: QrImage(
                  data: product.qrCodeValue,
                  size: size,
                  onError: (ex) {
                    setState(() {});
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _captureAndSharePng(GlobalKey globalKey) async {
    try {
      RenderRepaintBoundary boundary =
          globalKey.currentContext.findRenderObject();
      var image = await boundary.toImage();
      ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();

      final tempDir = await getTemporaryDirectory();
      final file = await new File('${tempDir.path}/image.png').create();
      await file.writeAsBytes(pngBytes);

      //Open Share option
      ShareExtend.share(file.path, "image");
    } catch (e) {
      print(e.toString());
    }
  }

  Widget priceWidget(ProductModel product) {
    if (product.hasDiscount) {
      return Container(
          child: new RichText(
        text: new TextSpan(
          children: <TextSpan>[
            new TextSpan(
              text: product.salePrice.toString(),
              style: new TextStyle(
                color: Colors.black,
                fontSize: 12,
                decoration: TextDecoration.lineThrough,
                fontFamily: 'Montserrat',
              ),
            ),
            new TextSpan(
              text: product.realPrice.toStringAsFixed(1) + "€",
              style: new TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Montserrat',
                  fontSize: 15.0),
            ),
          ],
        ),
      ));
    }
    return Text(product.salePrice.toString() + "€",
        style: new TextStyle(
            fontSize: 15.0, color: Colors.black, fontFamily: 'Montserrat'));
  }
}
