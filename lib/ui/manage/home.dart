import 'package:cash_register_app/ui/manage/history/home.dart';
import 'package:cash_register_app/ui/manage/painter.dart';
import 'package:cash_register_app/ui/manage/stats/home.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class HomeShopScreen extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  HomeShopScreen({this.observer});

  @override
  HomeShopScreenState createState() {
    return new HomeShopScreenState(this.observer);
  }
}

class HomeShopScreenState extends State<HomeShopScreen>
    with SingleTickerProviderStateMixin {
  final FirebaseAnalyticsObserver observer;

  PageController _pageController;
  Color left = Colors.black;
  Color right = Colors.white;

  HomeShopScreenState(this.observer);

  @override
  void initState() {
    observer.analytics.setCurrentScreen(screenName: "/manage");

    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    // Dispose of the Tab Controller
    _pageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: getBody(),
      backgroundColor: Colors.transparent,
    );
  }

  Widget getBody() {
    return Column(
      children: <Widget>[_buildMenuBar(context), _buildPages()],
    );
  }

  Widget _buildPages() {
    return Expanded(
        flex: 2,
        child: PageView(
          controller: _pageController,
          onPageChanged: (i) {
            if (i == 0) {
              setState(() {
                right = Colors.white;
                left = Colors.black;
              });
            } else if (i == 1) {
              setState(() {
                right = Colors.black;
                left = Colors.white;
              });
            }
          },
          children: <Widget>[
            new ConstrainedBox(
              constraints: const BoxConstraints.expand(),
              child: StatsPage(this.observer),
            ),
            new ConstrainedBox(
              constraints: const BoxConstraints.expand(),
              child: HistoryPage(observer:this.observer),
            ),
          ],
        ));
  }

  Widget _buildMenuBar(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      width: 300.0,
      height: 50.0,
      decoration: BoxDecoration(
        color: Color(0x552B2B2B),
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: CustomPaint(
        painter: TabIndicationPainter(pageController: _pageController),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignInButtonPress,
                child: Text(
                  "Statistiques",
                  style: TextStyle(color: left, fontSize: 16.0),
                ),
              ),
            ),
            //Container(height: 33.0, width: 1.0, color: Colors.white),
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "Historique",
                  style: TextStyle(color: right, fontSize: 16.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onSignInButtonPress() {
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _onSignUpButtonPress() {
    _pageController?.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void fetchData() {}
}
