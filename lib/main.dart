
import 'package:cash_register_app/ui/register/closing.dart';
import 'package:cash_register_app/ui/ui.dart';
import 'package:cash_register_app/utils/StringsManager.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    analytics.logAppOpen();

    return new MaterialApp(
      supportedLocales: [
        const Locale('en', 'US'), // English
        const Locale('fr', 'FR'), // French
      ],
      localizationsDelegates: [
        const StringManagerDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      navigatorObservers: <NavigatorObserver>[observer],
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new LoginPage(observer: observer),
        '/manage': (BuildContext context) =>
            new HomeShopScreen(observer: observer),
        '/stocks': (BuildContext context) =>
            new StockHomeScreen(observer: observer),
        '/customers': (BuildContext context) =>
            new CustomersPage(observer: observer),
        '/categories': (BuildContext context) =>
            new CategoriesPage(observer: observer),
        '/register': (BuildContext context) =>
            new RegisterScreen(observer: observer),
        '/close': (BuildContext context) =>
        new ClosingPage(observer: observer),
        '/todos': (BuildContext context) => new TodoScreen(observer: observer),
        '/settings': (BuildContext context) =>
            new HomeParametersPage(observer: observer),
        '/settings/finance': (BuildContext context) =>
            new FinanceSettingsPage(observer: observer),
        '/settings/contact': (BuildContext context) =>
            new ContactSettingsPage(observer: observer),
        '/settings/location': (BuildContext context) =>
            new LocationSettingsPage(observer: observer),
        '/settings/payment': (BuildContext context) =>
            new PaymentSettingsPage(observer: observer),
        '/settings/me': (BuildContext context) =>
            new PersonalContactSettingsPage(observer: observer),
        '/billsManager': (BuildContext context) => new BillsManager(),
        '/billsManager/create_bill': (BuildContext context) =>
            new CreateBillPage(observer: observer),
      },
      home: new LoginPage(observer: observer),
    );
  }
}
