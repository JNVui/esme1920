import 'package:cash_register_app/models/categories.dart';
import 'package:cash_register_app/models/company.dart';
import 'package:cash_register_app/models/product.dart';
import 'package:cash_register_app/models/tax.dart';
import 'package:cash_register_app/ressources/product_api_provider.dart';
import 'package:cash_register_app/ui/categories/select_parent.dart';
import 'package:cash_register_app/ui/login/theme.dart' as Theme;
import 'package:cash_register_app/ui/login/widgets.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CreateModifyItemScreen extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  final ProductModel product;
  final bool isModifying;
  final Company company;

  CreateModifyItemScreen({this.product, this.isModifying, this.company, this.observer});

  @override
  _AddFormState createState() => new _AddFormState(
    this.observer,
      product: this.product,
      isModifying: this.isModifying,
      company: this.company);
}

class _AddFormState extends State<CreateModifyItemScreen> {
  final FirebaseAnalyticsObserver observer;

  ProductModel product;
  bool isModifying;
  final List<String> productCategories;
  final Company company;
  int _radioValue1;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _AddFormState(this.observer,
      {this.product, this.isModifying, this.productCategories, this.company});

  TextEditingController productTextEditingControllerName;
  TextEditingController productTextEditingControllerUrl;
  TextEditingController productTextEditingControllerAisle;
  TextEditingController productTextEditingControllerSubtitle;
  TextEditingController productTextEditingControllerPhotoUrl;
  TextEditingController productTextEditingControllerCode;
  TextEditingController productTextEditingControllerBrands;
  TextEditingController productTextEditingControllerPrice;
  TextEditingController productTextEditingControllerDiscount;
  TextEditingController productTextEditingControllerQuantity;

  FocusNode myFocusName = new FocusNode();
  FocusNode myFocusUrl = new FocusNode();
  FocusNode myFocusAisle = new FocusNode();
  FocusNode myFocusSubtitle = new FocusNode();
  FocusNode myFocusPhotoUrl = new FocusNode();
  FocusNode myFocusCode = new FocusNode();
  FocusNode myFocusBrands = new FocusNode();
  FocusNode myFocusPrice = new FocusNode();
  FocusNode myFocusDiscount = new FocusNode();
  FocusNode myFocusQuantity = new FocusNode();

  @override
  initState() {
    super.initState();

    if (this.product == null) {
      this.product = new ProductModel();
      _radioValue1 = -1;
    }

    switch (this.product.tax) {
      case TAX.T20:
        _radioValue1 = 0;
        break;
      case TAX.T10:
        _radioValue1 = 1;
        break;
      case TAX.T85:
        _radioValue1 = 2;
        break;
      case TAX.T5:
        _radioValue1 = 3;
        break;
    }
    _initializeControllers();
  }

  void initController(TextEditingController controller, ProductModel p, name) {
    controller.text = p.setterFunctions()[name];
  }

  @override
  void dispose() {
    productTextEditingControllerName.dispose();
    productTextEditingControllerAisle.dispose();
    productTextEditingControllerSubtitle.dispose();
    productTextEditingControllerPhotoUrl.dispose();
    productTextEditingControllerCode.dispose();
    productTextEditingControllerBrands.dispose();
    productTextEditingControllerPrice.dispose();
    productTextEditingControllerDiscount.dispose();
    productTextEditingControllerQuantity.dispose();
    myFocusName.dispose();
    myFocusAisle.dispose();
    myFocusSubtitle.dispose();
    myFocusPhotoUrl.dispose();
    myFocusCode.dispose();
    myFocusBrands.dispose();
    myFocusPrice.dispose();
    myFocusDiscount.dispose();
    myFocusQuantity.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                Theme.Colors.loginGradientStart,
                Theme.Colors.loginGradientEnd
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: new Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
            ),
            backgroundColor: Colors.transparent,
            body: Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: Center(child: _buildEditForm(context)))));
  }

  Widget _buildEditForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: ListView(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 550.0,
                  child: ListView(
                    children: <Widget>[
                      buildText(
                          myFocusName,
                          productTextEditingControllerName,
                          TextInputType.text,
                          FontAwesomeIcons.idCard,
                          "Nom",
                          false,
                          null),
                      buildSeparator(),
                      buildText(
                          myFocusUrl,
                          productTextEditingControllerUrl,
                          TextInputType.url,
                          FontAwesomeIcons.code,
                          "Url",
                          false,
                          null),
                      buildSeparator(),
                      buildText(
                          myFocusCode,
                          productTextEditingControllerCode,
                          TextInputType.text,
                          FontAwesomeIcons.qrcode,
                          "Code",
                          false,
                          null),
                      buildSeparator(),
                      buildText(
                          myFocusBrands,
                          productTextEditingControllerBrands,
                          TextInputType.text,
                          FontAwesomeIcons.signature,
                          "Marque",
                          false,
                          null),
                      buildSeparator(),
                      _buildCategorySelector(),
                      buildSeparator(),
                      _buildPriceWidget(),
                      buildSeparator(),
                      dropDownMenu()
                    ],
                  ),
                ),
              ),
              buildButton(540.0, this.isModifying ? "Modifier" : "Ajouter",
                  () => _updateItem(this.product))
            ],
          )
        ],
      ),
    );
  }

  Widget _buildCategorySelector() {
    return ListTile(
      leading: Icon(FontAwesomeIcons.clipboardList),
      title: this.product.category == null
          ? Center(child: Text("Selectionner catégorie"))
          : Text(this
                  .company
                  .categories
                  .mappedCategories[this.product.category]
                  ?.name ??
              this.product.category),
      onTap: () {
        Future<Category> chosenCategory = Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SelectCategoriesPage(
                    isHome: false, currentCategoryId: product.category)));
        chosenCategory.then((Category cat) {
          if (cat != null) this.setState(() => this.product.category = cat.id);
        });
      },
    );
  }

  Widget _buildPriceWidget() {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        new Flexible(
            child: buildRowText(
                myFocusPrice,
                productTextEditingControllerPrice,
                TextInputType.number,
                FontAwesomeIcons.signature,
                "Prix",
                false,
                null)),
        new Flexible(
            child: buildRowText(
                myFocusDiscount,
                productTextEditingControllerDiscount,
                TextInputType.number,
                FontAwesomeIcons.signature,
                "Discount",
                false,
                null)),
        new Flexible(
            child: buildRowText(
                myFocusQuantity,
                productTextEditingControllerQuantity,
                TextInputType.number,
                FontAwesomeIcons.signature,
                "Quantité",
                false,
                null)),
      ],
    );
  }

  Widget dropDownMenu() {
    return Padding(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                new Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text("20%"),
              ],
            ),
            Column(
              children: <Widget>[
                new Radio(
                  value: 1,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text("10%"),
              ],
            ),
            Column(
              children: <Widget>[
                new Radio(
                  value: 2,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text("8.5%"),
              ],
            ),
            Column(
              children: <Widget>[
                new Radio(
                  value: 3,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text("5.5%"),
              ],
            )
          ],
        ));
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in productCategories) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  void _updateItem(ProductModel p) {
    this.product.discount =
        double.tryParse(productTextEditingControllerDiscount.text);
    this.product.url = productTextEditingControllerUrl.text;
    this.product.quantity =
        int.tryParse(productTextEditingControllerQuantity.text);
    this.product.salePrice =
        double.tryParse(productTextEditingControllerPrice.text);
    this.product.brands = productTextEditingControllerBrands.text;
    this.product.code = productTextEditingControllerCode.text;
    this.product.subtitle = productTextEditingControllerSubtitle.text;
    this.product.aisle = productTextEditingControllerAisle.text;
    this.product.name = productTextEditingControllerName.text;
    Future<ProductModel> _future =
        this.isModifying ? ProductProvider.update(this.product) : ProductProvider.create(this.product);
    _future.then((ProductModel p) {
      if (p != null)
        Navigator.of(context).pop(true);
      else
        print("p not found");
    }).catchError((error) => showInSnackBar(_scaffoldKey, context, error));
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          this.product.tax = TAX.T20;
          break;
        case 1:
          this.product.tax = TAX.T10;
          break;
        case 2:
          this.product.tax = TAX.T85;
          break;
        case 3:
          this.product.tax = TAX.T5;
          break;
      }
    });
  }

  void _initializeControllers() {
    productTextEditingControllerName =
        new TextEditingController(text: this.product.name);
    productTextEditingControllerUrl = new TextEditingController(
        text: this.product.url ?? this.company.contact.website);
    productTextEditingControllerAisle =
        new TextEditingController(text: this.product.aisle);
    productTextEditingControllerSubtitle =
        new TextEditingController(text: this.product.subtitle);
    productTextEditingControllerPhotoUrl =
        new TextEditingController(text: this.product.photoUrl);
    productTextEditingControllerCode =
        new TextEditingController(text: this.product.code);
    productTextEditingControllerBrands =
        new TextEditingController(text: this.product.brands);
    productTextEditingControllerPrice =
        new TextEditingController(text: this.product.salePrice.toString());
    productTextEditingControllerDiscount =
        new TextEditingController(text: this.product.discount.toString());
    productTextEditingControllerQuantity =
        new TextEditingController(text: this.product.quantity.toString());
  }
}
